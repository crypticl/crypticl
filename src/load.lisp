;;;;-*-lisp-*-
;;;; The Crypticl cryptographic library.
;;;;
;;;; Description: Loads the library.
;;;;              
;;;; Usage: Loading this file will load the rest of the library.
;;;;        After loading you can list the public interface with 
;;;;        (crypticl:print-external-symbols) from the top-level. 
;;;; Author: Taale Skogan
;;;; Distribution:  See the accompanying file LICENSE.

;;To do:

(in-package cl-user)

;;(proclaim '(optimize (speed 2) (safety 1) (space 1) (debug 3)))


;; Load library. NB! The order of the file names in the list  matter.
(let ((files '("crypticl-package"
	       "utilities" 
	       "numtheory"
	       "common"
               "sha"
	       "sha256"               
               "aes"
               "random"               
	       "keygenerator"               
	       "md5" "idea" "dsa" "rsa" "diffie-hellman"
	       
	       "keystore"
	       "test")))
  (format t "Loading the Crypticl library...")
  (dolist (file files)
    (let* ((path
	    (make-pathname 
	     :device (pathname-device *load-pathname*)
	     :directory (directory-namestring *load-pathname*)))
	   (module (merge-pathnames file path)))
      (compile-file module :verbose nil)
      (load module))))


