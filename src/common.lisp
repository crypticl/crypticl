;;;;-*-lisp-*-
;;;; The Crypticl cryptographic library.
;;;;
;;;; Description: Common functionality across cryptographic primitives.
;;;; Author: Taale Skogan
;;;; Distribution:  See the accompanying file LICENSE.

(in-package crypticl)

(defun print-external-symbols ()
  (do-external-symbols (symbl 'crypticl nil) 
    (print symbl)))

(defclass Crypto ()
  ((algorithm :accessor algorithm
	      :initarg :algorithm))
  (:documentation   
   "Common super class for all low-level crypto algorithms"))

(defmethod get-algorithm ((obj Crypto))
  (algorithm obj))

(defclass Hash (Crypto)
  ()
  (:documentation   
   "Common super class for all hash algorithms"))

(defclass Cipher (Crypto)
  ((padding :accessor padding))		;type checking in padding functions
  (:documentation   
   "Common super class for all Cipher algorithms"))

(defclass SymmetricCipher (Cipher)
  ()
  (:documentation   
   "Common super class for all Symmetric Cipher algorithms"))

(defclass AsymmetricCipher (Cipher)
  ()
  (:documentation   
   "Common super class for all Asymmetric Cipher algorithms"))

(defclass Signature (Crypto)
  ()
  (:documentation   
   "Common super class for all signature algorithms"))


;; Designed for using symbols as keys. Use :test #'equal with strings.
(defparameter *constructor-table* (make-hash-table :test #'equal))

(defun register-constructor (algorithm constructor)
  "register on both string value and symbol value. To use symbol value from an external package, prefix with package name, i.e. 'crypticl:AES"
  (setf (gethash algorithm *constructor-table*) constructor)
  (setf (gethash (symbol-name algorithm)
		 *constructor-table*) constructor))

(defun delete-constructor (algorithm)
  (remhash algorithm *constructor-table*))

(defun get-constructor (algorithm)
  (gethash algorithm *constructor-table*))

(defun display-algorithms ()
  (maphash #'(lambda (key value) (format t "~& ~A ~A" key value))
	   *constructor-table*))

(defun new-instance (algorithm)
  "Main function for getting new instances of an algorithm. "
  (do ((fun (get-constructor algorithm) (get-constructor algorithm)))
      (fun (apply fun '()))
    (restart-case (error  "new-instance: No such algorithm ~A implemented." algorithm)
      (store-value (value)
	  :report "Try another algorithm."
	  :interactive (lambda ()
			 (format t "~&New algorithm: ")
			 ;;(format t "(use 'SHA-1 or SHA-1, not \"SHA-1\"): ")
			 (list (read)))
	(typecase value
	  (cons (setf algorithm (second value))) ;input 'SHA-1
	  (string (setf algorithm value)) ;input "SHA-1" 
	  (symbol (setf algorithm value))))))) ;input SHA-1


(defun load-algorithm (&optional (path "des.lisp"))
  "Dynamic loading of new algorithms. The protocol new code must follow is illustraded in the file des.lisp."
  (compile-file path)
  (load path))




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Common functionality for hash functions.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun int32s-to-octet-vector (&rest int32s)
  "Make octet vector (byte array in C terms) from 32 bits integers. 

Note that SHA uses the big-endian convention so the least significant byte
of an integer is stored in the rightmost position in the byte array. 
This is the opposite of MD5."
  (let ((res (make-array (* 4 (length int32s))
			 :element-type '(unsigned-byte 8)
			 :fill-pointer 0)))
    (dolist (int32 int32s res)
      (vector-push (ldb (byte 8 24) int32) res)
      (vector-push (ldb (byte 8 16) int32) res)
      (vector-push (ldb (byte 8 8) int32) res)
      (vector-push (ldb (byte 8 0) int32) res))))

(defun 32-add (&rest args)
  "Adds a 32-bit number modulo (expt 2 32)"
  (ldb (byte 32 0) (reduce #'+ args)))


(defun left-rot-32 (a by)
  "Left rotation modulo 32. If number longer than 32 bits, ignore extra bits."
  (let ((pivot (- 32 by)))
    (dpb (ldb (byte pivot 0) a)
	 (byte pivot by)
	 (ldb (byte by pivot) a))))

(defun right-rot-32 (a by)
  "Right rotation modulo 32. If number longer than 32 bits, ignore extra bits.

1) Shift the (32 - by) left bits to the right by places
2) Shift the right by bits all over to the left.
"
  (let ((leftpart (- 32 by)))
    (+ (ash a (- by))
       (ash (ldb (byte by 0) a) leftpart))))

(defun make-string-reader-function (string)
  "Return a function that reads one byte at a time from a string or nil
when end-of-string. Note how let is used to carry state in the variables 
i and end between successive calls to string-reader, we are using a closure.
NB!! value of char-code depends on implementation. The same string can 
give different hash values in different implementations."
  (let ((i 0)
	(end (length string)))
    (flet ((string-reader ()
	     (if (< i end)
		 ;;XXX Can return more than a 8 bit value with Unicode strings
		 (prog1 (char-code (aref string i)) ;return this
		   (incf i))		;Can't use 1+
	       nil)))
      #'string-reader)))

(defun make-byte-array-reader-function-old (array &optional (end (length array)))
  "Return a reader that for each call returns the next byte from the array or 
nil if it reaches the end. Uses closure."
  (let ((i 0))
    (flet ((byte-array-reader ()
	     (if (< i end)
		 (prog1  (aref array i) ;return this
		   (incf i))		;Can't use 1+
	       nil)))
      #'byte-array-reader)))

(defun make-byte-array-reader-function (array &optional (in-end (length array)))
  "Return a reader that for each call returns the next byte from the array or 
nil if it reaches the end. Uses closure."
  (let ((i 0)
	(end in-end))
    (flet ((byte-array-reader ()
	     (if (< i end)
		 (prog1  (aref array i) ;return this
		   (incf i))		;Can't use 1+
	       nil)))
      #'byte-array-reader)))



;;;;;;
;;; MD5
;; Included here to bypass compiler error when compiling the 
;; md5-function-ffgghhii macro in md5.lisp.
(defparameter *random-sine-table*
	     (make-array 64
			 :element-type '(unsigned-byte 32)
			 :initial-contents
			 '(#xd76aa478 #xe8c7b756 #x242070db #xc1bdceee
			   #xf57c0faf #x4787c62a #xa8304613 #xfd469501
			   #x698098d8 #x8b44f7af #xffff5bb1 #x895cd7be
			   #x6b901122 #xfd987193 #xa679438e #x49b40821

			   #xf61e2562 #xc040b340 #x265e5a51 #xe9b6c7aa
			   #xd62f105d #x02441453 #xd8a1e681 #xe7d3fbc8
			   #x21e1cde6 #xc33707d6 #xf4d50d87 #x455a14ed
			   #xa9e3e905 #xfcefa3f8 #x676f02d9 #x8d2a4c8a

			   #xfffa3942 #x8771f681 #x6d9d6122 #xfde5380c
			   #xa4beea44 #x4bdecfa9 #xf6bb4b60 #xbebfbc70
			   #x289b7ec6 #xeaa127fa #xd4ef3085 #x04881d05
			   #xd9d4d039 #xe6db99e5 #x1fa27cf8 #xc4ac5665

			   #xf4292244 #x432aff97 #xab9423a7 #xfc93a039
			   #x655b59c3 #x8f0ccc92 #xffeff47d #x85845dd1
			   #x6fa87e4f #xfe2ce6e0 #xa3014314 #x4e0811a1
			   #xf7537e82 #xbd3af235 #x2ad7d2bb #xeb86d391)))

;;;;;;;;
;;;; Cipher

(defmethod pad-size ((obj Cipher) length-data block-size)
  "Parameters:
-block-size: size in octets for the cipher's blocks."
  (let ((padding (padding obj)))
    (case padding
      ('PKCS5 (- block-size (mod length-data block-size)))
      (otherwise (error "Unkown padding scheme ~A" padding)))))


(defmethod pad ((obj Cipher) data start size)
  (let ((padding (padding obj)))
    (case padding
      ('PKCS5 (PKCS5-pad data start size))
      (otherwise (error "Unkown padding scheme ~A" padding)))))

(defmethod remove-padding ((obj Cipher) data)
  (let ((padding (padding obj)))
    (case padding
      ('PKCS5 (PKCS5-remove-padding data))
      (otherwise (error "Unkown padding scheme ~A" padding)))))


(defun PKCS5-pad (data start size)
  " Do PKCS5 padding as defined in PKCS#5 from RSA.

Assume there is space in data for size octets starting at start."
  (dotimes (i size)
    (setf (aref data (+ start i)) size)))

(defun PKCS5-remove-padding (data)
  (if (adjustable-array-p data)
      (let* ((size (length data))
	     (last-octet (aref data (1- size))))
	(adjust-array data (- size last-octet)))

    (progn
      (warn "Array not adjustable in PKCS5-remove-padding.")
      data)))

;;;;;;;;
;;; Object encoding: Get a byte array representation useful for storing on 
;;; a file.
;;; Code from obol.lisp

(defun get-element-encodings (msg-elements)
  "Get byte encoding for each msg element, transform to base64 spki and return the bytes."
  (string-to-octets
   (to-spki (mapcar #'get-msg-element-encoding msg-elements))))

(defun to-spki(msg-elements)
  "(format-as-spki '(#(2 3) #(4 3))) => ((AgM=)(BAM=))"
  (mapc #'(lambda (msg-elm)
	    (assert (subtypep (type-of msg-elm) 'simple-array)))
	msg-elements)
  
  ;; Adds a : because we use the lisp reader when parsing in parse-spki
  ;; and then for instance a string with all numericals like 01010101...
  ;; would be seen as a bignum
  (let ((res
	 (apply #'concatenate 
		(append (list 'string "(")
			(mapcar #'(lambda (msg-elm)
				    (format nil "(:~A)" (crypticl:hex msg-elm)))
				msg-elements)
			(list ")")))))    
    res))


(defun get-msg-element-encoding (element)
  (typecase element
    (integer 
     (integer-to-octet-vector element))
    (simple-array 
     element)
    (t (error "~&get-msg-element-encoding:Unknown msg type=~A." 
	      (type-of element)))))

(defun parse-element (spki-element)
  "(...base64...)"
  (let ((str (car spki-element))) 
    (typecase str
      (symbol
       (crypticl:hexo (symbol-name (car spki-element))))
      (t 
       (error "~&parse-element: got ~A" (type-of str))))))

(defun parse-spki (spki-string)
  "Return a list of octet vectors."
  (mapcar #'parse-element (read-from-string spki-string)))

(defun construct-from-encoding (encoding type)
  "type=DSA"
  (mapcar 
   #'(lambda (octvec)
       (case type
	 ((DSA RSA Diffie-Hellman)
	  (octet-vector-to-integer octvec))
	 (t (error "Unknown type=~A" type))))	
   (parse-spki 
    (octets-to-string encoding))))


;;;;;;
;;; DIV
(defparameter *verbose* nil)

(defun formatv (&rest args)
  "Act as format if verbosity is set"
  (when *verbose*
    (apply #'format args)))



(defun vector-check (test correct &optional msg)
  (unless (equalp test correct)
    (format t "~&Wanted: ~A ~&Got: ~A" (hex correct) (hex test))
    (error msg)))

(defun xor-array (dst src &optional (start 0) (n (length src)))
  "xor two m*n arrays dst and src and change dst"
  (for (i 0 n)
       (setf (aref dst (+ start i)) (logxor (aref src i) 
					    (aref dst (+ start i))))))