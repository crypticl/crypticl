;;;;-*-lisp-*-
;;;; The Crypticl cryptographic library.
;;;;
;;;; Description: Test code for the library.
;;;; Author: Taale Skogan
;;;; Distribution:  See the accompanying file LICENSE.

(in-package crypticl)

(defun test-dynamic-load (&key keystore (path "des.lisp"))
  "Testing dynamic loading of new algorithms."
  (let ((ks (or keystore (new-instance 'KeyStore)))
	(dsa (new-instance 'DSA))
	(dsa2 nil)
	(codehash nil)                  ;Hash of the file containing the new code
	(signer "Ron Rivest")           ;Signer of the code.
	r s)

    ;; Simulate the author signing the code and creating a code certificate.
    ;; The certificate is represented by (signer,r,s, codehash) where r and s
    ;; is the algorithm specific signature part, in this case DSA with SHA-1.
    (setf codehash 
      (with-open-file (str path :direction :input)
	(sha-1-on-octet-stream str)))
    (multiple-value-setq (r s) (sign dsa nil :message-hash codehash))

    ;; Simulate sending the code and certificate from author to receiver
    ;; implicitly through the the variables codehash (=code) and 
    ;; (signer,r,s, codehash) (= certificate). This would normally happen
    ;; as part of a protocol excahnge.
    ()
    
    ;; Simulate obtaining and storing certificate signer's public key, maybe
    ;; from a second certificate from a TTP verifying the binding between the
    ;; code certificate's public key and the author of the code.
    (add-key ks (list signer) (get-public-key dsa))
    
    ;; Simultate verifying the certificate: Get the public key belonging to
    ;; the signer and use it to verify the certificate.
    (setf dsa2 (new-instance 'DSA))
    (init-verify dsa2 (nth 0 (get-dsa-public-keys ks signer)))
    
    (if (verify dsa2 r s nil :message-hash codehash)
	(progn 
	  (format t "~&Signature OK, loading new algorithm...")
	  (load-algorithm path))
      (format t "Bad signature for file ~A, aborting load." path))))


(defun run-tests()
  (test-SHA-1)
  (test-MD5)
  (test-AES)
  (test-RSA)
  (test-IDEA)
  (test-DSA)
  (test-SHA-256)
  (format t "~&All Crypticl load-time tests successful!"))

(run-tests)