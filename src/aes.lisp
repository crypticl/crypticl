;;;;-*-lisp-*-
;;;; The Crypticl cryptographic library.
;;;;
;;;; Description: AES
;;;; Author: Taale Skogan
;;;; Distribution:  See the accompanying file LICENSE.

#|
Implements the FIPS 197 standard (AES) of Rijndael. The code is based on the
reference implementation in [1] with some variable names from FIPS 197. 

TODO: The current version is very slow (too much consing). It needs a rewrite.

[1] Deamen,J. & Rijmen,V. 2002. "The Design of Rijndael - AES the Advanced 
Encryption Standard. Springer.
[2] NIST 2001.Advanced Encryption Standard. FIPS Pub 197.

AES uses 10,12 or 14 rounds.
|# 

(in-package crypticl)

(defparameter NB 4
  "'Num Bytes', the number of 32 bits words in the state = number of columns
in state. Equals variable BC in the reference implementation.")

(defparameter logtable
  #(  0   0  25   1  50   2  26 198  75 199  27 104  51 238 223   3
   100   4 224  14  52 141 129 239  76 113   8 200 248 105  28 193
   125 194  29 181 249 185  39 106  77 228 166 114 154 201   9 120
   101  47 138   5  33  15 225  36  18 240 130  69  53 147 218 142
   150 143 219 189  54 208 206 148  19  92 210 241  64  70 131  56
   102 221 253  48 191   6 139  98 179  37 226 152  34 136 145  16
   126 110  72 195 163 182  30  66  58 107  40  84 250 133  61 186
    43 121  10  21 155 159  94 202  78 212 172 229 243 115 167  87
   175  88 168  80 244 234 214 116  79 174 233 213 231 230 173 232
    44 215 117 122 235  22  11 245  89 203  95 176 156 169  81 160
   127  12 246 111  23 196  73 236 216  67  31  45 164 118 123 183
   204 187  62  90 251  96 177 134  59  82 161 108 170  85  41 157
   151 178 135 144  97 190 220 252 188 149 207 205  55  63  91 209
    83  57 132  60  65 162 109  71  20  42 158  93  86 242 211 171
    68  17 146 217  35  32  46 137 180 124 184  38 119 153 227 165
   103  74 237 222 197  49 254  24  13  99 140 128 192 247 112   7)
  )

(defparameter alogtable
  #(  1   3   5  15  17  51  85 255  26  46 114 150 161 248  19  53
    95 225  56  72 216 115 149 164 247   2   6  10  30  34 102 170
   229  52  92 228  55  89 235  38 106 190 217 112 144 171 230  49
    83 245   4  12  20  60  68 204  79 209 104 184 211 110 178 205
    76 212 103 169 224  59  77 215  98 166 241   8  24  40 120 136
   131 158 185 208 107 189 220 127 129 152 179 206  73 219 118 154
   181 196  87 249  16  48  80 240  11  29  39 105 187 214  97 163
   254  25  43 125 135 146 173 236  47 113 147 174 233  32  96 160
   251  22  58  78 210 109 183 194  93 231  50  86 250  21  63  65
   195  94 226  61  71 201  64 192  91 237  44 116 156 191 218 117
   159 186 213 100 172 239  42 126 130 157 188 223 122 142 137 128
   155 182 193  88 232  35 101 175 234  37 111 177 200  67 197  84
   252  31  33  99 165 244   7   9  27  45 119 153 176 203  70 202
    69 207  74 222 121 139 134 145 168 227  62  66 198  81 243  14
    18  54  90 238  41 123 141 140 143 138 133 148 167 242  13  23
    57  75 221 124 132 151 162 253  28  36 108 180 199  82 246   1)
    )

(defparameter Sbox
  #( 99 124 119 123 242 107 111 197  48   1 103  43 254 215 171 118
   202 130 201 125 250  89  71 240 173 212 162 175 156 164 114 192
   183 253 147  38  54  63 247 204  52 165 229 241 113 216  49  21
     4 199  35 195  24 150   5 154   7  18 128 226 235  39 178 117
     9 131  44  26  27 110  90 160  82  59 214 179  41 227  47 132
    83 209   0 237  32 252 177  91 106 203 190  57  74  76  88 207
   208 239 170 251  67  77  51 133  69 249   2 127  80  60 159 168
    81 163  64 143 146 157  56 245 188 182 218  33  16 255 243 210
   205  12  19 236  95 151  68  23 196 167 126  61 100  93  25 115
    96 129  79 220  34  42 144 136  70 238 184  20 222  94  11 219
   224  50  58  10  73   6  36  92 194 211 172  98 145 149 228 121
   231 200  55 109 141 213  78 169 108  86 244 234 101 122 174   8
   186 120  37  46  28 166 180 198 232 221 116  31  75 189 139 138
   112  62 181 102  72   3 246  14  97  53  87 185 134 193  29 158
   225 248 152  17 105 217 142 148 155  30 135 233 206  85  40 223
   140 161 137  13 191 230  66 104  65 153  45  15 176  84 187  22)
  "S-box.")

(defparameter Si
  #( 82   9 106 213  48  54 165  56 191  64 163 158 129 243 215 251
   124 227  57 130 155  47 255 135  52 142  67  68 196 222 233 203
    84 123 148  50 166 194  35  61 238  76 149  11  66 250 195  78
     8  46 161 102  40 217  36 178 118  91 162  73 109 139 209  37
   114 248 246 100 134 104 152  22 212 164  92 204  93 101 182 146
   108 112  72  80 253 237 185 218  94  21  70  87 167 141 157 132
   144 216 171   0 140 188 211  10 247 228  88   5 184 179  69   6
   208  44  30 143 202  63  15   2 193 175 189   3   1  19 138 107
    58 145  17  65  79 103 220 234 151 242 207 206 240 180 230 115
   150 172 116  34 231 173  53 133 226 249  55 232  28 117 223 110
    71 241  26 113  29  41 197 137 111 183  98  14 170  24 190  27
   252  86  62  75 198 210 121  32 154 219 192 254 120 205  90 244
    31 221 168  51 136   7 199  49 177  18  16  89  39 128 236  95
    96  81 127 169  25 181  74  13  45 229 122 159 147 201 156 239
   160 224  59  77 174  42 245 176 200 235 187  60 131  83 153  97
    23  43   4 126 186 119 214  38 225 105  20  99  85  33  12 125)
    "Inverse S-box.")

(defun mul (a b)
  "Multiply two elements of GF(256) by table lookup."
  (if (and (not (= a 0))
	   (not (= b 0)))
      (aref alogtable (mod (+ (aref logtable a)
			    (aref logtable b))
			   255))
    0))

(defun add-round-key (state round-key)
  "XOR state with round key. state and round-key is 4*NB matrices."
  (for (i 0 4)
       (for (j 0 NB)
	    (setf (aref state i j) 
	      (logxor (aref state i j)
		      (aref round-key i j))))))

(defun sub-bytes (state s-box)
  "Substitute byte in input by corresponding byte in S box or inverse S box"
  (for (i 0 4)
       (for (j 0 NB)
	    (setf (aref state i j) (aref s-box (aref state i j))))))

(defun shift-rows (state do-encrypt)
  "Rotate rows in 4*NB state. If do-encrypt is true, rotate left, else rotate right (decryption)."
  (let ((tmp (make-array NB)))
    (if do-encrypt
	(for (i 1 4)			;for each row
	     (for (j 0 NB)
		  (setf (aref tmp j)
		    (aref state i (mod (+ j i) NB)))) ;rotate left i slots
	     (for (k 0 NB)
		  (setf (aref state i k) (aref tmp k))))
      
      ;;decryption
      (for (i 1 4)			;for each row
	     (for (j 0 NB)
		  (setf (aref tmp j)
		    (aref state i (mod (- j i) NB)))) ;rotate right i slots
	     (for (k 0 NB)
		  (setf (aref state i k) (aref tmp k)))))))

(defun mix-columns (state)
  "Mix the four bytes in each column of the 4*NB matrix state"
  (let ((b (make-array  (list 4 NB))))
    (for (j 0 NB)
	 (for (i 0 4)
	      (setf (aref b i j)
		(logxor
		 (mul 2 (aref state i j))
		 (mul 3 (aref state (mod (+ i 1) 4) j))
		 (aref state (mod (+ i 2) 4) j)
		 (aref state (mod (+ i 3) 4) j)))))
    
    (copy-array-2d state b 4 NB)))
	
(defun inv-mix-columns (state)
  "Reverse of mix-columns"
  (let ((b (make-array  (list 4 NB))))
    (for (j 0 NB)
	 (for (i 0 4)
	      (setf (aref b i j)
		(logxor
		 (mul #xe (aref state i j))
		 (mul #xb (aref state (mod (+ i 1) 4) j))
		 (mul #xd (aref state (mod (+ i 2) 4) j))
		 (mul #x9 (aref state (mod (+ i 3) 4) j))))))
    
    ;;Copy back
    (for (i 0 4)
	 (for (j 0 NB)
	      (setf (aref state i j) (aref b i j))))))
			      

(defun get-Nk (key-length)
  "Get the Rijndael parameter Nk which is a function of the key length.
Either 4, 6 or 8 corresponding to key length 128, 192 and 256 respectively."
  (cond ((= key-length 16) 4)
	((= key-length 24) 6)
	((= key-length 32) 8)
	(t (error "Invalid key length ~A" key-length))))

(defun get-num-rounds (key-length)
  "Find number of rounds for given key length (128, 192 or 256 bits)"
  (cond ((= key-length 16) 10)
	((= key-length 24) 12)
	((= key-length 32) 14)
	(t (error "Invalid key length ~A" key-length))))

(defun change-key-format (key Nk)
  "Change format from byte array to 4*Nk array"
  (let ((new-key (make-array (list 4 Nk) :initial-element 0))
        (offset 0))
    (for (i 0 Nk)
	 (for (j 0 4)
	      (setf (aref new-key j i) (aref key offset))
	      (incf offset)))
    new-key))

(defun get-block (block data offset)
  "Get next 16 bytes block from octet-vector data starting at offset and write it to block using aes style (columns first order meaning that the first column is filled completely before the next.

AES block size is always 128-bits/16 bytes.
"
  (let ((k offset))
    (for (i 0 4)
	 (for (j 0 4)
	      (setf (aref block j i) (aref data k))
	      (incf k)))))

(defun copy-array-3d-fixed (dst src fixed row col)
  "Copy from 3D fixed*row*col src to row*col dst array."
  (for (i 0 row)
       (for (j 0 col)
	    (setf (aref dst i j) (aref src fixed i j)))))

(defun sbox-byte (b)
  "Apply sbox to a byte"  
  (aref Sbox b))

(defparameter Rcon #(#x8d #x01 #x02 #x04 #x08 #x10 #x20 #x40 #x80
                     #x1b #x36 #x6c #xd8 #xab #x4d #x9a #x2f
                     #x5e #xbc #x63 #xc6 #x97 #x35 #x6a #xd4
                     #xb3 #x7d #xfa #xef #xc5 #x91)
  "Rijndael round constants used in key scheduling.")

(defun rot-word (word)
  "Rotate word left one byte"
  (let ((ret (make-byte-array 4)))
    (dotimes (i 4 ret)
      (setf (aref ret i) (aref word (mod (+ i 1) 4))))))

(defun sub-word (word)
  "Apply sbox to each byte"
  (let ((ret (make-byte-array 4)))
    (dotimes (i 4 ret)
      (setf (aref ret i) (sbox-byte (aref word i))))))
    
(defun aes-key-expansion (key &key debug)
  "Return the key expansion for a AES key. 

Note that NB equals BC and Nk equals KC in the reference code in [1]. 
w is the resulting key expansion with dimensions ROUNDS+1 * 4 * NB.

Parameters:
key: is the original 128, 192 or 256 bits key given as a byte array.

Return:
w:   the key expansion (make-array (list (1+ Nr) 4 NB))
"
  (let* ((key-size (length key))
         (Nk (get-Nk key-size))
	 (num-rounds (+ 1 (get-num-rounds key-size)))
         (num-words (* NB num-rounds))
         (num-bytes (* 4 num-words))         
	 (prev (make-byte-array 4))    ; prev word
	 (words (make-byte-array num-bytes))
         i)                             ; current word

    ;; Copy key into the first slot of the key expansion
    (acopy key :out words)

    (setf i Nk)
    (while (< i num-words)
      (when debug
        (format t "i = ~A prev - sub - rcon xor - i-Nk ~%" i))
      ;; Grab previous word in key expansion      
      (acopy words :start (* (- i 1) 4) :size 4 :out prev)
      (when debug
        (format t " ~A" (hex prev)))
      
      (cond
       ((= (mod i Nk) 0)
        (setf prev (sub-word (rot-word prev)))
        (when debug
          (format t " ~A" (hex prev)))
        ;; Apply round constant to first byte
        (setf (aref prev 0) (logxor (aref prev 0)
                                    (aref Rcon (/ i Nk))))
        (when debug
          (format t " ~A" (hex prev)))
        )
       
       ((and (> Nk 6) (= (mod i Nk) 4))        
        (setf prev (sub-word prev))
        (when debug
          (format t " ~A" (hex prev))
          (format t " -------- ")))       ; no rcon
       (t
        (when debug
          (format t " -------- ")       ; no sub
          (format t " -------- "))))    ; no rcon  

      (let ((i-Nk (get-word words (- i Nk))))
        (when debug
          (format t " ~A" (hex i-Nk)))
        (xor-array prev i-Nk))
      
      (acopy prev :out words :out-start (* 4 i))
      (when debug
        (format t "  word ~,2R: ~A~%" i (hex-word words i)))
      (incf i))

    ;; Change format (XXX horribly inefficient!)
    (let ((new (make-array (list num-rounds 4 NB)))
          (offset 0))
      (dotimes (slot num-rounds new)
        (for (col 0 4)
             (for (row 0 4)
                  (setf (aref new slot row col) (aref words offset))
                  (incf offset)))))))

       
(defun hex-word (words i)
  (hex (get-word words i)))

(defun get-word (words i)
  (acopy words :start (* 4 i) :size 4))
  
(defun test-key-exp (&key key)
  (let ((key (or key (hexo "2b7e151628aed2a6abf7158809cf4f3c")))
        (256-key (hexo "603deb1015ca71be2b73aef0857d77811f352c073b6108d72d9810a30914dff4"))
        words)
    (declare (ignore 256-key))
    (setf words (aes-key-expansion key :debug t))    
    (for (i 0 (/ (length words) 4))
         (format t "word ~,2R: ~A~%" i (hex-word words i)))))
	       
(defun aes-encrypt-block (state round-key ROUNDS)
  "Encrypt one 128 bit block"
  ;;Hack to get sub-array. Terribly inefficient
  (let ((rk (make-array (list 4 NB))))
    (flet ((fill-rk (round)
	     (for (col 0 NB)
		  (for (row 0 4)
		       (setf (aref rk row col) 
			 (aref round-key round row col))))))
		

      (fill-rk 0)
      (add-round-key state rk)
  
      (for (r 1 ROUNDS)
	   (sub-bytes state Sbox)
	   (shift-rows state t)
	   (mix-columns state)
	   (fill-rk r)
	   (add-round-key state rk))
      
      ;;Last round is special. No mix-columns
      (sub-bytes state Sbox)
      (shift-rows state t)
      (fill-rk ROUNDS)
      (add-round-key state rk))))

(defun aes-decrypt-block (state round-key ROUNDS)
  "Decrypt one 128 bit block"
  ;;Hack to get sub-array. Terribly inefficient
  (let ((rk (make-array (list 4 NB))))
    (flet ((fill-rk (round)
	     (for (col 0 NB)
		  (for (row 0 4)
		       (setf (aref rk row col) 
			 (aref round-key round row col))))))
		

      (fill-rk ROUNDS)
      (add-round-key state rk)
      (sub-bytes state Si)
      (shift-rows state nil)
          
  
      (do ((r (1- ROUNDS) (1- r)))
	  ((<= r 0))
	(fill-rk r)
	(add-round-key state rk)
	(inv-mix-columns state)
	(sub-bytes state Si)
	(shift-rows state nil))
	
      ;;Last key addition
      (fill-rk 0)
      (add-round-key state rk))))

(defun aes-encrypt-octet-vector (data key mode &optional iv)
  "Wrapper for encryption."
  (aes-crypt-octet-vector data key mode t iv))

(defun aes-decrypt-octet-vector (data key mode &optional iv)
  "Wrapper for decryption"
  (aes-crypt-octet-vector data key mode nil iv))

(defun aes-crypt-octet-vector (data key mode doEncrypt &optional iv)
  "Encrypt data with key in given mode using an iv if necessary. Assumes data is a multiple of the block lenght (16 bytes = 128 bits).
-mode: 'ecb'
-key: byte array 
-doEncrypt is a boolean that chooses between decryption or encryption
"
  (assert (/= 0 (length data)))
  (assert (= 0 (mod (length data) 16)))
  
  (let ((num-rounds (get-num-rounds (length key)))
	(round-key (aes-key-expansion key)))
    
    (cond ((eq mode 'ecb)
	   (aes-ecb-mode data round-key num-rounds doEncrypt))
	  ((eq mode 'cbc)
	   (aes-cbc-mode data round-key num-rounds doEncrypt iv))
          ((eq mode 'ctr-onetime)
	   (aes-generate-one-time-pad-ctr data round-key num-rounds))
	  (t (error "No such mode ~A" mode)))))

(defun aes-generate-one-time-pad-ctr (data round-key num-rounds)
  "data is the counter"
  (let ((encrypted-block (make-array '(4 4)))
        (offset 0))
    
    ;;    (format t "input bytes = ~A~%" (hex data))
    (while (< offset (length data))
      ;; Do one 16 bytes block at a time
      (get-block encrypted-block data offset)
      (aes-encrypt-block encrypted-block round-key num-rounds)
      (copy-back-block encrypted-block data offset)
      ;;      (format t "new bytes = ~A~%" (hex data))  
      (setf offset (+ 16 offset)))
    ))


(defun aes-cbc-mode (data round-key num-rounds doEncrypt iv)
  "Wrapper for CBC mode encryption and decryption"
  (assert (not (equal iv nil)))
  (if doEncrypt
      (aes-cbc-mode-encrypt data round-key num-rounds iv)
    (aes-cbc-mode-decrypt data round-key num-rounds iv)))


(defun aes-cbc-mode-decrypt (data round-key num-rounds iv)
  "Do AES decryption in Cipher Block Chaining (CBC) mode.
Parameters:
-round-key: the expanded key.
-iv: initialization vector"
  (let ((len (length data))
	(block (make-array '(4 4)))
	(tmp-block (make-array '(4 4)))
	(iv2 (change-key-format iv 4))) ;Change format from vector to 2D array
    
    (do* ((offset 0 (+ offset 16)))
	((= offset len) (copy-back-block iv2 iv))
      (get-block block data offset)
      (copy-array-2d tmp-block block 4 4) ;save block for use as next iv2
      (aes-decrypt-block block round-key num-rounds)
      (xor-array-2D block iv2 4 4)
      (copy-array-2d iv2 tmp-block 4 4)		;update iv2
      (copy-back-block block data offset))))


(defun aes-cbc-mode-encrypt (data round-key num-rounds iv)
  "Do AES encryption in Cipher Block Chaining (CBC) mode.
Parameters:
-round-key: the expanded key.
-iv: initialization vector
Returns:
-updates iv by side-effect
"
  (let ((len (length data))
	(block (make-array '(4 4)))
	(iv2 (change-key-format iv 4))) ;Change format from vector to 2D array
    
    (do* ((offset 0 (+ offset 16)))
	((= offset len) (copy-back-block iv2 iv))
      (get-block block data offset)
      (xor-array-2D block iv2 4 4)
      (aes-encrypt-block block round-key num-rounds)
      (copy-array-2d iv2 block 4 4)	;update iv2
      (copy-back-block block data offset))))



(defun aes-ecb-mode (data round-key num-rounds doEncrypt)
  "Do AES ecb mode.
Parameters:
-round-key: the expanded key.
-doEncrypt is a boolean that chooses between decryption or encryption"
  (do* ((offset 0 (+ offset 16))
	(block (make-array '(4 4)))
	(len (length data)))
      ((= offset len))
    (get-block block data offset)
    (if doEncrypt
	(aes-encrypt-block block round-key num-rounds)
      (aes-decrypt-block block round-key num-rounds))
    (copy-back-block block data offset)))

(defun copy-back-block (block data &optional (offset 0))
  "Copy block back into data"
  (let ((k offset))
    (for (i 0 4)
	 (for (j 0 4)
	      (setf (aref data k) (aref block j i))
	      (incf k)))))
	
(defun xor-array-2D (dst src m n)
  "xor two m*n arrays dst and src and change dst"
  (for (i 0 m)
       (for (j 0 n)
	    (setf (aref dst i j) (logxor (aref src i j)
					 (aref dst i j))))))

(defun copy-array-2d (dst src row col)
  "Copy row*col array from src to dst"
  (for (i 0 row)
       (for (j 0 col)
	    (setf (aref dst i j) (aref src i j)))))

;;;;;;; CLOS

(defclass AES (SymmetricCipher)
  ((key :accessor key)
   (mode :accessor mode)
   (padding :accessor padding)
   (iv :accessor iv)
   (encryptp :accessor encryptp)	;true if initialized for encryption.
   (leftover-octets :accessor leftover-octets ;unprocessed octets
		    :initform (make-array 16 :element-type '(unsigned-byte 8)))
   (leftover-count :accessor leftover-count ;number of unprocessed octets
		   :initform 0)))

(defun make-AES ()
  "Constructor for the AES class."
  (make-instance 'AES :algorithm "AES"))

(defmethod init-encrypt ((obj AES) key
			 &key (mode 'cbc) iv  (padding 'PKCS5))
  ;; default iv
  (unless iv
    (setf iv #16(0)))
  (init obj key :mode mode :iv iv :encryptp t :padding padding))

(defmethod init-decrypt ((obj AES) key
			 &key (mode 'cbc) iv  (padding 'PKCS5))
    ;; default iv
  (unless iv
    (setf iv #16(0)))
  (init obj key :mode mode :iv iv :encryptp nil :padding padding))

(defmethod init ((obj AES) key 
		 &key (mode 'cbc) iv (encryptp t) (padding 'PKCS5))
  ;; Extract raw key if we got key object
  (typecase key
    (Key (setf (key obj) (key key)))
    (t (setf (key obj) key)))
  (setf (mode obj) mode)
  (setf (padding obj) padding)
  (setf (iv obj) (octet-vector-copy iv))
  (setf (encryptp obj) encryptp)
  (setf (leftover-count obj) 0))

(defmethod aes-store-state ((obj AES) data offset end)
  (let ((leftover-offset (leftover-count obj))
	(octets-left (- end offset)))

    ;; We know there are less than (16 - leftover-count) octets left so
    ;; they all fit in the leftover-octets vector in obj.
    (dotimes (i octets-left) 
      (setf (aref (leftover-octets obj) (+ leftover-offset i)) 
	(aref data (+ offset i))))
    
    (setf (leftover-count obj) (+ leftover-offset octets-left))))

(defmethod aes-update ((obj AES) data)
  "parameters: -data: octet-vector with length a multiple of 16."
  (aes-crypt-octet-vector data (key obj) (mode obj) (encryptp obj) (iv obj)))

(defmethod update ((obj AES) data &optional (start 0) (end (length data)))
  "Return nil if no data was encrypted or decrypted."
  ;; Add previous data to the front. Store leftover octets and use
  ;; data of length a multiple of 16.
  (let ((size (+ (leftover-count obj) (- end start))))
    ;; Prepare output vector if we have enough data
    (if (>= size 16)
	(let* ((offset 0)		;offset in out vector
	       (rem (mod size 16))	;leftover octets
	       (out (make-array (- size rem) :element-type '(unsigned-byte 8))))      
	  ;; Get leftover octets from previous calls to update.
	  (dotimes (i (leftover-count obj))
	    (setf (aref out i) (aref (leftover-octets obj) i))
	    (incf offset))
	      
	  ;; No leftover octets left so we reset the leftover count...
	  (setf (leftover-count obj) 0)
	  ;; ...and store any new leftover octets.
	  (aes-store-state obj data (- end rem) end)
	      
	  ;; Get new data
	  (octet-vector-copy data start (- end rem) out offset)
	      
	  ;; out is now a multiple of 16 octets so call low level primitive
	  (aes-update obj out)
	  out)
      (progn
	(aes-store-state obj data start end)
	nil))))

(defmethod update-and-encrypt ((obj AES) data 
			       &optional (start 0) (end (length data)))
  ;; Add previous data to the front. Pad or strip leftover octets and use
  ;; data of length a multiple of 16.
  (let* ((init-size (+ (leftover-count obj) (- end start)))
	 (pad-size (pad-size obj init-size 16))
	 (size (+ init-size pad-size))
    	 (offset 0)			;offset in out vector
	 (out (make-array size :element-type '(unsigned-byte 8))))
    
    ;; Get leftover octets from previous calls to update.
    (dotimes (i (leftover-count obj))
      (setf (aref out i) (aref (leftover-octets obj) i))
      (incf offset))
      
    ;; No leftover octets left so we reset the leftover count.
    (setf (leftover-count obj) 0)
	
    ;; Get new data
    (when data
      (octet-vector-copy data start end out offset))
	
    ;; Add padding
    (pad obj out (- size pad-size) pad-size)

    ;; out is now a multiple of 16 octets so call low level primitive
    (aes-update obj out)
    out))

(defmethod update-and-decrypt ((obj AES) data 
			   &optional (start 0) (end (length data)))
  ;; Add previous data to the front. Pad or strip leftover octets and use
  ;; data of length a multiple of 16.
  (let* ((init-size (+ (leftover-count obj) (- end start)))
	 (size init-size)
    	 (offset 0)			;offset in out vector
	 (out (make-array size 
			  :adjustable t
			  :element-type '(unsigned-byte 8))))
    
    ;; Get leftover octets from previous calls to update.
    (dotimes (i (leftover-count obj))
      (setf (aref out i) (aref (leftover-octets obj) i))
      (incf offset))
      
    ;; No leftover octets left so we reset the leftover count.
    (setf (leftover-count obj) 0)
	
    ;; Get new data
    (when data
      (octet-vector-copy data start end out offset))

    ;; data is a multiple of 16 octets so call low level primitive
    (aes-update obj out)
    
    ;; Remove padding.
    (remove-padding obj out)))

(defmethod encrypt ((obj AES) &optional data (start 0) (end (length data)))
  (update-and-encrypt obj data start end))

(defmethod decrypt ((obj AES) &optional data (start 0) (end (length data)))
  (update-and-decrypt obj data start end))
     
   
;;;;;;;
;;; Test suite

(defun str-check (str1 str2 error-msg)
  "Signal error if the two strings are different."
  (when (string/= str1 str2)
    (error error-msg)))


(defun aes-test-suite-CLOS ()
  (let* ((clear-org			;testing un-aligned, long data
	  "00112233445566778899aabbccddeeff00112233445566778899aabbccdd")
	 (key-org
	 "000102030405060708090a0b0c0d0e0f")
	 (iv-org 
	 "01010101010101010101010101010101")
	 (encrypted
	  "a9541c06f1c21125e44013531e18f40682ffe0983d47361887c2b1d2a0bdf077")
	 (start-encrypted
	  "a9541c06f1c21125e44013531e18f406")
	 (final-encrypted
	  "82ffe0983d47361887c2b1d2a0bdf077")
	 (aa (make-AES))
	 (tmp nil)
	 (tmp2 nil)
	 (clear (hexo clear-org))
	 (key (hexo key-org))
	 (iv (hexo iv-org)))
    (format t "~&AES CLOS...")
    
    (formatv t "~&Clear text:~%~A" (hex clear))
    (init-encrypt aa key :iv iv)
    (setf tmp (encrypt aa clear))
    (formatv t "~&Encrypted:~%~A" (hex tmp))
    (str-check (hex tmp) encrypted "AES encryption")
    
    (init-decrypt aa key :iv (hexo iv-org))
    (setf tmp (decrypt aa tmp))
    (formatv t "~&Decrypted:~%~A" (hex tmp))
    (str-check (hex tmp) clear-org "AES decryption") 
    
    ;; Testing update version of encrypt and decrypt.
    (formatv t "~&Update + encrypt:")
    (init-encrypt aa key :iv (hexo iv-org))
    (setf tmp (update aa clear))
    (formatv t "~&Start encrypted:~%~A" (hex tmp))
    (str-check (hex tmp) start-encrypted "AES start encrypted")
    (setf tmp2 (encrypt aa))
    (formatv t "~&Final encrypted:~%~A" (hex tmp2))
    (str-check (hex tmp2) final-encrypted "AES final encrypted")
    
    (formatv t "~&Update + decrypt:")
    (formatv t "~&Cipher text:~%~A" (hex (concat tmp tmp2)))
    (init-decrypt aa key :iv (hexo iv-org))
    (setf tmp (update aa tmp))
    (formatv t "~&Start decrypted:~%~A" (hex tmp))
    (setf tmp2 (decrypt aa tmp2))
    (formatv t "~&Final decrypted:~%~A" (hex tmp2))
    (formatv t "~&New clear text:~%~A" (hex (concat tmp tmp2)))
    (str-check (hex (concat tmp tmp2)) clear-org 
	       "AES decryption update + encrypt")

    (format t "OK.")))		


(defun aes-test-suite-fips-197 ()
  "Offical FIPS 197 test vector (for ECB mode obviously)."
  (format t "~&AES on official FIPS 197 test vector...")
  (let* ((fips-clear "00112233445566778899aabbccddeeff")
	 (fips-key "000102030405060708090a0b0c0d0e0f")
	 (fips-ciphertext "69c4e0d86a7b0430d8cdb78070b4c55a")
	 (clear (hexo fips-clear))
	 (key (hexo fips-key)))
    (aes-encrypt-octet-vector clear key 'ecb)
    (if (string/= (hex clear) fips-ciphertext)
	(progn
	  (format t "~&Got:~A ~&Wanted:~A" (hex clear) fips-ciphertext)
	  (error "AES on FIPS 197 test vector"))
      (format t "OK."))))

(defun aes-test-suite-long ()
  "Testing long vector."
  (let ((aa (make-AES))
	(tmp nil)
	(key #16(2)) ;(generate-key 'AES 128)) Use a fixed key
	(iv #16(2))
	(clear #500(3)))
    (format t "~&AES long vector...")
    (init-encrypt aa key :iv iv)
    (setf tmp (encrypt aa clear))
    (init-decrypt aa key :iv iv)
    (setf tmp (decrypt aa tmp))
    (assert (equalp tmp clear) ()
      "AES long decryption and decryption")
    (format t "OK.")))

(defun test-AES ()
  (aes-test-suite-fips-197)
  (aes-test-suite-CLOS)
  (aes-test-suite-long))

;;;;;;;;
;;; Debug helpers

;;;(defun hex-print (a)
;;;  "Hex print 4*NB array"
;;;  (let ((hex-string ""))
;;;    (for (i 0 NB)
;;;	 (for (j 0 NB)
;;;	      (setf hex-string 
;;;		(concatenate 'string hex-string 
;;;			     (format nil "~2,'0X" (aref a j i))))))
;;;    (format nil "~A" hex-string)))
;;;
;;;		      
;;;(defun hex-print-3D (a)
;;;  "Hex print (rounds+1)4*NB array. For pretty printing the key expansion."
;;;  (let ((hex-string ""))
;;;    (for (r 0 (array-dimension a 0))
;;;	 (for (i 0 (array-dimension a 1))
;;;	      (for (j 0 (array-dimension a 2))
;;;		   (setf hex-string 
;;;		     (concatenate 'string hex-string 
;;;				  (format nil "~2,'0X" (aref a r j i))))))
;;;	 (setf hex-string 
;;;		     (concatenate 'string hex-string 
;;;				  (format nil "~%"))))
;;;    (print hex-string)))


;;; Test vectors
;;; from http://csrc.nist.gov/CryptoToolkit/aes/rijndael/rijndael-vals.zip
;;;
(defun aes-rijndael-testvectors (stream)
  (parse-rijndael-lines
   (loop for line = (read-line stream nil 'eof)
       until (eq line 'eof)	    
       collect line))
  (format t "AES-Rijndael original tests OK"))

(defun parse-rijndael-lines (lines)
  "Parse the aes-rijndael-testvectors file format.

file format:
I=0
KEY=00000000000000000000000000000000
PT=00000000000000000000000000000000
CT=C34C052CC0DA8D73451AFE5F03BE297F

I=1
KEY=C34C052CC0DA8D73451AFE5F03BE297F
PT=C34C052CC0DA8D73451AFE5F03BE297F
CT=0AC15A9AFBB24D54AD99E987208272E2
"
  (let ((state 'read-more)
	(count 0)
	key
	pt
	ct)	 
    (dolist (line lines)
      (cond 
       ((string-startswith line "KEY")
	(setf key (subseq line 4)))
       ((string-startswith line "PT")
	(setf pt (subseq line 3)))
       ((string-startswith line "CT")
	(setf ct (subseq line 3)
	      state 'new-test-case))
       (t
	(setf state 'read-more)))

      (case state
        ;; continue reading
	(read-more nil)			
        ;; we have a new test vector
	(new-test-case (aes-test-ecb key pt ct)
                       (incf count)
                       (setf state 'read-more
                             key nil
                             pt nil
                             ct nil))	
	))
    
    (format t "Tested ~D test vectors~%" count)
    ))

(defun aes-test-ecb (key pt ct)
  "Input as hex strings"
  (let ((clear (hexo pt)))
    (aes-encrypt-octet-vector clear (hexo key) 'ecb)
    (when (string/= (hex clear) (string-downcase ct))
      (format t "~&Got:~A ~&Wanted:~A" (hex clear) ct)
      (error "AES test vector"))))

(defun run-aes-rijndael-testvectors (&optional 
                                     (path "../etc/aes_testvectors/rijndael-vals/ecb_tbl.txt"))
  (with-open-file (str path)
    (aes-rijndael-testvectors str)))


(defun parse-aes-cbc-lines (lines)
  "Parse the aes-rijndael-testvectors file format.

file format:
I=0
KEY=00000000000000000000000000000000
IV=00000000000000000000000000000000
PT=00000000000000000000000000000000
CT=8A05FC5E095AF4848A08D328D3688E3D

I=1
KEY=8A05FC5E095AF4848A08D328D3688E3D
IV=8A05FC5E095AF4848A08D328D3688E3D
PT=204F17E2444381F6114FF53934C0BCD3
CT=192D9B3AA10BB2F7846CCBA0085C657A
"
  (let ((state 'read-more)
	(count 0)
	key
        iv
	pt
	ct)	 
    (dolist (line lines)
      (cond 
       ((string-startswith line "KEY")
	(setf key (subseq line 4)))
       ((string-startswith line "IV")
	(setf iv (subseq line 3)))
       ((string-startswith line "PT")
	(setf pt (subseq line 3)))
       ((string-startswith line "CT")
	(setf ct (subseq line 3)
	      state 'new-test-case))
       (t
	(setf state 'read-more)))

      (case state
        ;; continue reading
	(read-more nil)			
        ;; we have a new test vector
	(new-test-case (aes-test-cbc key pt ct iv count)
                       (incf count)
                       (setf state 'read-more
                             key nil
                             iv nil
                             pt nil
                             ct nil))	
	))
    
    (format t "Tested ~D test vectors~%" count)
    ))

(defun aes-test-cbc (key pt ct iv &optional count)
  "Input as hex strings"
  (let* ((KEY (hexo key))
         (PT (hexo pt))
         (CT (make-byte-array 16))
         (CV (hexo iv))
         ;; Don't compute round key more than once
         (num-rounds (get-num-rounds (length KEY)))
         (round-key (aes-key-expansion KEY)))
    
    (dotimes (j 10000)
      (xor-array PT CV)                 ; xor with iv      
      (aes-ecb-mode PT round-key num-rounds t)
      ;;(aes-encrypt-octet-vector PT KEY 'ecb)
      
      ;; PT is now encrypted (= CT)
      (acopy PT :out CT)
      ;; next plaintext is previous cipher text which
      ;; happens to be stored in CV
      (acopy CV :out PT)
      (acopy CT :out CV))
    (format t "CT ~A: ~A~%" count (string-upcase (hex CT)))
    (when (string/= (hex CT) (string-downcase ct))
      (format t "~&Got:~A ~&Wanted:~A" (hex CT) ct)
      (error "AES CBC test vector"))))

(defun run-aes-cbc-test (&optional (path "../etc/aes_testvectors/rijndael-vals/cbc_e_m.txt"))
  (with-open-file (str path)
    (parse-aes-cbc-lines
     (loop for line = (read-line str nil 'eof)
         until (eq line 'eof)	    
         collect line)))
  
  (format t "AES-Rijndael CBC Encryption tests OK"))


(register-constructor 'AES #'make-AES)
