;;;;-*-lisp-*-
;;;; The Crypticl cryptographic library.
;;;;
;;;; Description: The RSA MD5 message digest algorithm from Internet RFC 1321.
;;;; Author: Taale Skogan
;;;; Distribution:  See the accompanying file LICENSE.
;;;; Credit: Low level code based on implementation by Mark Nahabedian with 
;;;;         enhancements by Tony Eng.

(in-package crypticl)

(defmacro initialize-md5-state (a b c d)
  `(setf ,a #x67452301
	 ,b #xefcdab89
	 ,c #x98badcfe
	 ,d #x10325476))


;;;;;;;;;;;;;
;;; HELPERS

(defun 32-not (a)
  "Return bitwise not of the low order 32 bits"
  (ldb (byte 32 0) (lognot a)))

(defun 32-or (a b)
  "Return bitwise or of the low order 32 bits"
  (ldb (byte 32 0) (logior a b)))

(defun 32-xor (a b)
  "Returns bitwise xor of the low order 32 bits"
  (ldb (byte 32 0) (logxor a b)))

(defun 32-and (a b)
  "Returns bitwise and of the low order 32 bits"
  (ldb (byte 32 0) (logand a b)))



;;;;;;;
;;; Low level MD5 functions

;; Also included in common.lisp to bypass compiler error when compiling 
;; the md5-function-ffgghhii macro.

(unless (boundp '*random-sine-table*)
  (defparameter *random-sine-table*
      (make-array 64
		  :element-type '(unsigned-byte 32)
		  :initial-contents
		  '(#xd76aa478 #xe8c7b756 #x242070db #xc1bdceee
		    #xf57c0faf #x4787c62a #xa8304613 #xfd469501
		    #x698098d8 #x8b44f7af #xffff5bb1 #x895cd7be
		    #x6b901122 #xfd987193 #xa679438e #x49b40821

		    #xf61e2562 #xc040b340 #x265e5a51 #xe9b6c7aa
		    #xd62f105d #x02441453 #xd8a1e681 #xe7d3fbc8
		    #x21e1cde6 #xc33707d6 #xf4d50d87 #x455a14ed
		    #xa9e3e905 #xfcefa3f8 #x676f02d9 #x8d2a4c8a
		    
		    #xfffa3942 #x8771f681 #x6d9d6122 #xfde5380c
		    #xa4beea44 #x4bdecfa9 #xf6bb4b60 #xbebfbc70
		    #x289b7ec6 #xeaa127fa #xd4ef3085 #x04881d05
		    #xd9d4d039 #xe6db99e5 #x1fa27cf8 #xc4ac5665
		    
		    #xf4292244 #x432aff97 #xab9423a7 #xfc93a039
		    #x655b59c3 #x8f0ccc92 #xffeff47d #x85845dd1
		    #x6fa87e4f #xfe2ce6e0 #xa3014314 #x4e0811a1
		    #xf7537e82 #xbd3af235 #x2ad7d2bb #xeb86d391))))



(defun md5-function-f (x y z)
  (32-or (32-and x y) (32-and (32-not x) z)))

(defun md5-function-g (x y z)
  (32-or (32-and x z) (32-and y (32-not z))))

(defun md5-function-h (x y z)
  (32-xor (32-xor x y) z))

(defun md5-function-i (x y z)
  (32-xor y (32-or x (32-not z))))



(defun md5-length64 (message-length-in-bits)
  "Return integer as two 32-bit words, high order bits first"
  (values (ldb (byte 32 32) message-length-in-bits)
	  (ldb (byte 32 0) message-length-in-bits)))


(defmacro md5-make-octet-vector (a b c d)
  "Make octet-vector from 4 32 bits integers. "
  (flet ((bytes (num32)
	   `((ldb (byte 8 0) ,num32)
	     (ldb (byte 8 8) ,num32)
	     (ldb (byte 8 16) ,num32)
	     (ldb (byte 8 24) ,num32))))
    `(let ((a ,a) (b ,b) (c ,c) (d ,d))
       (vector ,@(bytes 'a)
	       ,@(bytes 'b)
	       ,@(bytes 'c)
	       ,@(bytes 'd)))))



(defmacro md5-function-ffgghhii (a b c d X k s i fghi)
  `(setf ,a (32-add ,b
		    (left-rot-32 (32-add (32-add ,a
						 (,fghi ,b ,c ,d))
					 (32-add (aref ,X ,k)
						 ,(aref *random-sine-table* (1- i))))
				 ,s))))


(defmacro md5-generate-code-for-one-round (fghi (a b c d) x
					   (k-initial k-inc)
					   (s0 s1 s2 s3) i-initial)
  "Return 16 function calls with correct arguments to the specified
F,G,H or I function."
  (do* ((k k-initial (mod (+ k k-inc) 16))
	(which 0 (mod (1+ which) 4))
	(i i-initial (1+ i))
	(forms nil))
      ((>= i (+ i-initial 16))
       (cons 'progn (nreverse forms)))
    (multiple-value-bind (s abcd)
	(ecase which
	  (0 (values s0 (list a b c d)))
	  (1 (values s1 (list d a b c)))
	  (2 (values s2 (list c d a b)))
	  (3 (values s3 (list b c d a))))
      (push `(md5-function-ffgghhii ,@abcd ,x ,k ,s ,i ,fghi)
	    forms))))


;;;;;;;;;
;;; Higher level MD5 functions

(defun md5-make-buffer-filler (reader-function &optional (octet-count 0))
  "Returns buffer-filler function for use as argument to md5-encode.
The buffer-filler fills a buffer with 16 32 bits words and returns true
if there is more data. It returns nil if all data including padding and 
data length has been returned.

This is almost identical to the SHA-1 function sha-1-make-buffer-filler, except for the order (big-endian vs little-endian) the octets are stored in.

The buffer-filler is a state-machine with four states. :done, :data, :length and :zeropad. The initial state is :data. When there is no more data, #x80 is returned and the new state is either :write-length (if current word is 13 and current byte is 3) else :zeropad. If we enter :write-length we write the length in the last two 32 bit words and enter :done. In state :zeropad we write zeros until we reach word 13 and byte 3 and then enter :write-length. When we reach the :done state, the next call will return nil."
  (let ((state :data)
	(byte-count octet-count)        ;counts number of bytes read
	(byte-num -1))                  ;0,1,2 or 3 afterwards
    (flet ((buffer-filler (buffer)
	     (dotimes (word 16 t)       ;16*32 = 512
	       (flet ((gb ()            ;helper to get the next byte
			(setf byte-num (mod (1+ byte-num) 4))
			(ecase state    
			  (:done (return-from buffer-filler nil))
			  (:data
			   (let ((a-byte (funcall reader-function)))
			     (cond
			      (a-byte 
			       (incf byte-count) a-byte)
			      ((and (= word 13)
				    (= byte-num 3))
			       (setf state :write-length) #x80)
			      (t (setf state :zeropad) #x80))))
			  (:zeropad 
			   (if (and (= word 13)
				    (= byte-num 3))
			       (setf state :write-length))
			   0)
			  (:write-length
			   (multiple-value-bind (hi low) 
			       (md5-length64 (* 8 byte-count))
			     (setf (aref buffer 14) low)
			     (setf (aref buffer 15) hi)
			     (setf state :done)
			     (return-from buffer-filler t))))))
                 
                 ;;Get a word
		 (let ((b0 (gb)) (b1 (gb)) (b2 (gb)) (b3 (gb)))
		   (setf (aref buffer word)
		     (dpb b3 (byte 8 24)
			  (dpb b2 (byte 8 16)
			       (dpb b1 (byte 8 8)
				    b0)))))))))
      #'buffer-filler)))



(defun md5-encode (buffer-filler)
  "Main function. Encode blocks of 512 bits until done"
  (let ((x (make-array 16 :element-type '(unsigned-byte 32)))
	a b c d)
    (initialize-md5-state a b c d)
    (while (funcall buffer-filler x)
      (multiple-value-setq (a b c d)
        (md5-round a b c d x)))

    (md5-make-octet-vector a b c d)))


;;;;;;;;;;;
;;; CLOS

(defclass MD5 (Hash)
  ((octet-count :accessor octet-count   ;octets processed so far
		:initform 0)
   (leftover-octets :accessor leftover-octets ;unprocessed octets
		    :initform (make-array 64 :element-type '(unsigned-byte 8)))
   (leftover-count :accessor leftover-count ;number of unprocessed octets
		   :initform 0)
   ;; MD5 state: four 32 bits words.
   (a :accessor a)
   (b :accessor b)
   (c :accessor c)
   (d :accessor d)))


(defmethod md5-fill-vector ((obj MD5) return-vector octet-vector start)
  "Return a 16 * 32 bit vector filled with leftover octets from previous rounds and octets from the input vector. We know that we have at least 64 bytes.

NB! Uses a different byte order than sha-1-fill-vector."
  (let ((offset 0)                      ;offset in the tmp vevtor v.
	(used 0)                        ;Num octets used from input vector.
	(v (make-array 64 :element-type '(unsigned-byte 8))))
    
    ;; Get leftover octets from previous calls to add.
    ;; We kown that obj contains < 64 bytes.
    (dotimes (i (leftover-count obj))
      (setf (aref v offset) (aref (leftover-octets obj) offset))
      (incf offset))

    ;; No leftover octets so we reset the leftover count.
    (setf (leftover-count obj) 0)

    ;; How many octets do we need from input vector.
    (setf used (- 64 offset))
    
    ;; Fill the remaining entries.
    (dotimes (i used)
      (setf (aref v (+ offset i)) (aref octet-vector (+ start i))))
    
    ;; Transfer to new format. Note different byte order from sha-1.
    (dotimes (word 16)
      (let ((b0 (aref v (* word 4)))
	    (b1 (aref v (+ (* word 4) 1)))
	    (b2 (aref v (+ (* word 4) 2)))
	    (b3 (aref v (+ (* word 4) 3))))
	(setf (aref return-vector word)
	  (dpb b3(byte 8 24)
	       (dpb b2 (byte 8 16)
		    (dpb b1 (byte 8 8)
			 b0))))))
    
    ;; Return offset in input vector.
    (+ start used)))


(defmethod store-state ((obj MD5) octet-vector offset end octet-count)
  "Store state between calls to add"
  (let ((leftover-offset (leftover-count obj))
	(octets-left (- end offset)))

    ;; We know there are less than 64 octets left so they all fit
    ;; in the leftover-octets array in obj.
    (dotimes (i octets-left) 
      (setf (aref (leftover-octets obj) (+ leftover-offset i)) 
	(aref octet-vector (+ offset i))))

    (setf (octet-count obj) octet-count)
    (setf (leftover-count obj) (+ leftover-offset octets-left))))


(defmethod md5-add-octet-vector ((obj MD5) octet-vector start end)
  (let ((vec (make-array 16 :element-type '(unsigned-byte 32)))
	(input-size (- end start))
	(offset start))	 

    ;; First empty any leftover octets from previous rounds.
    ;; We consume 64 octets each round until there is less than 64 left. Then
    ;; we store the remaining.
    (do ((left (+ (leftover-count obj) input-size) (- left 64))
	 (oct-count (octet-count obj) (+ 64 oct-count)))
	((< left 64) (store-state obj octet-vector offset end oct-count))
      (setf offset (md5-fill-vector obj vec octet-vector offset))
      (md5-round-object obj vec))))



(defmethod md5-round-object ((obj MD5) octet-vector)
  (multiple-value-bind (aa bb cc dd)
      (md5-round (a obj) (b obj) (c obj) (d obj) octet-vector)
    (setf (a obj) aa
	  (b obj) bb
	  (c obj) cc
	  (d obj) dd)))


(defun md5-round (a b c d octet-vector)
  (let ((aa a)	(bb b)	(cc c)	(dd d))
    ;; Round 1
    (md5-generate-code-for-one-round
     md5-function-f (a b c d) octet-vector (0 1) (7 12 17 22) 1)
    ;; Round 2
    (md5-generate-code-for-one-round
     md5-function-g (a b c d) octet-vector (1 5) (5 9 14 20) 17)
    ;; Round 3
    (md5-generate-code-for-one-round
     md5-function-h (a b c d) octet-vector (5 3) (4 11 16 23) 33)
    ;; Round 4
    (md5-generate-code-for-one-round
     md5-function-i (a b c d) octet-vector (0 7) (6 10 15 21) 49)
    
    (values  (32-add a aa) (32-add b bb) (32-add c cc) (32-add d dd))))



(defmethod md5-do-final ((obj MD5))
  (let ((vec (make-array 16 :element-type '(unsigned-byte 32)))
	(buffer-filler 
	 (md5-make-buffer-filler
	  (make-byte-array-reader-function 
	   (leftover-octets obj) (leftover-count obj))
	  (octet-count obj))))
    
    ;; Loops at most two times.
    (while (funcall buffer-filler vec)
      (md5-round-object obj vec))
    
    ;; Reset object and return hash.
    (prog1 (md5-make-octet-vector (a obj) (b obj) (c obj) (d obj))
      (reset obj))))


(defmethod reset ((obj MD5))
  (initialize-md5-state (a obj) (b obj) (c obj) (d obj))
  (setf (octet-count obj) 0
	(leftover-count obj) 0))



;;;;;;;;
;;; CLOS API

(defun make-MD5 ()
  "Constructor for the MD5 class"
  (let ((obj (make-instance 'MD5 :algorithm "MD5")))
    (initialize-md5-state (a obj) (b obj) (c obj) (d obj))
    obj))


(defmethod hash ((obj MD5) &optional data (start 0) (end (length data)))
  "Return the 128 bits MD5 hash."
  (when data
    (typecase data
      (vector (md5-add-octet-vector obj data start end)) 
      (otherwise 
       (error "Hash on data type ~A not implemented." (type-of data)))))

  (md5-do-final obj))


(defmethod update ((obj MD5) (octet-vector vector) 
                   &optional (start 0) (end (length octet-vector)))
  "Add octets to MD5 hash object. Get hash value by calling hash."
  (md5-add-octet-vector obj octet-vector start end))


;;;;;;;;;;;
;;; Low level API

(defmethod hash-stream ((obj MD5) (s stream))
  (md5-on-octet-stream s))

(defmethod hash-string ((obj MD5) (str string))
  (md5-on-string str))

(defun md5-on-string (string)
  "Return MD5 hash of string"
  (md5-encode
   (md5-make-buffer-filler
    (make-string-reader-function string))))

(defun md5-on-octet-vector (octet-vector)
  "Returns the MD5 hash of an octet-vector"
  (md5-encode
   (md5-make-buffer-filler
    (make-byte-array-reader-function octet-vector))))

(defun md5-on-octet-stream (stream)
  "Return MD5 hash of the stream."
  (md5-encode
   (md5-make-buffer-filler #'(lambda () (read-byte stream nil)))))


;;;;;;;;;;
;;; RFC 1321 test suite and some other test vectors.

(defun md5-test (spec testsuite)
  (dolist (test testsuite)
    (let ((in (first test))
	  (out (second test)))
      
      (case spec
	('MD5/RFC1321
	 (assert (string= (hex (md5-on-string in)) out)()
	   "md5 test string version for input string ~A~%" in)
	 
	 (let ((in-vec (string-to-octets in)))
	   (assert (string= (hex (md5-on-octet-vector in-vec)) out)()
	     "md5 test octet version for input string ~A~%" in)
	   
	   (let ((obj (make-MD5)))
             ;; Test hash only.
	     (assert (string= (hex (hash obj in-vec)) out) ()
	       "md5 CLOS test for input string ~A~%" in)
	     
             ;; Test update and hash.
	     (update obj in-vec)   
	     (assert (string= (hex (hash obj)) out) ()
	       "md5 CLOS update+hash test for input string ~A~%" in))))
	
	('MD5/long
	 (assert (string= (hex (md5-on-octet-vector in)) out)()
	   "md5 test octet version for long vector~%"))
        
	(otherwise (error "Unknown spec ~A" spec)))))
  (format t "~&~A test suite OK." spec))


(defun test-MD5()
  (md5-test
   'MD5/RFC1321                         ;test vectors from RFC 1321
   '(("" "d41d8cd98f00b204e9800998ecf8427e")
     ("a" "0cc175b9c0f1b6a831c399e269772661")
     ("abc" "900150983cd24fb0d6963f7d28e17f72")
     ("message digest" "f96b697d7cb7938d525a2f31aaf161d0")
     ("abcdefghijklmnopqrstuvwxyz" "c3fcd3d76192e4007dfb496cca67e13b")
     ("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
      "d174ab98d277d9f5a5611c2c9f419d9f")
     ;; Length > 64 octets to test multiple rounds.
     ("12345678901234567890123456789012345678901234567890123456789012345678901234567890"
      "57edf4a22be3c955ac49da2e2107b67a")))
  
  (md5-test
   'MD5/long
   (list (list #9999(2) "a373b18167a1abd419ed333afec8ea32"))))


(register-constructor 'MD5 #'make-MD5)