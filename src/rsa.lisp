;;;;-*-lisp-*-
;;;; The Crypticl cryptographic library.
;;;;
;;;; Description: RSA encryption/decryption according to PKCS#1
;;;; Author: Taale Skogan
;;;; Distribution:  See the accompanying file LICENSE.

;;; References:
;;; [1] RFC 2313 Kaliski, B. 1998. PKCS #1: RSA Encryption.

;;To do:
;; - PKCS#1 parser is not correct for type 0 (but that is a poor byte type anyway ans is not likely to be used)

(in-package crypticl)

;;;;;;;;
;;; Message formatting

(defun format-encryption-block (block-type data block-size)
  "Does encryption block formatting according to PKCS#1. An encryption block EB has the folowing format: EB = 00||BT||PS||00||data where block-type BT is one octet (0,1 or 2) and padding string PS has length: block-size - 3 - length(data). But PS has minimum length 8 octets. Input: -data: assumed to be short enough to fit to modulus. If data longer than block-size - 11, it will be truncated.
Don't allow block-type 0.
"
  (assert (member block-type (list 1 2)))
  (assert (> block-size 11))            ;Require at least 8 bytes padding
  ;; if block-type 0, then data must not start with a 0-octet
  (when (= block-type 0)
    (assert (/= 0 (aref data 0))))

  (let ((EB (make-array block-size :element-type '(unsigned-byte 8)))
        ;; 0 BT PS 0. This may truncate data.
	(data-length (min (length data)
			  (- block-size 1 1 8 1))))
    
    (formatv t "~&Max data block-size=~A" (- block-size 11))
    (formatv t "~&data-length=~A  (len data)=~A" data-length (length data))
    (when (< data-length (length data))
      (warn  "Input too long in RSA encryption: This key takes max ~A bytes, data was ~A bytes. Will truncate input." (- block-size 11) (length data)))
    
    (setf (aref EB 0) 0)
    (setf (aref EB 1) block-type)

    ;;Make padding string PS
    (do ((index 2 (1+ index)))
        ;; -1 because of null byte between PS and data
	((>= index (- block-size data-length 1)) 
	 (setf (aref EB index) 0))
      (setf (aref EB index)
	(ecase block-type
	  ((0) 0)
	  ((1) #xff)
	  ((2) (+ 1 (random 255))))))   
    
    ;; fill in data
    (do ((from-index 0 (1+ from-index))
	 (to-index (- block-size data-length) (1+ to-index)))
	((>= to-index block-size))
      (setf (aref EB to-index) (aref data from-index)))
    
    (formatv t "~&Made EB:~&~A" (hex EB))
    EB))


(defun extract-data-from-encryption-block (EB)
  "Reverse of format-encryption-block, but because octet-vector-to-integer
looses leading zeros we mustignore them too"
  (formatv t "~&Extracting data from this EB:~&~A" (hex EB))
  
  (let ((data-array nil)
	(from-index))                   ;One step ahead of i   
    ;; Skip padding. NB! We cannot assume the first byte is 0. 
    ;; Note two stop conditions.
    (do ((i 0 (1+ i)))
	((or (>= i (length EB))         ;In case no null byte due to error
	     (and (> i 0)               ;Skip leading 00 byte if there is one
		  (= 0 (aref EB i))))
         ;; Two loop exit forms
	 (assert (< i (length EB)) ()   ;first exit form
	   "Couldn't find null byte. Maybe because of error in decryption?") 
         
	 (setf from-index (+ i 1))))    ;final exit form. Skip null byte.

    ;;Make new array and copy in data from EB
    (setf data-array (make-array (- (length EB) from-index)
				 :element-type '(unsigned-byte 8)))
    (do ((to-index 0 (1+ to-index))
	 (from-index from-index (1+ from-index)))
	((>= from-index (length EB)))
      (setf (aref data-array to-index) (aref EB from-index)))
    data-array)) 




;;;;;;;
;;; Low level API

;; The encryption process has four steps: encryption-block-formatting, 
;; octet-string-to-integer conversion, rsa-encryption and 
;; integer-to-octet-string conversion.

(defun rsa-encrypt-block (data exponent modulus)
  "Low-level interface to RSA encryption. 

Parameters:
-data: Array with unsigned bytes (octets), type (unsigned-byte 8). The length of data is limited and the typical content is a symmetric key or a hash value. The exact maximum length in octets is k - 11 where 2^8*(k-1) <= n < 2^8*k where n is the RSA modulus. 
-exponent: public or private exponent. 
-modulus: the RSA modulus.

Returns:
-octet vector with encrypted data
"
  ;; block-size is maximum number of octets we can encrypt with 
  ;; this modulus. Called k in RFC 2313.
  (let ((block-size (ceiling (integer-length modulus) 8)))    
    (integer-to-octet-vector 
     (mod-expt (octet-vector-to-integer 
		(format-encryption-block 1 data block-size))
	       exponent modulus))))


(defun rsa-decrypt-block (ciphertext exponent modulus)
  "Low-level interface to RSA decryption. One block at a time only. "
  (extract-data-from-encryption-block
   (integer-to-octet-vector 
    (mod-expt (octet-vector-to-integer ciphertext) exponent modulus))))





;;;;;;
;;; CLOS API. 
;;;
;;; Note that using update as for the symmetric cipher makes
;;; litle sense because RSA is not meant for bulk encryption.

(defclass RSA (AsymmetricCipher)
  ((key :accessor key)
   (encryptp :accessor encryptp)        ;true if initialized for encryption.
   (leftover-octets :accessor leftover-octets ;unprocessed octets
		    :initform (make-array 8 :element-type '(unsigned-byte 8)))
   (leftover-count :accessor leftover-count ;number of unprocessed octets
		   :initform 0)))

(defclass RSAPrivateKey (PrivateKey)
  ((d :accessor d :initarg :d)          ;private exponent
   (n :accessor n :initarg :n)))


(defclass RSAPublicKey (PublicKey)
  ((e :accessor e :initarg :e)          ;public exponent
   (n :accessor n :initarg :n)))        ;modulus


(defun make-RSA ()
  "Constructor."
  (make-instance 'RSA :algorithm "RSA"))

(defmethod reset ((obj RSA))
  (setf (leftover-octets obj) nil)
  (setf (leftover-count obj) 0))

(defmethod init-encrypt ((obj RSA) key &key mode iv padding)
  "mode, iv and padding are only included for conformity with other Cipher objects and are not used."
  (when (or mode iv padding)
    (warn "Using mode(~A)/iv(~A)/padding(~A) makes no sense for RSA encrypt. These options will be ignored."
	  mode iv padding))
  (init obj key	:encryptp t ))

(defmethod init-decrypt ((obj RSA) key &key mode iv padding)
  "mode, iv and padding are only included for conformity with other Cipher objects and are not used."
  (when (or mode iv padding)
    (warn "Using mode(~A)/iv(~A)/padding(~A) makes no sense for RSA decrypt. These options will be ignored."
	  mode iv padding))
  (init obj key	:encryptp nil))

(defmethod init ((obj RSA) key &key (encryptp t) mode iv padding) 
  "mode, iv and padding are only included for conformity with other Cipher objects and are not used."
  (declare (ignore mode iv padding))
  (reset obj)
  (setf (key obj) key)
  (setf (encryptp obj) encryptp)
  (setf (leftover-count obj) 0))


(defmethod rsa-store-state ((obj RSA) data start end)
  "Simply store incomming vector in object together with any previous data in a new vector."
  ;; Sanity check on size of data
  (assert (< (length data) 50000) ()
    "Input to rsa-store-state is too large(size=~A)" (length data))
  
  (let* ((size (+ (leftover-count obj) (- end start)))
	 (vec (make-array size :element-type '(unsigned-byte 8)))
	 (offset 0))                    ;offset in vec
    ;; Sanity check on size of stored data
    (assert (< size 100000) ()
      "rsa-store-state is storing too much data(size=~A)" size)
    
    ;; Add previous data
    (dotimes (i (leftover-count obj) (setf offset i)) 
      (setf (aref vec i) (aref (leftover-octets obj) i)))
    
    ;; Add new data
    (dotimes (i (- end start)) 
      (setf (aref vec (+ offset i)) (aref data (+ start i))))
    
    (setf (leftover-octets obj) vec)
    (setf (leftover-count obj) size)))

(defmethod update ((obj RSA) data &optional (start 0) (end (length data)))
  "Always return nil. Note that update only store incomming bytes because we assume RSA will never be used for bulk encryption."
  (rsa-store-state obj data start end)
  nil)


(defmethod rsa-crypt ((obj RSA) data)
;;;  (if (encryptp obj)
;;;      (rsa-encrypt-block data (e (key obj)) (n (key obj)))
;;;    (rsa-decrypt-block data (d (key obj))  (n (key obj)))))
  ;; Note symmetry
  (if (encryptp obj)
      (typecase (key obj)
	(RSAPublicKey
	 (rsa-encrypt-block data (e (key obj)) (n (key obj))))
	(RSAPrivateKey
	 (rsa-encrypt-block data (d (key obj)) (n (key obj))))
	(t (error "Unkown keytype ~A" (key obj))))
    (typecase (key obj)
      (RSAPublicKey
       (rsa-decrypt-block data (e (key obj)) (n (key obj))))
      (RSAPrivateKey
       (rsa-decrypt-block data (d (key obj)) (n (key obj))))
      (t (error "Unkown keytype ~A" (key obj))))))


(defmethod update-and-crypt ((obj RSA) data 
                             &optional (start 0) (end (length data)))
  (let* ((size (+ (leftover-count obj) (- end start)))
	 (offset 0)                     ;offset in out vector
	 (out (make-array size :element-type '(unsigned-byte 8))))
    
    ;; Get leftover octets from previous calls to update.
    (dotimes (i (leftover-count obj))
      (setf (aref out i) (aref (leftover-octets obj) i))
      (incf offset))
    
    ;; No leftover octets left so we reset the leftover count.
    (setf (leftover-count obj) 0)
    
    ;; Get new data
    (when data
      (octet-vector-copy data start end out offset))

    ;; call low level primitive
    (rsa-crypt obj out)))



(defmethod encrypt ((obj RSA) &optional data (start 0) (end (length data)))
  "Returns new vector with encrypted data."
  (prog1
      (update-and-crypt obj data start end)
    (reset obj)))

(defmethod decrypt ((obj RSA) &optional data (start 0) (end (length data)))
  "Returns new vector with decrypted data."
  (prog1
      (update-and-crypt obj data start end)
    (reset obj)))


;;;;;;;;;;;;
;;; Signatures SHA-1withRSA

(defclass SHA-1withRSA (Signature)
  ((key :accessor key
	:initarg :key))
  (:documentation "A class for digital signatures using RSA and SHA-1."))

(defun make-SHA-1withRSA ()
  "Constructor. The default is to create an empty instance that can be initialized with the apropriate keys for signing or verifying. The typical usage will be to use init-verify with an authenticated copy of someone's public key to verify a document they have signed.
"
  (make-instance 'SHA-1withRSA))

(defmethod init-sign ((obj SHA-1withRSA) (private-key RSAPrivateKey))
  "Initialize for signing."
  (setf (key obj) private-key))

(defmethod init-verify ((obj SHA-1withRSA) (public-key RSAPublicKey))
  "Initialize instance for verifying. "
  (setf (key obj) public-key))

(defmethod sign ((obj SHA-1withRSA) message &key message-hash)
  "Sign a message and return the signature (s). Input is either the message as byte array message or a hash of the message, message-hash. Use nil for the message to choose the message-hash variant."
  (let* ((key (key obj))
	 (cipher (new-instance (algorithm key))))
    
    (init-encrypt cipher key)
    (list (encrypt cipher (or message-hash
                              (cond 
                               ((stringp message)	
                                (sha-1-on-string message))
                               (t (sha-1-on-octet-vector message))))))))

(defmethod verify ((obj SHA-1withRSA) signature message &key message-hash)
  "Verify a DSA signature s for a message. Input is either the message as byte array message or a hash of the message, message-hash. Use nil for the message to choose the message-hash variant."
  (let* ((sig (first signature))
	 (key (key obj))
	 (cipher (new-instance (algorithm key)))
	 (hash (or message-hash
		   (cond 
		    ((stringp message)	
		     (sha-1-on-string message))
		    (t (sha-1-on-octet-vector message))))))
    
    (init-decrypt cipher key)
    (if (vector-equal (decrypt cipher sig) hash)
	t
      nil)))





;;;;;;
;;; Key generation


(defun make-RSAPublicKey (e n)
  (make-instance 'RSAPublicKey :e e :n n :algorithm "RSA"))

(defmethod string-rep ((obj RSAPublicKey))
  (format nil "~A ~A" (e obj) (n obj)))

(defmethod print-object ((obj RSAPublicKey) stream)
  (format stream "<RSAPublicKey e=~A n=~A>" (e obj) (n obj)))



(defun make-RSAPrivateKey (d n)
  (make-instance 'RSAPrivateKey :d d :n n :algorithm "RSA"))

(defun make-RSAPublicKey-from-encoding (encoding)
  (let ((lst (construct-from-encoding encoding 'RSA)))
    (make-instance 'RSAPublicKey 
      :e (first lst)
      :n (second lst)
      :algorithm "RSA")))

(defun make-RSAPrivateKey-from-encoding (encoding)
  (let ((lst (construct-from-encoding encoding 'RSA)))
    (make-instance 'RSAPrivateKey 
      :d (first lst)
      :n (second lst)
      :algorithm "RSA")))

(defmethod string-rep ((obj RSAPrivateKey))
  (format nil "~A ~A" (d obj) (n obj)))

(defmethod print-object ((obj RSAPrivateKey) stream)
  (format stream "<RSAPrivateKey d=~A n=~A>" (d obj) (n obj)))

(defmethod get-encoding ((obj RSAPublicKey))
  (get-element-encodings (list (e obj) (n obj))))

(defmethod get-encoding ((obj RSAPrivateKey))
  (get-element-encodings (list (d obj) (n obj))))


(defclass RSAKeyPair (KeyPair)
  ())

(defun make-RSAKeyPair (e d n)
  (make-instance 'RSAKeyPair 
    :public (make-RSAPublicKey e n)
    :private (make-RSAPrivateKey d n)))


(defun rsa-get-prime (bitsize e)
  "Get a RSA prime n so that (n,e) = 1"
  (do ((n (random-bignum-max-odd bitsize) 
	  (random-bignum-max-odd bitsize)))
      ((and (rsa-primep n) 
	    (= 1 (gcd e (- n 1))))
       n)))

(defun rsa-generate-keys (bitsize)
  "Returns list with (public exponent, private exponent, modulus)"
  (let* ((e 17)
	 (p (rsa-get-prime (floor bitsize 2) e))
	 (q (rsa-get-prime (floor bitsize 2) e))
	 (d (mod-inverse e (* (- p 1) (- q 1)))))
    ;;(list e d p q  (* p q))))
    (make-RSAKeyPair e d (* p q))))



;;;;;;;;;
;;; Test suite

(defun rsa-test-cipher(spec testsuite)
  "Test a cipher spec with an entire testsuite.  The testsuite should be a
  list of 4- or 5-element lists of the form <strength/info key input output
  [iv]> where iv is optional. The output position may be nil, in which case
  the test succeeds if (equal X (decrypt (encrypt X))). Returns t if all
  tests are successfull."
  (dolist (test testsuite)
    (let* ((key (second test))
	   (input (hexo (third test)))
	   (output (hexo (fourth test)))
	   (in (octet-vector-copy input)))
      
      (case spec
	('RSA/lowlevel/generate-key
         ;;(format t "~&Keys:~&~A" key)
	 (setf in (rsa-encrypt-block in (first key) (third key)))
         ;;(format t "~&encrypted block:~&~A" (hex in))
	 (setf in (rsa-decrypt-block in (second key) (third key)))
	 (vector-check in input "RSA decryption error"))
	
	('RSA/lowlevel
	 (setf in (rsa-encrypt-block in (first key) (third key)))
	 (formatv t "~&encrypted block:~&~A" (hex in))
	 (vector-check in output "RSA encryption error")
	 (setf in (rsa-decrypt-block in (second key) (third key)))
	 (vector-check in input "RSA decryption error"))
	
	('RSA/CLOS
	 (let* ((keypair 
		 (make-RSAKeyPair (first key) (second key) (third key)))
		(pub (public keypair))
		(priv (private keypair))
		(obj (make-RSA)))
	   (init-encrypt obj pub)
	   (update obj in)
	   (setf in (encrypt obj))
	   (formatv t "~&encrypted block:~&~A" (hex in))
	   (vector-check in output "RSA encryption error")
	   (init-decrypt obj priv)
	   (update obj in)
	   (setf in (decrypt obj))
	   (vector-check in input "RSA decryption error")))

	
	(otherwise (error "Unknown spec ~A" spec)))))
  (format t "~&~A test suite OK." spec))


(defun test-RSA()
  ""
  (rsa-test-cipher  
   'RSA/lowlevel
   '((256 
      (17 
       52283595186792625783835800947080774046835597734964979634532429014747252191153 
       59254741211698309221680574406691543920236369742032585858025738669076013823747)
      "eeff2233eeff2233eeff" 
      "2a14fcef707a75e2d9be8108b8fbeabbb2721dd4821c2eb863cbf8572226e639")))
  
  (rsa-test-cipher  
   'RSA/lowlevel
   '((512
      (17 
       5659145250033004395435465881416767217973097095207092909909776107324839919862184327050565492971636598215112373569407363095069345689329342790220553896062769
       8017122437546756226866909998673753558795220884876714955705516152043523219804941971646706078609567425439414380465806269923389432836689805434367092898370539       )
      "eeff2233eeff2233eeffaaaa"
      "1d84004d7f25aa31e3bb61e4cec8e322cfa96e8def3f29d95d8c53a407d0a7453e0ffc4b17948121b0aeabf1faa1f2e187dec4a47e37c452fbad2d8ed9d366ba")))
  
  (rsa-test-cipher  
   'RSA/CLOS
   '((256 
      (17 
       52283595186792625783835800947080774046835597734964979634532429014747252191153 
       59254741211698309221680574406691543920236369742032585858025738669076013823747)
      "eeff2233eeff2233eeff" 
      "2a14fcef707a75e2d9be8108b8fbeabbb2721dd4821c2eb863cbf8572226e639")))
  
  (rsa-test-cipher
   'RSA/lowlevel/generate-key
   (list 
    (list 128
          ;; Return key as components (e d n)
	  (let (( key (generate-key 'RSA 128)))
	    (list (e (public key)) (d (private key)) (n (private key)))) 
	  "eeff2233ee" ))))





(defun rsa-test-key-primes (prime1 prime2 exponent)
  "Greatest Common Divisor(GCD) of prime1*prime2 and exponent must be 0.
That is GCD(prime1, exponent) = GCD(prime2,exponent) = 1 when we assume that exponent 
is less than the modulus: exponent < prime1 * prime2"
  (let ((result t))
    ;;uses local macro, note use of back quote
    (macrolet ((test (prime exp)
                 `(when (/= 1 (gcd (1- ,prime) ,exp))
                                        ;error
                    (setf result nil)
                    (format t "ERROR: (prime - 1) = ~A and exponent ~A has common factor~%" 
                            (1- ,prime)  ,exp))))
      
      (test prime1 exponent)
      (test prime2 exponent))
    result))


(register-constructor 'RSA #'make-RSA)
(register-constructor 'SHA-1withRSA #'make-SHA-1withRSA)
(register-key-generator 'RSA #'rsa-generate-keys)
(register-key-from-encoding 'RSAPublicKey #'make-RSAPublicKey-from-encoding)
(register-key-from-encoding 'RSAPrivateKey #'make-RSAPrivateKey-from-encoding)
