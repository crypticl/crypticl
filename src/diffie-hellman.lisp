;;;;-*-lisp-*-
;;;; The Crypticl cryptographic library.
;;;;
;;;; Description: Diffie-Hellman
;;;; Author: Taale Skogan
;;;; Distribution:  See the accompanying file LICENSE.


(in-package crypticl)

(defclass Diffie-Hellman (Crypto)
  ((key :accessor key :initform nil))
  (:documentation
   "Class for standard Diffie-Hellman key exchange. May later be changed to superclass for all Diffie-Hellman algorithms, including elliptic curves."))

(defun make-Diffie-Hellman ()
  "Constructor"
  (make-instance 'Diffie-Hellman :algorithm "Diffie-Hellman"))


;; Imprecise name
(defmethod init-Diffie-Hellman ((obj Diffie-Hellman) k)
  (setf (key obj) k))

(defmethod generate-random-Diffie-Hellman ((obj Diffie-Hellman))
  (let* ((g (g (key obj)))
	 (p (p (key obj)))
	 (x (random-secure-bignum-range 1 (- p 2))))
    
    (setf (x (key obj)) x)
    (mod-expt g x p)))

(defmethod get-secret-Diffie-Hellman ((obj Diffie-Hellman) y)
  (let ((p (p (key obj)))
	(x (x (key obj))))
    (mod-expt y x p)))
    

;;;;;;;;
;;; Key generation

(defclass Diffie-HellmanKey (Key)
  ((g :accessor g :initarg :g)
   (p :accessor p :initarg :p)
   (x :accessor x :initarg :x)))

(defmethod make-Diffie-HellmanKey (g p)
  (make-instance 'Diffie-HellmanKey 
    :key nil
    :g g 
    :p p
    :algorithm "Diffie-Hellman"))

(defmethod copy ((obj Diffie-HellmanKey))
  (make-Diffie-HellmanKey (g obj) (p obj)))

(defun Diffie-Hellman-generate-key (bitsize)
  (let ((p (random-bignum-max-odd bitsize)))
    
    (while (not (primep p))
      (setf p (random-bignum-max-odd bitsize)))

    ;; Find generator
    (do ((g 2 (+ g 1)))
	((/= (mod-expt g p p) 1) (make-Diffie-HellmanKey g p)))))
    
(defun make-Diffie-HellmanKey-from-encoding (encoding)
  (let ((lst (construct-from-encoding encoding 'Diffie-Hellman)))
    (make-instance 'Diffie-HellmanKey
      :key nil
      :g (first lst)
      :p (second lst)
      :algorithm "Diffie-Hellman")))

(defmethod string-rep ((obj Diffie-HellmanKey))
  (format nil "~A ~A" (g obj) (p obj)))

(defmethod get-encoding ((obj Diffie-HellmanKey))
  (get-element-encodings (list (g obj) (p obj))))



(defun test-dh ()
  (let (half-secret-1
	half-secret-2
	;; secret-1 and secret-2 should be equal in the end
	secret-1 
	secret-2
	dh1
	dh2
	key-1
	key-1-copy)
    
    (setf dh1 (make-Diffie-Hellman))
    (setf dh2 (make-Diffie-Hellman))
    (setf key-1 (generate-key 'Diffie-Hellman 64))
    ;; Make a copy of the key. We need a copy because the init function
    ;; stores state in the key object.
    (setf key-1-copy (copy key-1))
    (init-Diffie-Hellman dh1 key-1)
    (init-Diffie-Hellman dh2 key-1-copy)
    (setf half-secret-1 (generate-random-Diffie-Hellman dh1))
    (setf half-secret-2 (generate-random-Diffie-Hellman dh2))
    (setf secret-1 (get-secret-Diffie-Hellman dh1 half-secret-2))
    (setf secret-2 (get-secret-Diffie-Hellman dh2 half-secret-1))
    (list secret-1 secret-2)))
    

(register-constructor 'Diffie-Hellman #'make-Diffie-Hellman)
(register-key-generator 'Diffie-Hellman #'Diffie-Hellman-generate-key)
(register-key-from-encoding 'Diffie-HellmanKey #'make-Diffie-HellmanKey-from-encoding)  
  