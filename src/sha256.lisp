;;;;-*-lisp-*-
;;;; The Crypticl cryptographic library.
;;;;
;;;; Description: The SHA-256 hash algorithm
;;;; Author: Taale Skogan
;;;; Distribution:  See the accompanying file LICENSE.
;;;; Initial version: 16.01.2007


;;; Based on reference [1] from http://csrc.nist.gov/cryptval/shs.htm
;;; (live 07.01.2007).
;;;
;;; [1] NIST. 2002. Secure Hash Standard. FIPS PUB 180-2,
;;;     http://csrc.nist.gov/publications/fips/fips180-2/
;;;     fips180-2withchangenotice.pdf.
;;; [2] Lawrence E. Bassham III. 2004. The Secure Hash Algorithm
;;;     Validation System (SHAVS).

(in-package crypticl)

;;;;;;;;;;;;;;;;;;;
;;;
;;; Implementation
;;;
;;;;;;;;;;;;;;;;;;;
(defparameter *sha-256-constants*
    (make-array 64
		:element-type '(unsigned-byte 32)
		:initial-contents
		'(#x428a2f98 #x71374491 #xb5c0fbcf #xe9b5dba5 
		  #x3956c25b #x59f111f1 #x923f82a4 #xab1c5ed5 
		  #xd807aa98 #x12835b01 #x243185be #x550c7dc3 
		  #x72be5d74 #x80deb1fe #x9bdc06a7 #xc19bf174 
		  #xe49b69c1 #xefbe4786 #x0fc19dc6 #x240ca1cc 
		  #x2de92c6f #x4a7484aa #x5cb0a9dc #x76f988da 
		  #x983e5152 #xa831c66d #xb00327c8 #xbf597fc7 
		  #xc6e00bf3 #xd5a79147 #x06ca6351 #x14292967 
		  #x27b70a85 #x2e1b2138 #x4d2c6dfc #x53380d13 
		  #x650a7354 #x766a0abb #x81c2c92e #x92722c85 
		  #xa2bfe8a1 #xa81a664b #xc24b8b70 #xc76c51a3 
		  #xd192e819 #xd6990624 #xf40e3585 #x106aa070 
		  #x19a4c116 #x1e376c08 #x2748774c #x34b0bcb5 
		  #x391c0cb3 #x4ed8aa4a #x5b9cca4f #x682e6ff3 
		  #x748f82ee #x78a5636f #x84c87814 #x8cc70208 
		  #x90befffa #xa4506ceb #xbef9a3f7 #xc67178f2))
  "SHA-256 uses a sequence of 64 32-bit word constants.")

(defun sha-256-constant (i)
  (aref *sha-256-constants* i))

(defmacro initial-sha-256-hash-value (a b c d e f g h)
  "Initializes the state of the hash algorithm"
  `(setf ,a #x6a09e667
	 ,b #xbb67ae85
	 ,c #x3c6ef372
	 ,d #xa54ff53a
	 ,e #x510e527f
	 ,f #x9b05688c
	 ,g #x1f83d9ab
	 ,h #x5be0cd19))

;;; SHA-256 uses six logical functions, where each function operates on 32-bit 
;;; words, which are represented as x, y, and z. The result of each function 
;;; is a new 32-bit word.
;;;
;;; Note on the notation in the docstrings: not is a bitwise not operation,
;;; also referred to as the complement operation.
(defun ch-256 (x y z)
  "(x and y) xor (not x and z)"
  (logxor (logand x y)
	  (logand (lognot x) z)))

(defun maj-256 (x y z)
  "(x and y) xor (x and z) xor (y and z)"
  (logxor (logand x y)
	  (logand x z)
	  (logand y z)))

(defun sum-0 (x)
  "ROTR 2(x) xor ROTR 13(x) xor ROTR 22(x)"
  (logxor (right-rot-32 x 2)
	  (right-rot-32 x 13)
	  (right-rot-32 x 22)))

(defun sum-1 (x)
  "ROTR 6(x) xor ROTR 11(x) xor ROTR 25(x)"
  (logxor (right-rot-32 x 6)
	  (right-rot-32 x 11)
	  (right-rot-32 x 25)))

(defun sigma-0 (x)
  "ROTR 7(x) xor ROTR 18(x) xor SHR 3(x)"
  (logxor (right-rot-32 x 7)
	  (right-rot-32 x 18)
	  (ash x -3)))

(defun sigma-1 (x)
  "ROTR 17(x) xor ROTR 19(x) xor SHR 10(x)"
  (logxor (right-rot-32 x 17) 
	  (right-rot-32 x 19)
	  (ash x -10)))


(defun sha-256-encode (buffer-filler)
  "Compute SHA-256 hash."
  (let ((mb (make-array 16 :element-type '(unsigned-byte 32)))
	a b c d e f g h)                ; the 8 working variables.
    (initial-sha-256-hash-value a b c d e f g h)
    
    (while (funcall buffer-filler mb)
      (multiple-value-bind (aa bb cc dd ee ff gg hh)
	  (do-sha-256-message-block a b c d e f g h mb)
	(setq a (32-add a aa)
	      b (32-add b bb)
	      c (32-add c cc)
	      d (32-add d dd)
	      e (32-add e ee)
    	      f (32-add f ff)
	      g (32-add g gg)
	      h (32-add h hh))))
    
    ;; Return hash value as array.
    (int32s-to-octet-vector a b c d e f g h)))

(defun do-sha-256-message-block (a b c d e f g h mb)
  "Hash one 512 bits sha-256 message block.

Parameters:
a b c d e f g h - the current state of the hash function
mb - the message block, 512 bits of message, possibly padded, in a byte array
"
  (let ((ms (sha-256-message-schedule mb)))
    (dotimes (i 64)
      (let (T1 T2)
	(setf 
            ;; h + sum-1(e) + ch(e f g) + Kt + Wt
	    T1 (32-add h 
		       (sum-1 e)
		       (ch-256 e f g)	      
		       (sha-256-constant i)
		       (aref ms i))	   
            ;; sum-0(a) + maj(a b c)
	    T2 (32-add (sum-0 a) (maj-256 a b c))	    
	    h g
	    g f
	    f e
	    e (32-add d T1)
	    d c
	    c b
	    b a
	    a (32-add T1 T2)))
      ;;(pp-sha-256-state i a b c d e f g h)
      )
    
    (values a b c d e f g h)))

(defun pp-sha-256-state (iteration &rest r)
  "pretty-print the 8 state variables for debugging"
  (format t "iter ~D ~A~%" iteration 
	  (make-str (map 'list 
		      (lambda (x) (format nil "~A " (hex-int32 x))) r))))

(defun sha-256-message-schedule (mb)
  "Return the message schedule."
  (let ((w (make-array 64 :element-type '(unsigned-byte 32))))
    (dotimes (i 16)
      ;; Wt = Mt
      (setf (aref w i) (aref mb i)))
    (for (i 16 64)
         ;; Wt = sigma-1(Wt-2) + Wt-7 + sigma-0(Wt-15) + Wt-16
         (setf (aref w i) 
           (32-add (sigma-1 (aref w (- i 2)))
                   (aref w (- i 7))
                   (sigma-0 (aref w (- i 15)))
                   (aref w (- i 16)))))    
    w))


;;;;;;;;;;;;;;;;;;;
;;;
;;; CLOS internals
;;;
;;;;;;;;;;;;;;;;;;;
(defclass SHA-256 (Hash)
  ((octet-count :accessor octet-count   ;octets processed so far
		:initform 0)
   (leftover-octets :accessor leftover-octets ;unprocessed octets
		    :initform (make-array 64 :element-type '(unsigned-byte 8)))
   (leftover-count :accessor leftover-count ;number of unprocessed octets
		   :initform 0)
   (fresh :accessor fresh :initform t)
   ;; True if we have called hash and need to reset the object state.
   (called-hash :accessor called-hash :initform nil)
   ;; SHA-256 state: 8 32 bits words.
   (a :accessor a)
   (b :accessor b)
   (c :accessor c)
   (d :accessor d)
   (e :accessor e)
   (f :accessor f)
   (g :accessor g)
   (h :accessor h)))


(defmethod store-leftover ((obj SHA-256) octet-vector offset end octet-count)
  "Store leftover bytes between calls to update"
  (let ((leftover-offset (leftover-count obj))
	(octets-left (- end offset)))

    ;; We know there are less than 64 octets left so they all fit
    ;; in the leftover-octets array in obj.
    (dotimes (i octets-left) 
      (setf (aref (leftover-octets obj) (+ leftover-offset i)) 
	(aref octet-vector (+ offset i))))

    (setf (octet-count obj) octet-count)
    (setf (leftover-count obj) (+ leftover-offset octets-left))))


(defmethod sha-256-add-octet-vector ((obj SHA-256) octet-vector start end)
  "Compute intermediate hash value, and store leftover bytes.

Consume a multiple of 512 bits (64 bytes) blocks and compute the
intermediate hash value. Store any leftover bytes while waiting
for more data (cannot pad at this point).
"
  (let ((vec (make-array 16 :element-type '(unsigned-byte 32)))
	(input-size (- end start))
	(offset start))	 

    ;; First consume leftover bytes from previous rounds.
    ;; We consume 64 bytes (512 bits; the size of the message schedule)
    ;; each round until there is less than left. Store leftovers.
    (do ((left (+ (leftover-count obj) input-size) (- left 64))
	 (oct-count (octet-count obj) (+ 64 oct-count)))
	((< left 64) (store-leftover obj octet-vector offset end oct-count))
      (setf offset (fill-vector obj vec octet-vector offset))
      (sha-256-encode-block obj vec))))


(defmethod sha-256-final ((obj SHA-256))
  (let ((vec (make-array 16 :element-type '(unsigned-byte 32)))
	(buffer-filler 
	 (make-buffer-filler
	  (make-byte-array-reader-function 
	   (leftover-octets obj) (leftover-count obj))
	  (octet-count obj))))
    
    ;; Loops at most two times.
    (while (funcall buffer-filler vec)
      (sha-256-encode-block obj vec))
    
    ;; Return hash.
    (int32s-to-octet-vector (a obj) (b obj) (c obj) (d obj) 
                            (e obj) (f obj) (g obj) (h obj))))


(defmethod sha-256-encode-block ((obj SHA-256) mb)
  "Encode a single 512 bits block and add the state to the object."
  (multiple-value-bind (aa bb cc dd ee ff gg hh)
      (do-sha-256-message-block (a obj) (b obj) (c obj) (d obj)
				(e obj) (f obj) (g obj) (h obj)	mb)
    (setf (a obj) (32-add (a obj) aa)
	  (b obj) (32-add (b obj) bb)
	  (c obj) (32-add (c obj) cc)
	  (d obj) (32-add (d obj) dd)
	  (e obj) (32-add (e obj) ee)
	  (f obj) (32-add (f obj) ff)
	  (g obj) (32-add (g obj) gg)
	  (h obj) (32-add (h obj) hh))))


;;; TODO identical to SHA-1 method i sha.lisp so reuse
(defmethod fill-vector ((obj SHA-256) return-vector octet-vector start)
  "Return the next 512 bits for hashing.

Return a 16 * 32 bit vector filled with leftover octets from previous
rounds and octets from the input vector. We know that we have at
least 64 bytes."
  (let ((offset 0)                      ;offset in the tmp vevtor v.
	(used 0)                        ;Num octets used from input vector.
	(v (make-array 64 :element-type '(unsigned-byte 8))))
    
    ;; Get leftover octets from previous calls to add.
    ;; We kown that obj contains < 64 bytes.
    (dotimes (i (leftover-count obj))
      (setf (aref v offset) (aref (leftover-octets obj) offset))
      (incf offset))

    ;; No leftover octets so we reset the leftover count.
    (setf (leftover-count obj) 0)

    ;; How many octets do we need from input vector.
    (setf used (- 64 offset))
    
    ;; Fill the remaining entries.
    (dotimes (i used)
      (setf (aref v (+ offset i)) (aref octet-vector (+ start i))))
    
    ;; Transfer to new format.
    (dotimes (word 16)
      (let ((b3 (aref v (* word 4)))
	    (b2 (aref v (+ (* word 4) 1)))
	    (b1 (aref v (+ (* word 4) 2)))
	    (b0 (aref v (+ (* word 4) 3))))
	(setf (aref return-vector word)
	  (dpb b3(byte 8 24)
	       (dpb b2 (byte 8 16)
		    (dpb b1 (byte 8 8)
			 b0))))))
    
    ;; Return offset in input vector.
    (+ start used)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Low-level function API
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun sha-256-on-octet-vector (octet-vector)
  "Return SHA-256 hash of byte array/octet vector"
  (sha-256-encode
   (make-buffer-filler
    (make-byte-array-reader-function octet-vector))))

(defun sha-256-on-string (string)
  "Return SHA-256 hash of a string.

NB! With this function the hash value depends on the encoding of string
and implementation specific details of the Common Lisp distribution you
are using (see make-string-reader-function and its' use of char-code
for more details). For more control, decode the string to a byte array
yourself and use the byte array interface sha-256-on-octet-vector instead.
"
  (sha-256-encode
   (make-buffer-filler
    (make-string-reader-function string))))

(defun sha-256-on-octet-stream (stream)
  "Return SHA-256 hash of stream."
  (sha-256-encode
   (make-buffer-filler #'(lambda () (read-byte stream nil)))))

(defun sha-256-file (path)
  "Return SHA-256 hash of a file."
  (with-open-file (str path)
    (sha-256-on-octet-stream str)))


;;;;;;;;;;;;;
;;;
;;; CLOS API
;;;
;;;;;;;;;;;;;
(defun make-SHA-256 ()
  "Constructor for the SHA-256 class"
  (let ((obj (make-instance 'SHA-256 :algorithm "SHA-256")))
    (reset obj)
    obj))

(defmethod reset ((obj SHA-256))
  (initial-sha-256-hash-value (a obj) (b obj) (c obj) (d obj)
			      (e obj) (f obj) (g obj) (h obj))
  (setf (octet-count obj) 0
	(leftover-count obj) 0
	(called-hash obj) nil
	(fresh obj) t))

(defmethod hash ((obj SHA-256) &optional data (start 0) (end (length data)))
  "Return SHA-256 hash of all bytes added so far. 

Note that calling hash on an empty object object makes no sense but the spec
seems to be that we run algorithm on the initial state and return a full
256 bits hash even when the message length is 0.

Calling it a second time without adding data returns the previous value.
"
  (when (and (not data) (called-hash obj))
    ;; Return previous hash value when we have one and no data has been
    ;; added since last call to hash.
    (return-from hash (int32s-to-octet-vector 
		       (a obj) (b obj) (c obj) (d obj)
		       (e obj) (f obj) (g obj) (h obj))))
  (when data
    (typecase data
      (vector (sha-256-add-octet-vector obj data start end))
      (otherwise 
       (error "Hash on data type ~A not implemented." (type-of data)))))

  (setf (called-hash obj) t)
  (sha-256-final obj))

(defmethod update ((obj SHA-256) (octet-vector vector) 
		   &optional (start 0) (end (length octet-vector)))
  "Add bytes to SHA-256 hash object.
 
Will compute the intermediate hash value and not store the input. Useful
for hashing a large file that doesn't fit in memory or a data stream.

When all bytes have been added you get the hash value by calling the 
hash method."
  ;; Reset object if we have called hash
  (when (called-hash obj)
    (reset obj))
  
  (sha-256-add-octet-vector obj octet-vector start end)
  (setf (fresh obj) nil))


;;;;;;;;;;;;;;;;;;
;;;;
;;;; Tests
;;;;
;;;;;;;;;;;;;;;;;;
(defun run-all-sha-256-tests ()
  "Run all tests."
  (test-sha-256-short)
  (test-sha-256-long)
  (run-shavs-256))

(defun test-SHA-256 ()
  "Run these tests at load time."
  (test-sha-256-short))

(defun test-sha-256-short ()
  "Test vector 1 and 2 are taken from reference FIPS 180-2."
  (let ((test-list 
	 (list
	  (list "abc" 
		"ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad")
	  (list "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq"
		"248d6a61d20638b8e5c026930c3e6039a33ce45964ff2167f6ecedd419db06c1")
	  )))
    (format t "~&SHA-256 short vectors...")
    (dolist (x test-list (format t "OK."))
      (let ((in (first x))
	    (ex (second x))
	    (obj (make-SHA-256)))
        ;; low-level API
	(assert (string= (hex (sha-256-on-string in)) ex) ()
	  "sha-256 test for input string ~A~%" in)
        
        ;; CLOS API
        ;; Test hash only.
	(reset obj)
	(assert (string= (hex (hash obj (string-to-octets in))) ex) ()
	  "sha-256 CLOS test for input string ~A~%" in)	
	
        ;; Test update and hash.
	(reset obj)   
	(update obj (string-to-octets in))
	(assert (string= (hex (hash obj)) ex) ()
	  "sha-256 CLOS update+hash test for input string ~A~%" in)
	))))


(defun test-sha-256-long ()
  "Test long message."
  (format t "Testing long messages. This may take some seconds...~%")
  ;; only "a"s, ascii code of a is 97.
  (assert (string= (hex (sha-256-on-string 
			 (make-string 1000000 :initial-element #\a)))
		   "cdc76e5c9914fb9281a1c7e284d73e67f1809a48a497200e046d39ccc7112cd0") ()
    "sha-256 test for long test vector 1000000.")
  (format t "Long messages OK.~%"))


;;; Official test vectors from The Secure Hash Algorithm Validation System 
;;; (SHAVS) (reference [2].
(defun shavs (stream)
  (parse-shavs-lines
   (loop for line = (read-line stream nil 'eof)
       until (eq line 'eof)	    
       collect line))
  (format t "SHAVS tests OK"))

(defun parse-shavs-lines (lines)
  "Parse the SHAVS file format.

file format:
Len = 0
Msg = 00
MD = e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855

Len = 8
Msg = bd
MD = 68325720aabd7c82f30f554b313d0570c95accbb7dc4b5aae11204c08ffe732b
"
  (let ((state 'read-more)
	(count 0)
	len
	msg
	md)	 
    (dolist (line lines)
      (cond 
       ((string-startswith line "Len")
	(setf len (parse-integer line :start 6)))
       ((string-startswith line "Msg")
	(setf msg (subseq line 6)))
       ((string-startswith line "MD")
	(setf md (subseq line 5)
	      state 'run-sha))
       (t
	(setf state 'read-more)))

      (case state
        ;; continue reading
	(read-more nil)			
        ;; we have a new test vector
	(run-sha (shavs-256 len msg md)
		 (incf count)
		 (setf state 'read-more
		       len nil
		       msg nil
		       md nil))	
	))
    
    (format t "Tested ~D test vectors~%" count)
    ))


(defun shavs-256 (len msg md)
  (when (= len 0)
    (setf msg ""))
  (let ((bts (hexo msg))                ; msg as bytes
	(obj (make-SHA-256)))
    
    ;; Low-level API
    (assert (string= (hex (sha-256-on-octet-vector bts)) md) ()
      "sha-256 failed low-level hash on msg '~A'" msg)
    
    ;; CLOS API
    ;; hash all in one
    (assert (string= (hex (hash obj bts)) md) ()
      "sha-256 failed all-in-on-hash on msg '~A'" msg)
    
    ;;  update with all, then hash
    (reset obj)
    (update obj bts)
    (assert (string= (hex (hash obj)) md) ()
      "sha-256 failed single-update-hash on msg '~A'" msg)
    
    ;; update multiple times, then hash
    (reset obj)
    (let ((size (length bts)))
      (do* ((pos 0 (+ pos next))
	    (next (shavs-next-update-size pos size) 
		  (shavs-next-update-size pos size))
	    (end (+ pos next) (+ pos next)))
	  ((> end size))
        ;;(format t "size ~2@A pos ~2@A next ~2@A end ~2@A~%" size pos next end)
	(update obj (subseq bts pos end))))
    (assert (string= (hex (hash obj)) md) ()
      "sha-256 failed multiple-updates-hash on msg '~A'" md)
    ))

(defun shavs-next-update-size (pos size)
  ;; 60 a bit bigger than the msg block size (64 bytes)
  (let ((next (random 100))
	(leftover (- size pos))) 
    (max 1                              ; always advance at least once
	 (if (>= next leftover)
	     leftover
	   next))))

(defun run-shavs-on-string (string)
  (with-input-from-string (str string)
    (shavs str)))

(defun run-shavs-256 (&optional (path "../test/SHA256ShortMsg.txt"))
  "Verify SHA-256 against SHAVS test vectors from file.

NB The long test vectors are in the file ../test/SHA256LongMsg.txt"
  (with-open-file (str path)
    (shavs str)))


(register-constructor 'SHA-256 #'make-SHA-256)