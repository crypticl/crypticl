;;;;-*-lisp-*-
;;;; The Crypticl cryptographic library.
;;;;
;;;; Description: Simple key store utility.
;;;; Author: Taale Skogan
;;;; Distribution:  See the accompanying file LICENSE.

;;To do:


;; Printed representation of KeyStore object
;; users:
;;( (("Taale Skogan" "tasko@stud.cs.uit.no"...) ("22ffee" "55aadd" ...))
;;  (("Ross Anderson" "ross@acm.org" ...) ("eeff34"..:)))
;; ht:
;; "22ffee" -> #<RSAPrivateKey @ #x211f58ba>

(in-package crypticl)

(defclass KeyStore ()
  ((path :accessor path :initarg :path)
   (users :accessor users :initform ())
   (ht :accessor ht :initform (make-hash-table :test #'equal))))

(defun handle-RSAPublicKey (tokens)
   (make-RSAPublicKey (parse-integer (first tokens)) 
		      (parse-integer (second tokens))))

(defun handle-RSAPrivateKey (tokens)
   (make-RSAPrivateKey (parse-integer (first tokens)) 
		       (parse-integer (second tokens))))

(defun handle-DSAPublicKey (tokens)
  (make-DSAPublicKey (parse-integer (nth 0 tokens)) 
		     (parse-integer (nth 1 tokens))
		     (parse-integer (nth 2 tokens))
		     (parse-integer (nth 3 tokens))))

(defun handle-DSAPrivateKey (tokens)
  (make-DSAPrivateKey (parse-integer (nth 0 tokens)) 
		      (parse-integer (nth 1 tokens))
		      (parse-integer (nth 2 tokens))
		      (parse-integer (nth 3 tokens))
		      (parse-integer (nth 4 tokens))))

#|
"Taale Skogan" "tasko@stud.cs.uit.no"  - first line
RSAPublicKey "22ffee" 5 119         - one line per key
RSAPrivateKey "22ffee" 77 119
****                                - separator between users
"Ron Rivest" "ron@acm.org" 
RSAPublicKey "1234ffee" 3533 11413
RSAPrivateKey "4321ffee" 6597 11413
****                          
|#

(defun new-user (line)
  (string= (string-trim " " line ) "****"))

(defmethod put ((obj KeyStore) key-id key)
  (setf (gethash key-id (ht obj)) key))

(defmethod insert-user ((obj KeyStore) names key-fingerprints)
  (push (list names key-fingerprints) (users obj)))

(defmethod find-entry ((obj KeyStore) names)
  "Returns reference to entry where all names occur or nil."
  (print names)
  (dolist (entry (users obj))
    (print entry)
    (when (subsetp names (first entry) :test #'string=)
      (return entry))))

(defun get-int-fingerprint (int)
  "Get fingerprint from integer. Uses the 64 least significant bits as in RFC 1991 (PGP)"
  (hex-prepad-zero
   (integer-to-octet-vector
    (if (>= (integer-length int) 64)
	(ldb (byte 64 0) int)
      (ldb (byte (integer-length int) 0) int)))
   8))
   
(defmethod get-fingerprint ((obj Key))
  (etypecase obj
    (DSAPublicKey (get-int-fingerprint (y obj)))
    (DSAPublicKey (get-int-fingerprint (x obj)))
    (RSAPublicKey (get-int-fingerprint (n obj)))
    (RSAPrivateKey (get-int-fingerprint (n obj)))))

  

(defun parse-user (obj stream usernames)
  "
Stored as Lisp readable input:
-RSAPublicKey  ee23445566aabb e n
-RSAPrivateKey 223344ddaaee23 d n
"
  (let* ((names)
	 (key-fingerprints ()))
    
    ;; Parse user names in the first line
    (let ((start 0))
      (loop
	(multiple-value-bind (token end)
	    (read-from-string usernames nil 'eof :start start)
	  ;;(format t "~&Got:'~A' (start=~A, end=~A)" token start end)
	  (when (eq token 'eof)
	    (return))  ;break out of loop
	  (push (string token) names)
	  (setf start end))))
	  
    ;; Parse all key lines untill we reach a new user
    (do ((line (read-line stream) (read-line stream nil 'eof))) 
	((or (eq line 'eof)
	     (new-user line)))

      (let* ((tokens (split-string line " "))
	    (type (read-from-string (first tokens))) ;make a token
	    (fingerprint (read-from-string (second tokens)))
	    (key-parts (cdr (cdr tokens)))
	    (key nil))
	(push fingerprint key-fingerprints)
	
	;; Parse key parts and return a key object
	(setf key
	  (case  type
	    ('RSAPublicKey (handle-RSAPublicKey key-parts))
	    ('RSAPrivateKey (handle-RSAPrivateKey key-parts))
	    ('DSAPublicKey (handle-DSAPublicKey key-parts))
	    ('DSAPrivateKey (handle-DSAPrivateKey key-parts))
	    (t (error "~&not a valid key type=~A" type))))
	(put obj fingerprint key)))
	
    (insert-user obj names key-fingerprints)))




	  

(defmethod get-key ((obj KeyStore) fingerprint)
  "Retrieves key"
  (gethash fingerprint (ht obj)))

#|
"Taale Skogan" "tasko@stud.cs.uit.no"  - first line
RSAPublicKey "22ffee" 5 119         - one line per key
RSAPrivateKey "22ffee" 77 119
****                                - separator between users
"Ron Rivest" "ron@acm.org" 
RSAPublicKey "1234ffee" 3533 11413
RSAPrivateKey "4321ffee" 6597 11413
****                          
|#

(defmethod write-to-file ((obj KeyStore) &optional (filename (path obj)))
  (with-open-file  (str filename :direction :output :if-exists :supersede)
    (dolist (user (users obj))
      ;; First line with user name and aliases
      (dolist (name (first user) (format str "~%"))
	(format str "~w " name))

      ;; Print each key
      (dolist (fingerprint (second user) (format str "****~%"))
	(let ((key (get-key obj fingerprint)))
	  (format str "~w ~w ~A~%" 
		  (type-of key) fingerprint (string-rep key)))))))


(defmethod write-KeyStore ((obj KeyStore) &optional (filename (path obj)))
  (with-open-file  (str filename :direction :output :if-exists :supersede)
    (dolist (user (users obj))
      ;; First line with user name and aliases
      (dolist (name (first user) (format str "~%"))
	(format str "~w " name))

      ;; Print each key
      (dolist (fingerprint (second user) (format str "****~%"))
	(let ((key (get-key obj fingerprint)))
	  (format str "~w ~w ~A~%" 
		  (type-of key) fingerprint (string-rep key)))))))

		
(defun print-entry (id key)
  (print (list id key)))

(defmethod print-object ((obj KeyStore) (str stream))
  (format str "~&Users:")
  (print (users obj) str)
  (format str "~&Keys:")
  (maphash #'(lambda (fingerprint key) 
	       (format str "~&~A ~A" fingerprint key))
	   (ht obj)))



(defun load-KeyStore (obj path)
  "NB! If a key has multiple id it will be stored several times"
  (with-open-file (str path :direction :input)
    (when str
      (do ((line (read-line str) (read-line str nil 'eof))) 
	  ((eq line 'eof))
	(parse-user obj str line)))))

;;;;;;;
;;; User level API

(defmethod reset ((obj KeyStore))
  (setf (path obj) nil)
  (clrhash (ht obj)))
  
(defmethod init-KeyStore ((obj KeyStore) &optional (path "keystore.txt"))
  "Init object with data from path."
  (reset obj)
  (load-KeyStore obj path))

(defun make-KeyStore (&optional (path "keystore.txt"))
  (let ((obj (make-instance 'KeyStore :path path)))
    (load-KeyStore obj path)
    obj))
    
(defmethod add-key ((obj KeyStore) names key)
  "Adds key to keystore with the given list of names as identifiers. If the names already exists, store the key under the existing entry."
  (let ((fingerprint (get-fingerprint key))
	(entry (find-entry obj names)))
    (format t "~&add-key: Found entry: ~A" entry)

    ;; Add fingerprint if not already present, else make new user
    (if entry
	(setf (nth 1 entry) (adjoin fingerprint (nth 1 entry) :test #'string=))
      (push (list names (list fingerprint)) (users obj)))
          
    ;; Insert key unless already there. 
    (unless (gethash fingerprint (ht obj))
      (put obj fingerprint key))))



(defmethod get-keys ((obj KeyStore) user)
  (let ((fingerprints
	 (dolist (entry (users obj))
	   (when (member user (first entry) :test #'(lambda (u e) 
						      (string= u e)))
	     (return (second entry))))))
    (mapcar #'(lambda (fingerprint)
		(get-key obj fingerprint))
	    fingerprints)))

(defmethod get-public-keys ((obj KeyStore) user)
  (let ((fingerprints
	 (dolist (entry (users obj))
	   (when (member user (first entry) :test #'(lambda (u e) 
						      (string= u e)))
	     (return (second entry))))))
    (delete nil 
	    (mapcar #'(lambda (fingerprint)
			(let ((key (get-key obj fingerprint))) 
			  (when (subtypep  (class-of key) 'PublicKey)
			    key)))
		    fingerprints))))

(defmethod get-dsa-public-keys ((obj KeyStore) user)
  (delete-if-not #'(lambda (x) 
		     (subtypep (class-of x) 'DSAPublicKey))
		 (get-public-keys obj user)))
    

(register-constructor 'KeyStore #'make-KeyStore)