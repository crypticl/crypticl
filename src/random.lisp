;;;;-*-lisp-*-
;;;; The Crypticl cryptographic library.
;;;;
;;;; Description: Cryptographically secure pseudo random number generator.
;;;; Author: Taale Skogan
;;;; Distribution:  See the accompanying file LICENSE.

(in-package crypticl)

;;;;
;;;; INTERNALS
;;;;

(defparameter *random-secure-state* nil
  "State for the secure random number generator")

(defun high-entropy-octets (size)
  "Return size octets from some hopefully high entropy bit source."
  (let ((ret (make-array size :element-type '(unsigned-byte 8))))
    (with-open-file (file "/dev/random" :direction :input
		     :if-does-not-exist nil)          
      (if (not file)
	  (progn
            (warn "Unable to get high entropy bits for seeding the secure")
            (warn "random number generator. Seed with at least 256 high")
            (warn "entropy bits by calling reseed-secure-prng. Will continue") 
            (warn "in NON-SECURE mode in the mean time.")
            (do ((i 0 (1+ i)))
                ((>= i size))
              (setf (aref ret i) (random 256))))
	
        (do ((i 0 (1+ i)))
            ((>= i size))
          (setf (aref ret i) (read-byte file nil)))))
    
    ret))

;;;
;;; AES related code
;;;
;;; Based on Fortuna from Practical Cryptography.
;;;

(defclass SecurePRNG-AES ()
  ((key 
    :accessor key
    :initform #32(0))
   (ctr 
    :accessor ctr
    ;; NB! The counter size must be equal to the block size
    ;; of the cipher, in this case AES with 128-bit/16 bytes block size.
    :initform #16(0)))
  (:documentation "Cryptographically secure pseudo random number generator."))

(defun make-SecurePRNG-AES ()
  "Constructor for the Secure-PRNG class. Assumes that X bits secret/seed is enough."
  (let ((obj (make-instance 'SecurePRNG-AES)))
    (reseed obj (high-entropy-octets 32))))

(defmethod reseed ((obj SecurePRNG-AES) new-seed)
  "Reseed with byte array of high entropy bits."
  (let ((hasher (make-SHA-256))
        (keysize (length (key obj))))
    ;; Concatenate old key with new seed and hash
    (update hasher (key obj))
    (setf (key obj) (subseq (hash hasher new-seed) 0 keysize))
    ;; We run in counter mode so update counter
    (inc-counter obj)
    obj))

(defmethod inc-counter ((obj SecurePRNG-AES))
  (int-as-octet-vector-add (ctr obj) 1))
    
(defmethod SecurePRNG-octets-aes ((obj SecurePRNG-AES) size)
  "Returns size pseudorandom octets from a cryptographically secure PRNG."
  (let* ((res (make-array size 
                          :element-type '(unsigned-byte 8)
                          :initial-element 0))
         (ctr-size (length (ctr obj)))
         (tmp (make-array ctr-size 
                          :element-type '(unsigned-byte 8)
                          :initial-element 0)))
  (do* ((offset 0 (+ offset next))
        (leftover size (- leftover next))
        (next (min ctr-size leftover) (min ctr-size leftover)))
      ((<= leftover 0))
    ;; the cipher overwrites the input buffer so we cannot use
    ;; (ctr obj) directly.
    (octet-vector-copy (ctr obj) 0 ctr-size tmp 0)
    (aes-crypt-octet-vector tmp (key obj) 'ctr-onetime nil)
    (octet-vector-copy tmp 0 next res offset)
    (inc-counter obj))
    
  res))


;;;;
;;;; API 
;;;;

(defun random-secure-octets (size)
  "Return size octets from a cryptographically secure PRNG."
  (unless *random-secure-state*
    (setf *random-secure-state* (make-SecurePRNG-AES)))  
  (SecurePRNG-octets-aes *random-secure-state* size))


(defun random-secure-bignum (bitsize)
  "Return bignum from a cryptographically secure PRNG."
  (let* ((size (ceiling bitsize 8))
         (keep (mod bitsize 8))
	 (ov (random-secure-octets size)))    
    ;; Remove extra bits if bitsize not a multiple of 8.
    ;; This is done by only keeping the least (bitsize mod 8) significant
    ;; bits in the most significant byte.
    (unless (= keep 0)
      (setf (aref ov 0) (mask-field (byte keep 0) (aref ov 0))))
    (octet-vector-to-integer ov)))


(defun reseed-secure-prng (new-seed)
  "Reseed the global secure PRNG. 

The input should be high entropy bits, ideally 256 bits of entropy or more,
given as a bignum or a byte array."
  (unless *random-secure-state*
    (setf *random-secure-state* (make-SecurePRNG-AES)))  
  (typecase new-seed
    (integer (reseed *random-secure-state* 
                     (integer-to-octet-vector new-seed)))
    (vector (reseed *random-secure-state* new-seed))))


(defun random-bignum-max-odd (bitsize)
  "Return random, bitsize bits long, odd integer.
 
In other words, the least and most significant bit is always 1.
Used by RSA and DSA."
  (let ((n (random-secure-bignum bitsize)))
    (setf n (dpb 1 (byte 1 (1- bitsize)) n)
	  n (dpb 1 (byte 1 0) n))))


(defun random-secure-bignum-range (low high)
  "Return bignum in the range [low,high] from secure PRNG."
  ;; Be lazy and retry a few times
  (let ((bitsize (integer-length high)))
    (do ((n (- low 1)(random-secure-bignum bitsize)))
        ((and (<= n high) (>= n low)) n))))


;;;; TESTING

;;;; Failed attempt to get access to CryptGenRandom on Windows.
;;;; Unable to get Allegro's ffi binding to do what I want.
;;;(defun win32-random () ;(size)
;;;  "
;;;Some material on CryptGenRandom and the win32 api in general:
;;;
;;;win32 typedefs:
;;;
;;;typedef unsigned char BYTE
;;;typedef unsigned long DWORD
;;;
;;;LPCTSTR	Long Pointer to a Constant null-Terminated String (C programming/Windows API)
;;;typedef const CHAR     *PCSTR,      *LPCSTR;
;;;
;;;Python Cryptography Toolkit, file src/winrandom.c
;;;A successful use of CryptGenRandom.
;;;
;;;wincrypt.h
;;;typedef unsigned long HCRYPTPROV;
;;;
;;;From msdn library:
;;;http://msdn.microsoft.com/library/default.asp?url=/library/en-us/seccrypto/security/cryptgenrandom.asp
;;;
;;;http://msdn.microsoft.com/library/default.asp?url=/library/en-us/seccrypto/security/cryptgenrandom.asp
;;;
;;;BOOL WINAPI CryptGenRandom(
;;;  HCRYPTPROV hProv,
;;;  DWORD dwLen,
;;;  BYTE* pbBuffer);  
;;;
;;;BOOL WINAPI CryptAcquireContext(
;;;  HCRYPTPROV* phProv,
;;;  LPCTSTR pszContainer,
;;;  LPCTSTR pszProvider,
;;;  DWORD dwProvType,
;;;  DWORD dwFlags);
;;;
;;;if(CryptAcquireContext(
;;;   &hCryptProv,               // handle to the CSP
;;;   UserName,                  // container name 
;;;   NULL,                      // use the default provider
;;;   PROV_RSA_FULL,             // provider type
;;;   0))                        // flag values
;;;{...
;;;
;;;Example C code:
;;;
;;;#include <windows.h>
;;;#include <wincrypt.h>
;;;   
;;;static HCRYPTPROV hProvider;
;;;   
;;;void spc_rand_init(void) {
;;;  if (!CryptAcquireContext(&hProvider, 0, 0, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT))
;;;    ExitProcess((UINT)-1);  /* Feel free to properly signal an error instead. */
;;;}
;;;   
;;;unsigned char *spc_rand(unsigned char *pbBuffer, size_t cbBuffer) {
;;;  if (!hProvider) spc_rand_init(  );
;;;  if (!CryptGenRandom(hProvider, cbBuffer, pbBuffer))
;;;    ExitProcess((UINT)-1); /* Feel free to properly signal an error instead. */
;;;  return pbBuffer;
;;;}
;;;
;;;
;;;Another example (from http://erngui.com/articles/rng/index.html):
;;;
;;;#define _WIN32_WINNT 0x0400
;;;#include <windows.h>
;;;#include <wincrypt.h>
;;;
;;;long getrand()
;;;{
;;;  HCRYPTPROV hProv = 0;
;;;  CryptAcquireContext(&hProv, 
;;;    0, 0, PROV_RSA_FULL, 
;;;	CRYPT_VERIFYCONTEXT);
;;;  long rnd;
;;;  CryptGenRandom(hProv, 
;;;  	sizeof(rnd), (BYTE*)&rnd);
;;;  CryptReleaseContext(hProv, 0); 
;;;  return rnd;
;;;}
;;;
;;;
;;;/* Provider Types */
;;;1530 #define PROV_RSA_FULL             1
;;;1531 #define PROV_RSA_SIG              2
;;;1532 #define PROV_DSS                  3
;;;1533 #define PROV_FORTEZZA             4
;;;1534 #define PROV_MS_EXCHANGE          5
;;;1535 #define PROV_SSL                  6
;;;1536 #define PROV_RSA_SCHANNEL         12
;;;1537 #define PROV_DSS_DH               13
;;;1538 #define PROV_EC_ECDSA_SIG         14
;;;1539 #define PROV_EC_ECNRA_SIG         15
;;;1540 #define PROV_EC_ECDSA_FULL        16
;;;1541 #define PROV_EC_ECNRA_FULL        17
;;;1542 #define PROV_DH_SCHANNEL          18
;;;1543 #define PROV_SPYRUS_LYNKS         20
;;;1544 #define PROV_RNG                  21
;;;1545 #define PROV_INTEL_SEC            22
;;;1546 #define PROV_REPLACE_OWF          23
;;;1547 #define PROV_RSA_AES              24
;;;
;;;
;;;common error codes:
;;;
;;;ERROR_INVALID_PARAMETER ( 0x57/87L )
;;;One of the parameters contains a value that is not valid. This is most 
;;;often a pointer that is not valid.
;;;
;;;NTE_KEYSET_NOT_DEF( 0x80090019L )
;;;The key container specified by pszContainer does not exist, or the 
;;;requested provider does not exist.
;;;"
;;;  (load "Kernel32.dll") ; for GetLastError
;;;  (load "Advapi32.dll")
;;;  ;; Check that we have the foreign functions we need
;;;  (load "" :unreferenced-lib-names
;;;        (list "CryptGenRandom" "CryptAcquireContextW" 
;;;              "CryptAcquireContextA" "GetLastError"))
;;;
;;;  ;; BOOL WINAPI CryptAcquireContext(
;;;  ;; HCRYPTPROV* phProv,
;;;  ;; LPCTSTR pszContainer,   // const CHAR *LPCTSTR ??? No?
;;;  ;; LPCTSTR pszProvider,
;;;  ;; DWORD dwProvType,
;;;  ;; DWORD dwFlags);  
;;;  ;;
;;;  ;; CryptAcquireContextW also seems to work.
;;;  (ff:def-foreign-call (CryptAcquireContext "CryptAcquireContextA")
;;;      ((phProv (* :unsigned-long) (:unsigned-long))
;;;       (pszContainer (* :char)) 
;;;       (pszProvider (* :char))
;;;       (dwProvType :unsigned-long fixnum)
;;;       (dwFlags :unsigned-long fixnum)) 
;;;    :error-value :os-specific
;;;    :returning (:int boolean))
;;;  
;;;  (ff:def-foreign-call (GetLastError "GetLastError") (:void)
;;;    :returning (:unsigned-long bignum))
;;;
;;;  (flet ((err (where)
;;;           (format t "Error in ~A: 0x~,2'0X~%" where (GetLastError))))    
;;;    
;;;    (let ((phProv (ff:allocate-fobject :unsigned-long :foreign-static-gc)))    
;;;      (unless (CryptAcquireContext
;;;               phProv                   ;phProv
;;;               0                        ;pszContainer
;;;               0                        ;pszProvider
;;;               1                        ;dwProvType 1 = PROV_RSA_FULL
;;;               0)                       ;dwFlags)
;;;        (err "CryptAcquireContext"))
;;;
;;;      ;;  BOOL WINAPI CryptGenRandom(
;;;      ;;  HCRYPTPROV hProv, //typedef unsigned long HCRYPTPROV;
;;;      ;;  DWORD dwLen, 
;;;      ;;  BYTE* pbBuffer);  //typedef unsigned char BYTE
;;;      (ff:def-foreign-call (CryptGenRandom "CryptGenRandom") 
;;;          ((a :unsigned-long (:unsigned-long))
;;;           (b :unsigned-long)
;;;           (c (* :unsigned-char)))
;;;        :returning (:int boolean))
;;;      
;;;      (let*((c (make-array 16 :element-type '(unsigned-byte 8)
;;;                          :initial-element 2))
;;;            (before (hex c))
;;;            after)
;;;        ;; XXX This call always fail and error code is 87/0x57
;;;        ;; which supposedly is a bad pointer of some sort.
;;;        (unless (CryptGenRandom phProv 8 c)
;;;          (err "CryptGenRandom"))
;;;        (setf after (hex c))
;;;        (if (string= before after)
;;;            (format t "failure, no random bytes returned")
;;;          (format t "SUCCESS!! Got random bytes: ~A~%" after))))))
