;;;;-*-lisp-*-
;;;; The Crypticl cryptographic library.
;;;;
;;;; Description: IDEA - International Data Encryption Algorithm
;;;; Author: Taale Skogan
;;;; Distribution:  See the accompanying file LICENSE.

;;; Based on the references [1] and [2].
;;;
;;; [1] Xuejia Lai: "On the Design and Security of Block Ciphers", ETH Series 
;;; in Information Processing, vol. 1, Hartung-Gorre Verlag, Konstanz, 
;;; Switzerland, 1992.
;;; Note: Contains a test vector and a reference implementation in C.
;;;
;;; [2] Kaufman,C, Perlman, R & Speciner,M. 2002. "Network Security - Private 
;;; Communications in a Public World" Prentice Hall.

;;To do:
;; -CLOS version is almost identical to AES. May be worth abstracting.


(in-package crypticl)

;;;;;;;
;;; Low level utilities (group operations)

(defun idea-xor (a b)
  "16 bits xor"
  (ldb (byte 16 0) (logxor a b)))

(defun idea-add (a b)
  "Addition modulo (expt 2 16) = 65536"
  (ldb (byte 16 0) (+ a b)))

(defun idea-add-inverse (a)
  "Get inverse of a under addition. Equals negative"
  (ldb (byte 16 0) (- a)))

(defun idea-mult (a b)
"Multiplication modulo 1 + (expt 2 16) = 65537, a prime. We want to work on 16 bit numbers and modulo 65537 gives us one number to much, namely 65536. But because 0 does not have a multiplicative inverse we map it to 65536 during the computations."
(let* ((a (if (zerop a) 65536 a))
       (b (if (zerop b) 65536 b))
       (product (mod (* a b) 65537)))
  (if (= product 65536)
      0
    product)))



(defun idea-mult-inverse (e)
  "Use Extended Euclidean Algorithm to compute inverse of e mod 65537.
Computes 65537*x + ey = 1 and returns normalized y.
Assumes that e is a 16 bit number
Variables in algorithm from Menezes et al p. 67 with n = a and e = b. Note that
0 is it's own inverse and this situation is detected in the first exit test.
"   
  (let ((n 65537)(x2 1) (x1 0) (y2 0) (y1 1)
	(q) (r) (x) (y))
    (do ()
	((< e 1) (mod y2 65537))	;return normalized value 
      (setf q (floor n e)
	    r (- n (* q e))
	    x (- x2 (* q x1))
	    y (- y2 (* q y1))
	    n e
	    e r
	    x2 x1
	    x1 x
	    y2 y1
	    y1 y))))


(defun left-rotate (num positions word-width)
  (dpb (ldb (byte (- word-width positions) 0) num)
       (byte (- word-width positions) positions)
       (ldb (byte positions (- word-width positions)) num)))

;;;;;;
;;; Main functions

(defun idea-crypt (Xa Xb Xc Xd key-52)
  "Low level IDEA encryption and decryption (remember that encryption and decryption is equal in IDEA except for the order the subkeys are applied). The four variables Xa,Xb,Xc,Xd represents one 64-bits block of input. key-52 is an array of 52 16 bit integers that results from the key expansion of the original 128 bit key. This key array determines if we do encryption or decryption.

The variables follows the description in [2]. They describe IDEA as having 17 rounds, 9 odd and 8 even where the odd rounds use 4 subkeys and the even rounds use two. The last round is a special case resulting in two types of odd rounds, 8 normal and one final output transformation (the referecne [2] avoids this by changing the key sequence: They swap subkeys 50 and 51 for both encryption and decryption after genearating the keys in the normal way.). This implementation use the special case version."
  (do ((round 1 (1+ round)))
      ((> round 16) (values Xa Xb Xc Xd))
    (if (oddp round)
	  (multiple-value-setq (Xa Xb Xc Xd) 
	    (odd-round Xa Xb Xc Xd (with-subkeys key-52 round)))
	(multiple-value-setq (Xa Xb Xc Xd) 
	  (even-round Xa Xb Xc Xd (with-subkeys key-52 round)))))
  (output-transformation Xa Xb Xc Xd (with-subkeys key-52 17)))


(defun output-transformation (Xa Xb Xc Xd key-list)
  "Do the final output transformation"
  (let ((Ka (nth 0 key-list))
	(Kb (nth 1 key-list))
	(Kc (nth 2 key-list))
	(Kd (nth 3 key-list)))
    (values
     (idea-mult Xa Ka)
     (idea-add Xc Kb)
     (idea-add Xb Kc)
     (idea-mult Xd Kd))))


(defun with-subkeys (key-52 round)
  "Return correct subkeys for given round. Let key-52 be array of length 52. Let round be integer in the interval [1,17]"
  (let ((start))			;start offset into array
    (if (oddp round)
	(progn
	  (setf start (* 6 (floor round 2)))
	  (list 
	   (aref key-52 start)
	   (aref key-52 (+ start 1))
	   (aref key-52 (+ start 2))
	   (aref key-52 (+ start 3))))
      (progn
	(setf start (+ 4 (* 6 (1- (/ round 2)))))
	(list 
	 (aref key-52 start)
	 (aref key-52 (+ start 1)))))))
	
	          
(defun odd-round (Xa Xb Xc Xd key-list)
  "Do odd IDEA round"
  (let ((Ka (nth 0 key-list))
	(Kb (nth 1 key-list))
	(Kc (nth 2 key-list))
	(Kd (nth 3 key-list)))
      (values
       (idea-mult Xa Ka)
       (idea-add Xc Kc)
       (idea-add Xb Kb)
       (idea-mult Xd Kd))))


(defun even-round (Xa Xb Xc Xd key-list)
  "Do even IDEA round"
  (let*((Ke (nth 0 key-list))
	(Kf (nth 1 key-list))
	(Yin  (idea-xor Xa Xb))
	(Zin (idea-xor Xc Xd))
	(Yout (idea-mult Kf (idea-add Zin (idea-mult Yin Ke))))
	(Zout (idea-add Yout (idea-mult Yin Ke))))
    (values				;return new values for Xa,Xb,Xc and Xd
     (idea-xor Xa Yout)
     (idea-xor Xb Yout)
     (idea-xor Xc Zout)
     (idea-xor Xd Zout))))



(defun make-encryption-subkeys (key)
  "key is 128 bit key. Return array with 52 16 bit integers. The 16 most significant bits of key becomes the first subkey."
  (let ((key key)
	(array-pos 0)
	(key-52 (make-array 52 :element-type '(unsigned-byte 16))))
    (flet
	((rotate-key ()
	   (setf key (left-rotate key 25 128)))
	 
	 ;;Get a number of subkeys equal to num-subkeys and insert
	 ;;them in key-52 starting at offset array-pos.
	 (get-subkeys (array-pos num-subkeys)
	   (do ((k 7 (1- k)))
	       ((< k (- 8 num-subkeys)))
	     (setf (aref key-52 (+ array-pos (- 7 k)))
	       (ldb (byte 16 (* 16 k)) key)))))	   
	   
      (dotimes (j 6)			;6*8 = 48 
	(get-subkeys array-pos 8)
	(rotate-key)
	(setf array-pos (+ array-pos 8))) 
      (get-subkeys array-pos 4)		;48 + 4 = 52
      ;(rotatef (aref key-52 50) (aref key-52 49))
      key-52)))
      


(defun make-decryption-subkeys (key)
  "Makes the decryption subkeys. key is 128 bit key. Return array with 52 16 bit integers. The 16 most significant bits of key becomes the first subkey for decryption."
  (let ((encrypt-key-52 (make-encryption-subkeys key))
	(key-52 (make-array 52 :element-type '(unsigned-byte 16)
		:fill-pointer 0)))
    (flet (
	   ;;Invert subkeys if necessary, then add
	   (invert-add-keys (round key-list)
	     (cond
	      ((= 2 (length key-list))	;even round, no invert
	       (dolist (x key-list) 
		 (vector-push x key-52)))
	      ((member round (list 1 17))
	       (vector-push (idea-mult-inverse (nth 0 key-list)) key-52)
	       (vector-push (idea-add-inverse (nth 1 key-list)) key-52)
	       (vector-push (idea-add-inverse (nth 2 key-list)) key-52)
	       (vector-push (idea-mult-inverse (nth 3 key-list)) key-52))
	      (t 
	       (vector-push (idea-mult-inverse (nth 0 key-list)) key-52)
	       (vector-push (idea-add-inverse (nth 2 key-list)) key-52)
	       (vector-push (idea-add-inverse (nth 1 key-list)) key-52)
	       (vector-push (idea-mult-inverse (nth 3 key-list)) key-52)))))
	       
    (do ((round 17 (1- round)))
	((< round 1) key-52)
      (invert-add-keys round (with-subkeys encrypt-key-52 round)))
    ;(rotatef (aref key-52 50) (aref key-52 49))
    key-52)))


;;;;;;;;
;;; Cryptographic modes

(defun idea-crypt-octet-vector (data key mode doEncrypt &optional iv)
    "Encrypt data with key in given mode using an iv if necessary. Assumes data is a multiple of the block lenght (8 octets = 64 bits).
-mode: 'ecb, 'cbc
-key: octet vector
-doEncrypt is a boolean that chooses between decryption or encryption
"
  (assert (/= 0 (length data)))
  (assert (= 0 (mod (length data) 8)))
  
  (let* ((round-key)
	 (key (octet-vector-to-integer key)))
    (if doEncrypt
	(setf round-key (make-encryption-subkeys key))
      (setf round-key (make-decryption-subkeys key)))
    
    (cond ((eq mode 'ecb)
	   (idea-ecb-mode data round-key))
	  ((eq mode 'cbc)
	   (if doEncrypt
	       (idea-cbc-mode-encrypt data round-key iv)
	     (idea-cbc-mode-decrypt data round-key iv)))
	  (t (error "No such mode ~A in IDEA implementation" mode)))))


(defun idea-get-next (data start)
  (values
   (octet-vector-to-integer data start (+ start 2))
   (octet-vector-to-integer data (+ start 2) (+ start 4))
   (octet-vector-to-integer data (+ start 4) (+ start 6))
   (octet-vector-to-integer data (+ start 6) (+ start 8))))

(defun integer-to-octet-vector-16 (n)
  "Assumes n is a 16 bits integer and always generates a 2-octet vector"
  (let ((ret (integer-to-octet-vector n)))
    (cond 
     ((= (length ret) 2) ret)
     ((= (length ret) 1) (concat #(0) ret))
     (t (error "invalid vector size, got ~A" ret)))))

(defun idea-copy-back (Xa Xb Xc Xd data start)
  (octet-vector-copy 
   (concat (integer-to-octet-vector-16 Xa)
	   (integer-to-octet-vector-16 Xb)
	   (integer-to-octet-vector-16 Xc)
	   (integer-to-octet-vector-16 Xd))
   0 8 data start))


(defun idea-block (data round-key start)
  "Do one IDEA block starting at start in data."
  (multiple-value-call #'idea-copy-back
    (multiple-value-call #'idea-crypt 
      (idea-get-next data start) round-key)
    data start))


(defun idea-ecb-mode (data round-key)
  "Do IDEA ecb mode."
  (assert (= 0 (mod (length data)  8)) () 
    "Got ~A octets (not a multiplum of 8)." (length data))
  
  ;; Consume 8 bytes each round.
  (let ((rounds (/ (length data) 8)))
    (do ((i 0 (+ i 8)))
	((>= i rounds))
      (idea-block data round-key i))))

    
(defun idea-cbc-mode-encrypt (data round-key iv)
  ;;(format t "~&iv comming in: ~A" iv)
  (let ((len (length data))
	(iv (octet-vector-copy iv))) ;Avoid destructive behavior
    (do ((offset 0 (+ offset 8)))
	((= offset len) iv)		;return iv for CLOS version
      (xor-array data iv offset)
      (idea-block data round-key offset)
      ;;(format t "~&iv before copy comming in: ~A" iv)
      ;;(format t "~&offset=~A" offset)
      (octet-vector-copy data offset 8 iv)))) ;update iv 

(defun idea-cbc-mode-decrypt (data round-key iv)
  (let ((len (length data))
	(iv (octet-vector-copy iv))	;Avoid destructive behavior
	(tmp-iv (make-array 8 :element-type '(unsigned-byte 8))))
    (do ((offset 0 (+ offset 8)))
	((= offset len) iv)		;return iv for CLOS version
      (octet-vector-copy data offset 8 tmp-iv) ;next iv
      (idea-block data round-key offset)
      (xor-array data iv offset)
      (octet-vector-copy tmp-iv 0 8 iv))))  ;update iv



;;;;;;;;
;;; CLOS

(defclass IDEA (SymmetricCipher)
  ((key :accessor key)
   (mode :accessor mode)
   (padding :accessor padding)
   (iv :accessor iv)
   (encryptp :accessor encryptp)	;true if initialized for encryption.
   (leftover-octets :accessor leftover-octets ;unprocessed octets
		    :initform (make-array 8 :element-type '(unsigned-byte 8)))
   (leftover-count :accessor leftover-count ;number of unprocessed octets
		   :initform 0)))


(defun make-IDEA ()
  "Constructor for the IDEA class."
  (make-instance 'IDEA :algorithm "IDEA"))


(defmethod init-encrypt ((obj IDEA) key
			 &key (mode 'cbc) iv  (padding 'PKCS5))
  ;; default iv
  (unless iv
    (setf iv #8(0)))
  (init obj key	:mode mode :iv iv :encryptp t :padding padding))

(defmethod init-decrypt ((obj IDEA) key
			 &key (mode 'cbc) iv  (padding 'PKCS5))
  ;; default iv. Note how the blocksize is half the key size.
  (unless iv
    (setf iv #8(0)))
  (init obj key	:mode mode :iv iv :encryptp nil :padding padding))

(defmethod init ((obj IDEA) key 
		 &key (mode 'cbc) iv (encryptp t) (padding 'PKCS5))
  ;; Extract raw key if we got key object
  (typecase key
    (Key (setf (key obj) (key key)))
    (t (setf (key obj) key)))
  (setf (mode obj) mode)
  (setf (padding obj) padding)
  (setf (iv obj) (octet-vector-copy iv))
  (setf (encryptp obj) encryptp)
  (setf (leftover-count obj) 0))
   

    
(defmethod idea-store-state ((obj IDEA) data offset end)
  (let ((leftover-offset (leftover-count obj))
	(octets-left (- end offset)))

    ;; We know there are less than (8 - leftover-count) octets left so
    ;; they all fit in the leftover-octets vector in obj.
    (dotimes (i octets-left) 
      (setf (aref (leftover-octets obj) (+ leftover-offset i)) 
	(aref data (+ offset i))))
    
    (setf (leftover-count obj) (+ leftover-offset octets-left))))


(defmethod idea-update ((obj IDEA) data)
  "parameters: -data: octet-vector with length a multiple of 8."
  (idea-crypt-octet-vector data (key obj) (mode obj) (encryptp obj) (iv obj)))


(defmethod update ((obj IDEA) data &optional (start 0) (end (length data)))
  "Return nil if no data was encrypted or decrypted."
  ;; Add previous data to the front. Store leftover octets and use
  ;; data of length a multiple of 8.
  (let ((size (+ (leftover-count obj) (- end start))))
    ;; Prepare output vector if we have enough data
    (if (>= size 8)
	(let* ((offset 0)		;offset in out vector
	       (rem (mod size 8))	;leftover octets
	       (out (make-array (- size rem) :element-type '(unsigned-byte 8))))      
	  ;; Get leftover octets from previous calls to update.
	  (dotimes (i (leftover-count obj))
	    (setf (aref out i) (aref (leftover-octets obj) i))
	    (incf offset))
	      
	  ;; No leftover octets left so we reset the leftover count...
	  (setf (leftover-count obj) 0)
	  ;; ...and store any new leftover octets.
	  (idea-store-state obj data (- end rem) end)
	      
	  ;; Get new data
	  (octet-vector-copy data start (- end rem) out offset)
	      
	  ;; out is now a multiple of 8 octets so call low level primitive
	  (idea-update obj out)
	  out)
      (progn
	(idea-store-state obj data start end)
	nil))))


(defmethod update-and-encrypt ((obj IDEA) data 
			       &optional (start 0) (end (length data)))
  ;; Add previous data to the front. Pad or strip leftover octets and use
  ;; data of length a multiple of 8.
  (let* ((init-size (+ (leftover-count obj) (- end start)))
	 (pad-size (pad-size obj init-size 8))
	 (size (+ init-size pad-size))
    	 (offset 0)			;offset in out vector
	 (out (make-array size :element-type '(unsigned-byte 8))))
    
    ;; Get leftover octets from previous calls to update.
    (dotimes (i (leftover-count obj))
      (setf (aref out i) (aref (leftover-octets obj) i))
      (incf offset))
      
    ;; No leftover octets left so we reset the leftover count.
    (setf (leftover-count obj) 0)
	
    ;; Get new data
    (when data
      (octet-vector-copy data start end out offset))
	
    ;; Add padding
    (pad obj out (- size pad-size) pad-size)

    ;; out is now a multiple of 8 octets so call low level primitive
    (idea-update obj out)
    out))


(defmethod update-and-decrypt ((obj IDEA) data 
			   &optional (start 0) (end (length data)))
  ;; Add previous data to the front. Pad or strip leftover octets and use
  ;; data of length a multiple of 8.
  (let* ((init-size (+ (leftover-count obj) (- end start)))
	 (size init-size)
    	 (offset 0)			;offset in out vector
	 (out (make-array size 
			  :adjustable t
			  :element-type '(unsigned-byte 8))))
    
    ;; Get leftover octets from previous calls to update.
    (dotimes (i (leftover-count obj))
      (setf (aref out i) (aref (leftover-octets obj) i))
      (incf offset))
      
    ;; No leftover octets left so we reset the leftover count.
    (setf (leftover-count obj) 0)
	
    ;; Get new data
    (when data
      (octet-vector-copy data start end out offset))

    ;; data is a multiple of 8 octets so call low level primitive
    (idea-update obj out)
    
    ;; Remove padding.
    (remove-padding obj out)))

(defmethod encrypt ((obj IDEA) &optional data (start 0) (end (length data)))
  "Returns new vector with encrypted data."
  (update-and-encrypt obj data start end))

(defmethod decrypt ((obj IDEA) &optional data (start 0) (end (length data)))
  "Returns new vector with decrypted data."
  (update-and-decrypt obj data start end))


;;;;;;;;;;;
;;; Key generation

(defun idea-generate-key (&optional (bitsize 128) )
  (assert (member bitsize '(128)) () "IDEA invalid key size ~A" bitsize)
  (generate-symmetric-key bitsize "IDEA"))


;;;;;;;
;;; Test suite

(defun idea-test-cipher(spec testsuite)
  "Test a cipher spec with an entire testsuite.  The testsuite should be a
  list of 4- or 5-element lists of the form <strength/info key input output
  [iv]> where iv is optional. The output position may be nil, in which case
  the test succeeds if (equal X (decrypt (encrypt X))). Returns t if all
  tests are successfull."
  (dolist (test testsuite)
    (let* ((key (hexo (second test)))
	   (input (hexo (third test)))
	   (output (hexo (fourth test)))
	   (iv (hexo (fifth test)))
	   
	   (in (octet-vector-copy input))
	   (de-key (make-decryption-subkeys 
		    (octet-vector-to-integer key)))
	   (en-key (make-encryption-subkeys 
		    (octet-vector-to-integer key))))
      
      (case spec
	('IDEA/ECB/NoPadding
	 (idea-ecb-mode in en-key)
	 (vector-check in output "IDEA ECB encryption error")
	 (idea-ecb-mode in de-key)
	 (vector-check in input "IDEA ECB decryption error"))
	
	('IDEA/CBC/NoPadding
	 (idea-cbc-mode-encrypt in en-key iv)
	 ;;(print (hex in))
	 (vector-check in output "IDEA CBC encryption error")
	 (idea-cbc-mode-decrypt in de-key iv)
	 (vector-check in input "IDEA CBC decryption error"))
	
	('IDEA/CBC/PKCS5Padding
	 (let ((obj (make-IDEA)))
	   (init-encrypt obj key :iv iv)
	   (setf in (encrypt obj in))
	   ;;(print (hex in))
	   (vector-check in output "IDEA CLOS encryption error")
	   (init-decrypt obj key :iv iv)
	   (setf in (decrypt obj in))
	   (vector-check in input "IDEA CLOS decryption error")))
	   
	(otherwise (error "Unknown spec ~A" spec)))))
  (format t "~&~A test suite OK." spec))
	   

(defun test-IDEA()
  "IDEA test (From Cryptix' testvectors)"
  (idea-test-cipher
   'IDEA/ECB/NoPadding
   '((1 "00010002000300040005000600070008"
       "0000000100020003" "11FBED2B01986DE5")
      (2 "00010002000300040005000600070008"
       "0102030405060708" "540E5FEA18C2F8B1")
      (3 "00010002000300040005000600070008"
       "0019324B647D96AF" "9F0A0AB6E10CED78")
      (4 "00010002000300040005000600070008"
       "F5202D5B9C671B08" "CF18FD7355E2C5C5")
      (5 "00010002000300040005000600070008"
       "FAE6D2BEAA96826E" "85DF52005608193D")
      (6 "00010002000300040005000600070008"
       "0A141E28323C4650" "2F7DE750212FB734")
      (7 "00010002000300040005000600070008"
       "050A0F14191E2328" "7B7314925DE59C09")
      (8 "0005000A000F00140019001E00230028"
       "0102030405060708" "3EC04780BEFF6E20")
      (9 "3A984E2000195DB32EE501C8C47CEA60"
       "0102030405060708" "97BCD8200780DA86")
      (10 "006400C8012C019001F4025802BC0320"
       "05320A6414C819FA" "65BE87E7A2538AED")
      (11 "9D4075C103BC322AFB03E7BE6AB30006"
       "0808080808080808" "F5DB1AC45E5EF9F9")))
  
  (idea-test-cipher
   'IDEA/CBC/NoPadding
   '((1 "00010002000300040005000600070008"
	"0000000100020003" "16a321333c020b94" "0011223344556677")
     (2 "00010002000300040005000600070008"
      "0102030405060708" "f44cd08cc60b7650" "0011223344556677")
     (3 "00010002000300040005000600070008"
      "0102030405060708aabbccddaabbccdd0011223344556677"
      "f44cd08cc60b765004f7bee27fe3647f9d47befbbba5958d"
      "0011223344556677")))
  
  (idea-test-cipher
   'IDEA/CBC/PKCS5Padding
   '((1 "00010002000300040005000600070008"
	"0000000100020003" "16a321333c020b94788bd335348e4e6c"
	"0011223344556677")
     (2 "00010002000300040005000600070008"
      "0102030405060708" "f44cd08cc60b76508719543e0c3b2da0"
      "0011223344556677")
     ;; Un-aligned data.
     (3 "00010002000300040005000600070008"
      "0102030405060708aabbccddaabbccdd001122334455"
      "f44cd08cc60b765004f7bee27fe3647f6842979e1cc7fca7"
      "0011223344556677"))))
  
  
#| 

The following test vector comes from [1]. Others can be found on newsgroups such as sci.crypt.[1] also has a C implementation.

encryption keys from main key (1 2 3 4 5 6 7 8):
#(1 2 3 4 5 6 7 8 1024 1536 2048 2560 3072 3584 4096 512 16 20 24 28 32 4 8 12
  10240 12288 14336 16384 2048 4096 6144 8192 112 128 16 32 48 64 80 96 0 8192
  16384 24576 32768 40960 49152 57345 128 192 256 320) 
  
  decryption keys from main key (1 2 3 4 5 6 7 8):
  #(65025 65344 65280 26010 49152 57345 65533 32768 40960 52428 0 8192 42326
  65456 65472 21163 16 32 21835 65424 57344 65025 2048 4096 13101 51200 53248
  65533 8 12 19115 65504 65508 49153 16 20 43670 61440 61952 65409 2048 2560
  18725 64512 65528 21803 5 6 1 65534 65533 49153) 

  round values for plaintext (0 1 2 3):
  round 1: 0 5 3 12 
  round 2: 240 245 266 261 
  round 3: 1680 1290 253 7674 
  round 4: 8751 8629 62558 59737 
  round 5: 12902 1118 12213 45102 
  round 6: 3974 14782 36584 4467 
  round 7: 29839 36616 14810 17868 
  round 8: 22495 44120 50779 47693 
  round 9: 51782 65115 56408 4461 
  round 10: 36481 47772 63359 14922 
  round 11: 2724 63471 55964 9443 
  round 12: 26946 37897 57883 7268 
  round 13: 48205 57963 37961 42358 
  round 14: 39376 51190 21297 25102 
  round 15: 55693 54065 10230 33464 
  round 16: 2596 152 60523 18725 
  round 17: 4603 60715 408 28133 

  round values for ciphertext (4603 60715 408 28133):
  round 1: 2596 152 60523 18725 
  round 2: 55693 54065 10230 33464 
  round 3: 39376 51190 21297 25102 
  round 4: 48205 57963 37961 42358 
  round 5: 26946 37897 57883 7268 
  round 6: 2724 63471 55964 9443 
  round 7: 36481 47772 63359 14922 
  round 8: 51782 65115 56408 4461 
  round 9: 22495 44120 50779 47693 
  round 10: 29839 36616 14810 17868 
  round 11: 3974 14782 36584 4467 
  round 12: 12902 1118 12213 45102 
  round 13: 8751 8629 62558 59737 
  round 14: 1680 1290 253 7674 
  round 15: 240 245 266 261 
  round 16: 0 5 3 12 
  round 17: 0 1 2 3 
|#


(register-constructor 'IDEA #'make-IDEA)
(register-key-generator 'IDEA #'idea-generate-key)
