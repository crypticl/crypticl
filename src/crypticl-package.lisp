;;;;-*-lisp-*-
;;;; The Crypticl cryptographic library.
;;;;
;;;; Description: This file defines the crypticl package, the public interface 
;;;;              of the library. 
;;;; Usage: Loading this file will load the rest of the library.
;;;;        After loading you can list the public interface with 
;;;;        (crypticl:print-external-symbols) from the top-level. 
;;;; Author: Taale Skogan
;;;; Distribution:  See the accompanying file LICENSE.

;;To do:

(in-package cl-user)

(defpackage crypticl
  (:nicknames clc)
  (:use common-lisp)
  (:export 
   get-secret-Diffie-Hellman
   generate-random-Diffie-Hellman
   init-Diffie-Hellman
   Diffie-HellmanKey
   AsymmetricKey
   RSAPublicKey 
   RSAPrivateKey   
   get-encoding 
   DSAPublicKey 
   DSAPrivateKey 
   init-sign 
   init-verify 
   sign
   verify
   integer-to-octet-vector
   octet-vector-to-integer 
   hex
   hexo
   make-SymmetricKey 
   integer-to-octet-vector
   SymmetricKey
   load-package
   fast-load-package
   generate-key
   key-from-encoding
   random-secure-octets
   public 
   private
   key
   algorithm
   new-instance
   init-encrypt
   init-decrypt
   update
   encrypt
   decrypt
   print-external-symbols))

