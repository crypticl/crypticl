;;;;-*-lisp-*-
;;;; The Crypticl cryptographic library.
;;;;
;;;; Description: The SHA-1 message digest algorithm.
;;;; Author: Taale Skogan
;;;; Distribution:  See the accompanying file LICENSE.


;;; Based on the references [1] and [2].
;;; [1] FIPS 180-2 "Secure Hash Standard"
;;; [2] Schneier,B. 1996. "Applied Cryptography"

;;TODO:
;; -repeating code in sha-1-round and sha-1-encode (difficult because of objects)

(in-package crypticl)

(defmacro initialize-sha-1-state (a b c d e)
  "Initializes the state of the hash algorithm."
  `(setf ,a #x67452301
	 ,b #xefcdab89
	 ,c #x98badcfe
	 ,d #x10325476
	 ,e #xc3d2e1f0))

(defun sha-1-length64 (message-length-in-bits)
  "Returns input integer as two 32-bit words, high order bits first."
  (values (ldb (byte 32 32) message-length-in-bits)
	  (ldb (byte 32 0) message-length-in-bits)))

(defun new-sha-1-length64 (mess-len octet-vector start)
  "Returns octet-vector after the interger mess-len has been encoded as eight
bytes, high order bits first, starting at start."
  (do ((char 56 (- char 8)))
      ((> 0 char) 
       octet-vector)
    (setf (aref octet-vector start)
      (ldb (byte 8 char) mess-len))
    (incf start)))



(defun sha-function-1 (x y z)
  (logior (logand x y)(logand (lognot x) z) ))

(defun sha-function-2 (x y z)
  (logxor x y z))

(defun sha-function-3 (x y z)
  (logior (logand x y) (logand x z) (logand y z)))

(defun sha-function-4 (x y z)
  (logxor x y z))


(defun make-buffer-filler (reader-function &optional (octet-count 0))
  "Returns buffer-filler function for use as argument to sha-1-encode.

The buffer-filler fills a buffer with 16 32 bits words and returns true
if there is more data. It returns nil if all data including padding and 
data length has been returned.

Note that MD5 and SHA-1 uses the same padding scheme.

The buffer-filler is a state-machine with the four states
:data 
:done
:length
:zeropad

The initial state is :data. When there is no more data, #x80 is returned 
and the new state is either :write-length (if current word is 13 and 
current byte is 3) else :zeropad. If we enter :write-length we write the 
length in the last two 32 bit words and enter :done. In state :zeropad we 
write zeros until we reach word 13 and byte 3 and then enter :write-length.
When we reach the :done state, the next call will return nil."
  (let ((state :data)
	(byte-count octet-count)        ;counts number of bytes read
	(byte-num -1))                  ;0,1,2 or 3 afterwards
    (flet ((buffer-filler (buffer)
	     (dotimes (word 16 t)       ;16*32 = 512
	       (flet (
		      (gb ()            ;helper to get the next byte
			(flet ((db ()
                                 ;; (format t "state ~A word ~D ~%" state word)	
				 nil))
                          (setf byte-num (mod (1+ byte-num) 4))
                          (ecase state    
                            (:done 
                             (db)
                             (return-from buffer-filler nil))
                            (:data
                             (db)
                             (let ((a-byte (funcall reader-function)))
                               (cond
                                (a-byte 
                                 (incf byte-count) a-byte)
                                ((and (= word 13)
                                      (= byte-num 3))
                                 (setf state :write-length) #x80)
                                (t (setf state :zeropad) #x80))))
                            (:zeropad 
                             (db)
                             (if (and (= word 13)
                                      (= byte-num 3))
                                 (setf state :write-length))
                             0)
                            (:write-length
                             (db)
                             (multiple-value-bind (hi low) 
                                 (sha-1-length64 (* 8 byte-count))
                               (setf (aref buffer 14) hi) ;opposite order of MD5
                               (setf (aref buffer 15) low) ;opposite order of MD5
                               (setf state :done)
                               (return-from buffer-filler t)))))))
                 
                 ;;Get a word. Note that again the order is the opposite of MD5
		 (let ((b3 (gb))
		       (b2 (gb)) 
		       (b1 (gb)) 
		       (b0 (gb)))
		   (setf (aref buffer word)
		     (dpb b3 (byte 8 24)
			  (dpb b2 (byte 8 16)
			       (dpb b1 (byte 8 8)
				    b0)))))))))
      #'buffer-filler)))


;;;;;;;;
;;; Main SHA-1 functions

(defun sha-1-expand (m)
  "Expand input array m with 512 bits = 16 32 bits words to array of 80 32 bits words"
  (let ((w (make-array 80 :element-type '(unsigned-byte 32))))
    (dotimes (i 16 t)
      (setf (aref w i) (aref m i)  ) )
    (dotimes (i 64 t)
      (setf (aref w (+ i 16)) 
	(left-rot-32 (logxor (aref w (+ i 13)) (aref w (+ i 8)) 
			     (aref w (+ i 2)) (aref w (+ i ))) 1) ))
    w))


(defun sha-1-stage (a b c d e x)
  "Do one SHA-1 stage. Equals hash value of input shorter or equal to 512 bits. x is the new data, (a b c d e) is the current state of the hash function."
  (let ((w (sha-1-expand x)) 
        (temp))
    (macrolet ((sha-round (sha-function w-offset constant)
		 `(setf temp (32-add (left-rot-32 a 5)
				     (,sha-function b c d)
				     e
				     (aref w (+ i ,w-offset))
				     ,constant)
			e d 
			d c
			c (left-rot-32 b 30 )
			b a
			a temp)))
      
      ;; Round 1
      (dotimes (i 20)
	(sha-round sha-function-1 0 #x5a827999))
      ;; Round 2
      (dotimes (i 20)
	(sha-round sha-function-2 20 #x6ed9eba1))
      ;; Round 3
      (dotimes (i 20)
	(sha-round sha-function-3 40 #x8f1bbcdc))
      ;; Round 4
      (dotimes (i 20)
	(sha-round sha-function-4 60 #xca62c1d6))
      
      (values a b c d e))))


(defun sha-1-encode (buffer-filler)
  "Main non-CLOS function. Encodes 512 bits blocks until done."
  (let ((vec (make-array 16 :element-type '(unsigned-byte 32)))
	a b c d e)
    (initialize-sha-1-state a b c d e)
    
    (while (funcall buffer-filler vec)
      (multiple-value-bind (aa bb cc dd ee)
	  (sha-1-stage a b c d e vec)
	(setq a (32-add a aa)
	      b (32-add b bb)
	      c (32-add c cc)
	      d (32-add d dd)
	      e (32-add e ee))))	
    ;; Return hash value.
    (int32s-to-octet-vector a b c d e)))



;;;;;;;
;;; CLOS

(defclass SHA-1 (Hash)
  ((octet-count :accessor octet-count   ;octets processed so far
		:initform 0)
   (leftover-octets :accessor leftover-octets ;unprocessed octets
		    :initform (make-array 64 :element-type '(unsigned-byte 8)))
   (leftover-count :accessor leftover-count ;number of unprocessed octets
		   :initform 0)
   (fresh :accessor fresh :initform t)
   ;;SHA-1 state: five 32 bits words.
   (a :accessor a)
   (b :accessor b)
   (c :accessor c)
   (d :accessor d)
   (e :accessor e)))


(defmethod fill-vector ((obj SHA-1) return-vector octet-vector start)
  "Return a 16 * 32 bit vector filled with leftover octets from previous rounds and octets from the input vector. We know that we have at least 64 bytes"
  (let ((offset 0)                      ;offset in the tmp vevtor v.
	(used 0)                        ;Num octets used from input vector.
	(v (make-array 64 :element-type '(unsigned-byte 8))))
    
    ;; Get leftover octets from previous calls to add.
    ;; We kown that obj contains < 64 bytes.
    (dotimes (i (leftover-count obj))
      (setf (aref v offset) (aref (leftover-octets obj) offset))
      (incf offset))

    ;; No leftover octets so we reset the leftover count.
    (setf (leftover-count obj) 0)

    ;; How many octets do we need from input vector.
    (setf used (- 64 offset))
    
    ;; Fill the remaining entries.
    (dotimes (i used)
      (setf (aref v (+ offset i)) (aref octet-vector (+ start i))))
    
    ;; Transfer to new format.
    (dotimes (word 16)
      (let ((b3 (aref v (* word 4)))
	    (b2 (aref v (+ (* word 4) 1)))
	    (b1 (aref v (+ (* word 4) 2)))
	    (b0 (aref v (+ (* word 4) 3))))
	(setf (aref return-vector word)
	  (dpb b3(byte 8 24)
	       (dpb b2 (byte 8 16)
		    (dpb b1 (byte 8 8)
			 b0))))))
    
    ;; Return offset in input vector.
    (+ start used)))


(defmethod store-state ((obj SHA-1) octet-vector offset end octet-count)
  "Store state between calls to update."
  (let ((leftover-offset (leftover-count obj))
	(octets-left (- end offset)))

    ;; We know there are less than 64 octets left so they all fit
    ;; in the leftover-octets array in obj.
    (dotimes (i octets-left) 
      (setf (aref (leftover-octets obj) (+ leftover-offset i)) 
	(aref octet-vector (+ offset i))))

    (setf (octet-count obj) octet-count)
    (setf (leftover-count obj) (+ leftover-offset octets-left))))


(defmethod sha-1-add-octet-vector ((obj SHA-1) octet-vector start end)
  (let ((vec (make-array 16 :element-type '(unsigned-byte 32)))
	(input-size (- end start))
	(offset start))	 

    ;; First empty any leftover octets from previous rounds.
    ;; We consume 64 octets each round until there is less than 64 left. Then
    ;; we store the remaining.
    (do ((left (+ (leftover-count obj) input-size) (- left 64))
	 (oct-count (octet-count obj) (+ 64 oct-count)))
	((< left 64) (store-state obj octet-vector offset end oct-count))
      (setf offset (fill-vector obj vec octet-vector offset))
      (sha-1-round obj vec))))


(defun sha-1-round (obj octet-vector)
  (multiple-value-bind (aa bb cc dd ee)
      (sha-1-stage (a obj) (b obj) (c obj) (d obj) (e obj) octet-vector)
    (setf (a obj) (32-add (a obj) aa)
	  (b obj) (32-add (b obj) bb)
	  (c obj) (32-add (c obj) cc)
	  (d obj) (32-add (d obj) dd)
	  (e obj) (32-add (e obj) ee))))


(defmethod sha-1-final ((obj SHA-1))
  (let ((vec (make-array 16 :element-type '(unsigned-byte 32)))
	(buffer-filler 
	 (make-buffer-filler
	  (make-byte-array-reader-function 
	   (leftover-octets obj) (leftover-count obj))
	  (octet-count obj))))
    
    ;; Loops at most two times.
    (while (funcall buffer-filler vec)
      (sha-1-round obj vec))
    
    ;; Reset object and return hash.
    (prog1 (int32s-to-octet-vector (a obj) (b obj) (c obj) (d obj) (e obj))
      (reset obj))))


(defmethod reset ((obj SHA-1))
  (initialize-sha-1-state (a obj) (b obj) (c obj) (d obj) (e obj))
  (setf (octet-count obj) 0
	(leftover-count obj) 0))


;;;;;;;;
;;; CLOS API

(defun make-SHA-1 ()
  "Constructor for the SHA-1 class"
  (let ((obj (make-instance 'SHA-1 :algorithm "SHA-1")))
    (initialize-sha-1-state (a obj) (b obj) (c obj) (d obj) (e obj))
    obj))


(defmethod hash ((obj SHA-1) &optional data (start 0) (end (length data)))
  "Return SHA-1 hash. Note that calling hash on an empty object or a second time on the same object makes no sense. The value returned in both cases is the initial state of the SHA-1 algorithm."
  (when (and (fresh obj) (not data))
    (return-from hash nil))
  (when data
    (typecase data
      (vector (sha-1-add-octet-vector obj data start end))
      (otherwise 
       (error "Hash on data type ~A not implemented." (type-of data)))))
  (setf (fresh obj) t)
  (sha-1-final obj))


(defmethod update ((obj SHA-1) (octet-vector vector) 
		   &optional (start 0) (end (length octet-vector)))
  "Add octets to SHA-1 hash object. Get hash value by calling hash."
  (sha-1-add-octet-vector obj octet-vector start end)
  (setf (fresh obj) nil))


;;;;;;;;
;;; Low level API

(defmethod hash-stream ((obj SHA-1) (s stream))
  (sha-1-on-octet-stream s))

(defmethod hash-string ((obj SHA-1) (str string))
  (sha-1-on-string str))

(defun sha-1-on-string (string)
  "Return SHA-1 hash of string"
  (sha-1-encode
   (make-buffer-filler
    (make-string-reader-function string))))

(defun sha-1-on-octet-vector (octet-vector)
  "Returns the SHA-1 hash of an octet-vector"
  (sha-1-encode
   (make-buffer-filler
    (make-byte-array-reader-function octet-vector))))

(defun sha-1-on-octet-stream (stream)
  "Returns SHA-1 hash of the stream."
  (sha-1-encode
   (make-buffer-filler #'(lambda () (read-byte stream nil)))))



;;;;;;;;
;;; Test suite

(defun test-SHA-1 (&key test-long)
  "Test vector 1 and 2 are taken from reference FIPS 180-2."
  (let ((test-list 
	 (list
	  (list (string-to-octets "abc") 
		"a9993e364706816aba3e25717850c26c9cd0d89d")
	  (list (string-to-octets 
		 "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq") 
		"84983e441c3bd26ebaae4aa1f95129e5e54670f1")
	  (list #300(2 2 2)
		"e697e7834a688d2c982003a312da660a17a0fc9d"))))
    (format t "~&SHA-1 short vectors...")
    (dolist (x test-list (format t "OK."))
      (let ((in (first x))
	    (ex (second x)))
	(assert (string= (hex (sha-1-on-octet-vector in)) ex)()
	  "sha-1 test for input string ~A~%" in)

	(let ((obj (make-SHA-1)))
          ;; Test hash only.
	  (assert (string= (hex (hash obj in)) ex) ()
	    "sha-1 CLOS test for input string ~A~%" in)
          ;; Test add and hash.
	  (update obj in)   
	  (assert (string= (hex (hash obj)) ex) ()
	    "sha-1 CLOS update+hash test for input string ~A~%" in))))

    (when test-long
      (format t "Testing long vector. This may take some seconds...~%")
      (assert (string= (hex (sha-1-on-octet-vector #200000(2)))
		       "f4c046625d9c6672e0356bbe0ed5cd93adfa924b") ()
	"sha-1 test for long test vector 200000.")
      (format t "Done testing long vector.~%"))))


(register-constructor 'SHA-1 #'make-SHA-1)