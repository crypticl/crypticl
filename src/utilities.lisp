;;;;-*-lisp-*-
;;;; The Crypticl cryptographic library.
;;;;
;;;; Description: Non-cryptopgrahic utilities
;;;; Author: Taale Skogan
;;;; Distribution:  See the accompanying file LICENSE.

(in-package crypticl)


(defmacro while (test &rest body)
  `(do ()
       ((not ,test))
     ,@body))

(defmacro for ((var start stop) &body body)
  "Macro that simulates C syntax for loop. Use: (for (i 0 3) (..)) where 3 is _not_ included.Used in AES code."
  (let ((gstop (gensym)))
    `(do ((,var ,start (1+ ,var))
	  (,gstop ,stop))
      ((>= ,var ,gstop))
       ,@body)))

(defun octet-vector-to-integer 
    (vector &optional (start 0) (end (length vector)))
  "Represents 8 bits byte string as integer. Assumes byte is 8 bits. Uses big endian format. Vector must be an array of bytes"
  (let ((integer 0))
    (do ((i start (1+ i)))
	((>= i end))
      (setq integer (+ (* integer 256) (aref vector i))))
    integer))

(defun integer-to-octet-vector (integer &key vector 
                                             (start 0)
                                             size)
  "Transforms positive integers to byte vector using big endian/most
significant byte first format.

Parameters:
size -- size of returned vector, prepadded with zero bytes if necessary.
"      
  (when (and size vector)
    (assert (= size (length vector))))
  (let* ((required-length 
          (cond 
           (vector (length vector))
           (size size)           
           (t
            ;; Special case for 0 because integer-length returns 0 then.
            (if (= integer 0)
                1   
              (ceiling (integer-length integer) 8)))))
	 (result (or vector
		     (make-array required-length
				 :element-type '(unsigned-byte 8)
				 :initial-element 0)))
	 (end (+ start required-length)))
    
    (if (> end (length result))
	(error "~s is not big enough" result))
    
    (do* ((index (1- end) (1- index))
	  (bytespec (byte 8 0) (byte 8 (+ 8 (byte-position bytespec)))))
	((< index start))
      (setf (aref result index) 
	(coerce (ldb bytespec integer) '(unsigned-byte 8))))
    result))

(defun int-as-octet-vector-add (ov n)
  "Add n to octet vector ov and keep size of octet vector."
  (integer-to-octet-vector (+ (octet-vector-to-integer ov) n) :vector ov))

(defun hex (ov)
  (octet-vector-to-hex-string ov))

(defun hex-int32 (i)
  (hex-prepad-zero (integer-to-octet-vector i) 4))
  
(defun octet-vector-to-hex-string (bv)
  "Returns a hex string representation of a byte vector. Does not ignore leading zeros."
  (string-downcase 
   (let ((hex-string ""))
     (dotimes (i (length bv) hex-string )
       (setf hex-string 
         (concatenate 'string hex-string 
                      (format nil "~2,'0X" (aref bv i))))))))

(defun pp-hex (ov)
  "Pretty-print byte array in hex with 8 hex digits per block."
  (with-output-to-string (str)
    (let ((count 0))
      (dolist (x (map 'list (lambda (x) x) ov))
	(when (and (> count 0) (= (mod count 4) 0))
	  (write-string " " str))
	(write-string (format nil "~2,'0X" x) str)
	(incf count)))))
	   
(defun  hex-prepad-zero (ov size)
  "Size is minimum length in octets. NB! One octet = 2 hex litterals."
  (let* ((out (hex ov))
	 (prefix-length (- size (/ (length out)  2))))
    (if (> prefix-length 0)
	(concat (make-string (* 2 prefix-length) :initial-element #\0)
		out)
      out)))

(defun hexo (str)
  (hex-string-to-octet-vector str))

(defun hex-string-to-octet-vector (str &optional (start 0) (end (length str)))
  "Assume even number of hex numbers. Each pair will be converted to an octet.
"
  (let* ((size (ceiling (/ (- end start) 2)))
	(vec (make-array size :element-type '(unsigned-byte 8))))

    (do ((i 0 (1+ i))
	 (str-offset start (+ 2 str-offset)))
	((>= i size) vec)
      
      ;; read two chars each time and convert to integer. If there is only
      ;; one character left, pad with "0"
      (setf (aref vec i) 
	
	;; if test is new
	(if (< (+ 1 str-offset) end)	;detect end of string when odd length
	    (parse-integer 
	     str :start str-offset :end (+ 2 str-offset) :radix 16)
	  
	  ;; new code
	  (parse-integer
	   str :start str-offset :end (+ 1 str-offset) :radix 16))))))
	  
(defun insert-space (string)
  "Inserts a space between every 8th char in a string. Useful for pretty-printing a SHA-1 hash string."
  (let ((new-string ""))
    (dotimes (i (length string) new-string )
      (setf new-string 
	(if (and (= 0 (mod i 8))
		 (> i 0))
	    (concatenate 'string 
	      new-string (format nil " ~A" (aref string i)))
	  (concatenate 'string 
	    new-string (format nil "~A" (aref string i))))))))

(defun octet-vector-copy (in &optional (start 0) (end (length in))
                                       out (out-start 0))
  "Returns copy of input vector or copies it into out at the given offset."
  (let ((size (- end start)))
    (unless out
      (setf out (make-array size :element-type '(unsigned-byte 8))))
    (dotimes (i size out)
      (setf (aref out (+ out-start i)) (aref in (+ start i))))))          


(defun acopy (in &key (start 0) (size (length in))
                      out (out-start 0))
  "array copy"
  (unless out
    (setf out (make-byte-array size)))
  (dotimes (i size out)
    (setf (aref out (+ out-start i)) (aref in (+ start i)))))


(defun concat (&rest args)
  "Concatenates strings and vectors. 
WARNING! Will not work correctly if you mix strings and other vectors.
"
  (cond
   ((every #'stringp args) (apply #'concatenate (cons 'string args)))
   ((every #'vectorp args) (apply #'concatenate (cons 'vector args)))
   (t (error "Invalid types ~A" args))))


(defun make-str (lst)
  "Construct a string from a list of string"
  (with-output-to-string (str)
    (dolist (s lst)
      (write-string s str))))

(defun split-seq (seq pred &key (start 0) end key strict)
  "Return a list of subseq's of SEQ, split on predicate PRED.
Start from START, end with END.  If STRICT is non-nil, collect
zero-length subsequences too.
  (split-seq SEQ PRED &key (start 0) end key strict)"
  (loop :for st0 = (if strict start
                       (position-if-not pred seq :start start
                                        :end end :key key))
        :then (if strict (if st1 (1+ st1))
                  (position-if-not pred seq :start (or st1 st0)
                                   :end end :key key))
        :with st1 = 0 :while (and st0 st1) :do
        (setq st1 (position-if pred seq :start st0 :end end :key key))
		    :collect (subseq seq st0 st1)))

(defun split-string (str chars &rest opts)
  "Split the string on chars."
  (apply #'split-seq str (lambda (ch) (find ch chars))
         opts))

(defun vector-equal (v1 v2)
  "Return true if two vectors are equal"
  (if (/= (length v1) (length v2))
      nil
    (do ((i 0 (1+ i)))
	((>= i (length v1)) t)
      (unless (equal (aref v1 i) (aref v2 i))
	(return nil)))))

(defun string-startswith (s prefix)
  "Return true if the string s starts with the given prefix"
  (let ((len (length prefix)))
    (if (> len (length s))
	nil ; prefix longer than string
      (string= s prefix :end1 len))))

(defun string-to-octets (s)
  "Return string to byte array. 

This function is NOT portable and very implementation dependend.
Each character is converted to a single byte. Intended for working with
ASCII strings and 8-bit encodings.
"
  (map '(simple-array (unsigned-byte 8) (*))
    ;; NB! char-code can return a number bigger than 8 bits
    #'(lambda (c) (ldb (byte 8 0) (char-code c))) s))

(defun octets-to-string (octet-vector)
  "Convert byte array to string.

Assumes the byte array was produced by string-to-octets on 
the same platform."
  (map 'string #'code-char octet-vector))

(defun make-byte-array (size)  
  (make-array size :element-type '(unsigned-byte 8) :initial-element 0))