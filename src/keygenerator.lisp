;;;;-*-lisp-*-
;;;; The Crypticl cryptographic library.
;;;;
;;;; Description: Interface for key generation. 
;;;; Author: Taale Skogan
;;;; Distribution:  See the accompanying file LICENSE.

;;To do:

(in-package crypticl)

(defclass Key ()
  ((key :accessor key
	:initarg :key)
   (algorithm :accessor algorithm
	      :initform nil
	      :initarg :algorithm))
  (:documentation "Superclass for all keys, both asymmetric and symmetric"))

(defmethod string-rep ((obj Key))
  (error "string-rep not implemented for object of type=~A" (type-of obj)))

(defclass SymmetricKey (Key)
  ())

(defmethod print-object ((obj SymmetricKey) stream)
  (format stream "<SymmetricKey value:#X~A>" (hex (key obj))))

(defclass AsymmetricKey (Key)
  ())

(defun make-SymmetricKey (key &optional algorithm)
  "Constructor"
  (make-instance 'SymmetricKey :key key :algorithm algorithm))

(defclass KeyPair ()
  ((public :accessor public :initarg :public) 
   (private :accessor private :initarg :private)))
  

(defclass PublicKey (AsymmetricKey)
  ())

(defclass PrivateKey (AsymmetricKey)
  ())



(defun generate-symmetric-key (bitsize &optional algorithm)
  (assert (= 0 (mod bitsize 8)))
  (let ((octet-size (/ bitsize 8)))
    (make-SymmetricKey (random-secure-octets octet-size) algorithm)))


(defun key-from-encoding (keytype encoding)
  "dispatch on keytype"
  (do ((fun (get-key-from-encoding keytype) 
	    (get-key-from-encoding keytype)))
      (fun (apply fun (list encoding)))
    (restart-case (error  "No such keytype ~A implemented." keytype)
      (store-value (value)
	  :report "Try another keytype."
	  :interactive 
	    (lambda ()
	      (format t "~&New keytype ")
	      (format 
	       t "(use 'RSAPublicKey or RSAPublicKey, not \"RSAPublicKey\"): ")
	      (list (read)))
	(typecase value
	  (cons (setf keytype (second value))) ;input format 'RSA
	  (symbol (setf keytype value)))))))
  

(defparameter *key-from-encoding-table* (make-hash-table :test #'equalp))

(defun register-key-from-encoding (algorithm key-generator)
  "The key generator function must accept one argument, an encoding. The encoding can be used to recreate a key."
  ;; Store both symbol and symbol name
  (setf (gethash algorithm *key-from-encoding-table*) key-generator)
  (setf (gethash (symbol-name algorithm)
		 *key-from-encoding-table*) key-generator))

(defun delete-key-from-encoding(algorithm)
  (remhash algorithm *key-from-encoding-table*)
  (remhash (symbol-name algorithm) *key-from-encoding-table*))

(defun get-key-from-encoding (algorithm)
  (gethash algorithm *key-from-encoding-table*))




(defparameter *key-generator-table* (make-hash-table :test #'equalp))

(defun register-key-generator (algorithm key-generator)
  "The key generator function must accept one argument, a bitsize. The bitsize may be ignored (e.g. DSA)." 
  ;; Store both symbol and symbol name
  (setf (gethash algorithm *key-generator-table*) key-generator)
  (setf (gethash (symbol-name algorithm)
		 *key-generator-table*) key-generator))

(defun delete-key-generator(algorithm)
  (remhash algorithm *key-generator-table*)
  (remhash (symbol-name algorithm) *key-generator-table*))

(defun get-key-generator (algorithm)
  (gethash algorithm *key-generator-table*))

(defun generate-key (algorithm &optional bitsize)
  "Main function for getting new keys."
  (do ((fun (get-key-generator algorithm) (get-key-generator algorithm)))
      (fun (apply fun (list bitsize)))
    (restart-case (error  "No such algorithm ~A implemented." algorithm)
      (store-value (value)
	  :report "Try another algorithm."
	  :interactive (lambda ()
			 (format t "~&New algorithm ")
			 (format t "(use 'RSA or RSA, not \"RSA\"): ")
			 (list (read)))
	(typecase value
	  (cons (setf algorithm (second value))) ;input format 'RSA
	  (symbol (setf algorithm value))))))) ;input format RSA




(defun aes-generate-key (&optional (bitsize 128) encoding)
  (declare (ignore encoding))
  (assert (member bitsize '(128 192 256)) () "AES invalid key size ~A" bitsize)
  (generate-symmetric-key bitsize "AES"))

(register-key-generator 'AES #'aes-generate-key)