;;;;-*-lisp-*-
;;;; The Crypticl cryptographic library.
;;;;
;;;; Description: A void DES implementation.
;;;; Author: Taale Skogan
;;;; Distribution:  See the accompanying file LICENSE.

;;; DES has not been implemented, but a DES class and some
;;; empty functions are included to test adding a new algorithm.

(in-package crypticl)

(defclass DES (Crypto)
  ())

(defun make-DES ()
  (warn "The DES implementation is void.")
  (make-instance 'DES))

(defmethod encrypt ((obj DES) &optional data (start 0) (end (length data)))
  (declare (ignore start end)))  

(defun register-des ()
  (register-constructor 'DES #'make-DES))

(register-des)