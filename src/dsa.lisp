;;;;-*-lisp-*-
;;;; The Crypticl cryptographic library.
;;;;
;;;; Description: The Digital Signature Algorithm (DSA).
;;;; Author: Taale Skogan
;;;; Distribution:  See the accompanying file LICENSE.

;;; Implementation based on the Digital Signature Standard (DSS) ([1]). 
;;; All variable names based on the notation in [1].
;;;
;;; [1] NIST. 2000. Digital Signature Standard (DSS). FIPS Pub 186-2.

;;To do:

(in-package crypticl)


(defclass DSA (Signature)
  ((p :accessor p
      :initarg :p)
   (q :accessor q
      :initarg :q)
   (g :accessor g
      :initarg :g)
   (x :accessor x
      :initarg :x)
   (y :accessor y
      :initarg :y))
  
  (:documentation "A class for digital signatures using DSA. p and q are the primes, g the generator,x the private and y the public key."))

(defclass DSAPrivateKey (PrivateKey)
  ((p :accessor p :initarg :p)
   (q :accessor q :initarg :q)
   (g :accessor g :initarg :g)
   (x :accessor x :initarg :x)
   (y :accessor y :initarg :y)))

(defclass DSAPublicKey (PublicKey)
  ((p :accessor p :initarg :p)
   (q :accessor q :initarg :q)
   (g :accessor g :initarg :g)
   (y :accessor y :initarg :y)))

;; Suiteable q and p (p 1024 bits) primes from the NIST DSA home page.
(defparameter *DSA-default-q* 
    (parse-integer "b5afd2f93246b1efcd1f3a7c240c1e9e21a3630b" :radix 16))

(defparameter *DSA-default-p* (parse-integer "a65feaab511c61e33df38fdddaf03b59b6f25e1fa4de57e5cf00ae478a855dda4f3638d38bb00ac4af7d8414c3fb36e04fbdf3d3166712d43b421bfa757e85694ad27c48f396d03c8bce8da58db5b82039f35dcf857235c2f1c73b2226a361429190dcb5b6cd0edfb0ff6933900b02cecc0ce69274d8dae7c694804318d6d6b9" :radix 16))


(defun make-DSA (&key q pq-list generatep defaultp)
  "Constructor for the DSA class. Note that this function is overloaded. Only one of the key word arguments should be used at a time. The default is to create an empty instance that can be initialized with the apropriate keys for signing or verifying. The typical usage will be to use init-verify with an authenticated copy of someone's public key to verify a document they have signed.
 
-q: Will generate p and the rest of the key.
-pq-list: A list containing primes q and p, where q|p-1. Will generate the rest of the key. Usefull for testing known primes given in the Digital Signature Standard documentation for example.
-generatep: A boolean. Will generate a new key pair."
  (let (p g x
	(L 1024))                       ;Modulus length
    (cond
     (q (setf p (dsa-generate-p q L)))  ;Must generate p
     (pq-list (setf p (nth 0 pq-list)   ;Both p and q given
		    q (nth 1 pq-list)))
     (generatep (setf q (dsa-generate-q) ;Generate new p and q
		      p (dsa-generate-p q L)))
     (defaultp 
	 (setf q *DSA-default-q*
	       p *DSA-default-p*))
     (t (return-from make-DSA (make-instance 'DSA)))) 
    
    (setf g (dsa-find-generator-pq p q)
	  x (dsa-find-private-key q))
    
    (make-instance 'DSA 
      :p p
      :q q
      :g g 
      :x x
      :y (mod-expt g x p))))


(defmethod get-private-key ((obj DSA))
  "Returns the private key of a DSA object if one exists, nil otherwise."
  (make-DSAPrivateKey (p obj) (q obj) (g obj) (x obj) (y obj)))


(defmethod get-public-key ((obj DSA))
  "Returns the public key of a DSA object if one exists, nil otherwise."
  (make-DSAPublicKey (p obj) (q obj) (g obj) (y obj)))


(defmethod init-sign ((obj DSA) (private-key DSAPrivateKey))
  "Initialize DSA instance for signing."
  (setf (p obj) (p private-key)
	(q obj) (q private-key)
	(g obj) (g private-key)
	(x obj) (x private-key)
	(y obj) (y private-key)))

(defmethod init-verify ((obj DSA) (public-key DSAPublicKey))
  "Initialize DSA instance for verify. "
  (setf (p obj) (p public-key)
	(q obj) (q public-key)
	(g obj) (g public-key)
	(y obj) (y public-key)))


(defmethod sign ((obj DSA) message &key message-hash)
  "Signs a message and returns the signature (r,s). Input is either the message as byte array message or a hash of the message, message-hash. Use nil for the message to choose the message-hash variant."
  (let* ((q (q obj))
	 (p (p obj))
	 (x (x obj))
	 (g (g obj))
	 (k (dsa-find-k q))
	 (k-inv (mod-inverse k q))
	 (hash (parse-integer
		(hex
		 (or message-hash
		     (cond 
		      ((stringp message) ;String?
		       (sha-1-on-string message))
		      (t (sha-1-on-octet-vector message)))))
		:radix 16))
	 r s)
    (setf r (mod (mod-expt g k p) q))
    (setf  s (mod (* k-inv (+ hash (* x r))) q))
    (list r s)))


(defmethod verify ((obj DSA) signature message &key message-hash)
  "Verify a DSA signature (r s) for a message. Input is either the message as byte array message or a hash of the message, message-hash. Use nil for the message to choose the message-hash variant."
  (let* ((r (first signature))
	 (s (second signature))
	 (q (q obj))
	 (p (p obj))
	 (g (g obj))
	 (y (y obj))
	 (hash (parse-integer 
		(hex
		 (or message-hash
		     (cond 
		      ((stringp message) ;String?
		       (sha-1-on-string message))
		      (t (sha-1-on-octet-vector message)))))
		:radix 16))
	 
	 w u1 u2 v1 v2 v)
    (when (or (<= r 0) (>= r q) (<= s 0) (>= s q))
      (return-from verify nil))         ;invalid signature
    
    (setf w (mod-inverse s q)
	  u1 (mod (* hash w) q)
	  u2 (mod (* r w) q)
          ;;Compute v = g^u1 * y^u2 mod p mod q via v1 and v2
	  v1 (mod-expt g u1 p)
	  v2 (mod-expt y u2 p)
	  v (mod (mod (* v1 v2) p) q))
    
    (if (= v r)
	t                               ;valid signature
      nil)))                            ;invalid signature




;;;;;;;;;;
;;; DSA HELPER FUNCTIONS

(defun dsa-generate-q ()
  "Generate a 160 bits prime. Faster than the FIPS 186-2 method."
  (do ((q (random-secure-bignum 160) (random-secure-bignum 160)))
      ((primep q) q)))


(defun dsa-generate-p (q bitsize)
  "Generate p so that q|p-1"
  (let ((low-limit (expt 2 (1- bitsize)))
	X p)
    (loop
      (setf X (random-secure-bignum bitsize)
            ;;Construct p so that q|p-1
	    p (- X (1- (mod X (* 2 q)))))
      
      (when (and (>= p low-limit)
		 (primep p))
	(return-from dsa-generate-p p)))))



(defun dsa-find-generator-pq (p q)
  "Get a genrator for a subgroup of GF(p) order q. Use standard method described in Menezes et al 1997, p. 163. FIPS 186 only requires that 1 < h < p-1, but the process is much faster if we simply use small integers. "
  (let ((e (/ (1- p) q)))
    (do* ((h 2 (1+ h))
	  (g (mod-expt h e p) (mod-expt h e p)))
	((/= g 1) g))))


(defun dsa-find-private-key (q)
  "Return random integer 1 < x < q"
  (let (x)
    ;;This loop should only run a few times because x and q has the same length
    ;;and are generated by the same number generator
    (loop
      (setf x (random-secure-bignum (integer-length q)))
      (when (and (> x 1)
		 (< x q))
        (return x)))))


(defun dsa-find-k (q)
  "Find the parameter k, 1 < k < q."
  (let (k)
    ;;This loop should only run a few times because k and q has the same length
    ;;and are generated by the same number generator
    (loop
      (setf k (random-secure-bignum (integer-length q)))
      (when (and (> k 1)
		 (< k q))
        (return k)))))




(defun make-DSAPrivateKey (p q g x y)
  (make-instance 'DSAPrivateKey :p p :q q :g g :x x :y y :algorithm "DSA"))

(defmethod string-rep ((obj DSAPrivateKey))
  (format nil "~A ~A ~A ~A ~A" (p obj) (q obj) (g obj) (x obj) (y obj)))

(defmethod print-object ((obj DSAPrivateKey) stream)
  (format stream "<DSAPrivateKey>"))
;; Long output
;; (format stream "<DSAPrivateKey p=~A q=~A g=~A x=~A y=~A>" (p obj) (q obj) (g obj) (x obj) (y obj)))



(defun make-DSAPublicKey (p q g y)
  (make-instance 'DSAPublicKey :p p :q q :g g :y y :algorithm "DSA"))

(defun make-DSAPublicKey-from-encoding (encoding)
  (let ((lst (construct-from-encoding encoding 'DSA)))
    (make-instance 'DSAPublicKey 
      :p (first lst)
      :q (second lst)
      :g (third lst)
      :y (fourth lst)
      :algorithm "DSA")))

(defun make-DSAPrivateKey-from-encoding (encoding)
  (let ((lst (construct-from-encoding encoding 'DSA)))
    (make-instance 'DSAPrivateKey 
      :p (first lst)
      :q (second lst)
      :g (third lst)
      :x (fourth lst)
      :y (fifth lst)      
      :algorithm "DSA")))

(defmethod string-rep ((obj DSAPublicKey))
  (format nil "~A ~A ~A ~A" (p obj) (q obj) (g obj) (y obj)))

(defmethod print-object ((obj DSAPublicKey) stream)
  (format stream "<DSAPublicKey>))"))

;; Long output format
;; (format stream "<DSAPublicKey p=~A q=~A g=~A y=~A>" (p obj) (q obj) (g obj) (y obj)))

(defclass DSAKeyPair (KeyPair)
  ())

(defun make-DSAKeyPair (p q g x y)
  (make-instance 'DSAKeyPair 
    :public (make-DSAPublicKey p q g y)
    :private (make-DSAPrivateKey p q g x y)))

(defun dsa-generate-keys (bitsize)
  (declare (ignore bitsize))
  "Return a DSAKeyPair"
  (let ((alg (make-DSA :defaultp t)))
    (make-DSAKeyPair (p alg) (q alg) (g alg) (x alg) (y alg))))

(defmethod get-encoding ((obj DSAPublicKey))
  (get-element-encodings (list (p obj) (q obj) (g obj) (y obj))))

(defmethod get-encoding ((obj DSAPrivateKey))
  (get-element-encodings (list (p obj) (q obj) (g obj) (x obj) (y obj))))




;;;;;;; 
;;; Test suite
;;; There is no official test vector because we don't implement the 
;;; precise FIPS procedure. Testing can be done by calling sign and
;;; verify on the same message with the same key.
;;;

(defun test-DSA ()
  (let* ((dsa (make-DSA :defaultp t))
	 (sig (sign dsa "abc")))
    (if (verify dsa sig "abc")
	(format t "~&DSA OK")
      (error "DSA sign and verify failed"))))




;;;;;;;;
;;; FIPS 186 FUNCTIONS 

;;;(defun dsa-generate-key-fips-186 ()
;;;  "Generate DSA key in FIPS-186 style. Note that L = 1024 is the only valid modulo length so
;;;n and b in L-1 = n*160 + b is also constants: 1023 = 6*160 + 63. Must find a p so that q|p-1."
;;;  (let ((L 1024)
;;;	(n 6)
;;;	(b 63)
;;;	q
;;;	counter
;;;	offset
;;;	p)
;;;    (loop
;;;      (multiple-value-bind (q seed) (dsa-generate-q) 
;;;	(do ((counter 0 (1+ counter))
;;;	     (offset 2 (+ offset n 1)))
;;;	    ((>= counter 4096))		;4096 = 2^12
;;;	  (setf p (dsa-generate-p))
;;;	  (when (>= p (expt 2 (1- L)))
;;;	    (if (primep p)
;;;		return-from dsa-generate-key (values p q))))))))
;;;
;;;
;;;(defun dsa-generate-q-fipfs-186 (&optional seed-string)
;;;  "Generate a 160 bits prime folowing the fips-186-2 style.
;;;Seed value is always 160 bits long.
;;;"
;;;  (let (seed 
;;;	seed+1
;;;	q)
;;;    (do ((i 0 (1+ i)))
;;;	()				;loop forever
;;;      
;;;      ;;If first iteration, use optional seed-string if there is one
;;;      (if (and seed-string (= i 0))
;;;	  (setf seed (parse-integer seed-string :radix 16))
;;;	(setf seed (random-secure-bignum 160)))
;;;      
;;;      (setf seed+1 (mod (1+ seed) (expt 2 160)))      
;;;      (setf q (logior
;;;	       (parse-integer 
;;;		(sha-1-on-octet-vector
;;;		 (integer-to-byte-vector seed))
;;;		:radix 16)
;;;	       (parse-integer 
;;;		(sha-1-on-octet-vector
;;;		 (integer-to-byte-vector seed+1))
;;;		:radix 16)))
;;;
;;;      ;;Set most and least significant bit to 1
;;;      (setf q (dpb 1 (byte 1 0) q))
;;;      (setf q (dpb 1 (byte 1 159) q))
;;;
;;;      (if (primep q)
;;;	  (return-from dsa-generate-q (values q seed))))))
;;;
;;;(defun dsa-generate-p-fips-186 (n b seed offset g L q)
;;;  "Generate a number p so that and q|p-1 in FIPS-186 style"
;;;  (let (W X)
;;;    (dotimes (k n)
;;;      (setf W (+ W (* (expt 2 (* 160 k))
;;;		      (parse-integer (sha-1-on-octet-vector 
;;;				      (integer-to-byte-vector
;;;				       (mod (+ seed  offset k) (expt 2 g))))
;;;				     :radix 16)))))
;;;    
;;;    ;;last "SHA-term"
;;;    (setf W (+ W (* (expt 2 (* 160 n))
;;;		    (mod (parse-integer (sha-1-on-octet-vector 
;;;					 (integer-to-byte-vector
;;;					  (mod (+ seed  offset n) (expt 2 g))))
;;;					:radix 16)
;;;			 (expt 2 b)))))
;;;    (setf X (+ W (expt 2 (1- L))))
;;;
;;;    ;;Return p
;;;    (- X (1- (mod X (* 2 q))))))
;;;
;;;
;;;(defun dsa-generate-key ()
;;;  "Generate dsa key-pair with 1024 bits modulus"
;;;  (let* ((q (dsa-generate-q))
;;;	 (p (dsa-generate-p q 1024)))
;;;    (values p q)))


(register-constructor 'DSA #'make-DSA)
(register-key-generator 'DSA #'dsa-generate-keys)
(register-key-from-encoding 'DSAPublicKey #'make-DSAPublicKey-from-encoding)
(register-key-from-encoding 'DSAPrivateKey #'make-DSAPrivateKey-from-encoding)