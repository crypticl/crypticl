;;;; .emacs for crypticl

;;Binding *.lisp files to Common Lisp mode. Does this by adding cons cell
;;to global variable. Must come before we load lisp.
(setq auto-mode-alist (cons '("\\.lisp?$" . common-lisp-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.emacs?$" . emacs-lisp-mode) auto-mode-alist))

(setq fi:common-lisp-buffer-name "*common-lisp*"
       fi:common-lisp-directory "C:/crypticl/src/"
       fi:common-lisp-image-name "C:/Programfiler/acl62/mlisp"
       fi:common-lisp-image-file "C:/Programfiler/acl62/mlisp"
       fi:common-lisp-image-arguments nil
       fi:common-lisp-host "localhost")

(load "C:/Programfiler/acl62/eli/fi-site-init.el")
  
(defun run-common-lisp ()
  (interactive)
  (fi:common-lisp fi:common-lisp-buffer-name
                  fi:common-lisp-directory
                  fi:common-lisp-image-name
                  fi:common-lisp-image-arguments
                  fi:common-lisp-host)
  (define-key fi:inferior-common-lisp-mode-map "\M-n" 'fi:push-input)
  (define-key fi:inferior-common-lisp-mode-map "\M-p" 'fi:pop-input))

(global-set-key "\C-x\C-x" '(lambda () 
			      (interactive)
			      (switch-to-buffer "*common-lisp*")))
;;Shortcut
(defun rcl ()
  (interactive)
  (run-common-lisp))



(defun run-allegro-8 ()
  (interactive)
  (push "C:/Programfiler/acl80-express/eli/" load-path)
  (load "fi-site-init.el")

  (setq fi:common-lisp-image-name 
        "C:/Programfiler/acl80-express/allegro-ansi.exe")
  (setq fi:common-lisp-image-file 
        "C:/Programfiler/acl80-express/allegro-ansi.dxl")
  (setq fi:common-lisp-image-arguments '("+B" "+cn"))


  (fi:common-lisp fi:common-lisp-buffer-name
                  fi:common-lisp-directory
                  fi:common-lisp-image-name
                  fi:common-lisp-image-arguments
                  fi:common-lisp-host
                  fi:common-lisp-image-file))


(defun run-sbcl ()
  (interactive)
  (setq inferior-lisp-program 
        "C:/Programfiler/sbcl/1.0/sbcl.exe --noinform")
  (add-to-list 'load-path "C:/slime-2.0")
  (require 'slime)
  (slime-setup))


;;; Use the Common Lisp HyperSpec
(require 'hyperspec)
(setq common-lisp-hyperspec-root "file:C:/hyperspec/HyperSpec/")
;; lookup on loop:collect works
(eval-after-load "hyperspec" '(load "hyperspec-addon"))
(global-set-key [f1] 'common-lisp-hyperspec)



;at .xemacs\abbrevs
;(read-abbrev-file)  

;;Mapping M-p to scroll through previous commands
(global-set-key "\M-p" 'fi:pop-input)

;;Mapping M-n to scroll through next commands
(global-set-key "\M-n" 'fi:push-input)

(defun last-command ()
  (interactive)
  (print (format "Last command:%S" last-command)))

;;;;;;;
;; Return after long jump.

(defvar *last-point* nil)

(defun store-point ()
  "queue sematics"
  (interactive)
  (setq *last-point* (append (list (point)) *last-point*)))

(defun pop-point ()
  "pops last point, unless there is only one left. If so, only return it (no pop)"
  (if (> (length *last-point*) 1)
      (prog1
	  (car *last-point*)
	(setq *last-point* (cdr *last-point*)))
    (car *last-point*)))
      
(defun return-to-point ()
  (interactive)
  (goto-char (pop-point)))

(global-set-key [f4] 'store-point)
(global-set-key [f5] 'return-to-point)			    

(defadvice end-of-buffer (before jump-return ())
  ""
  (store-point))
(ad-activate 'end-of-buffer)

(defadvice beginning-of-buffer (before jump-return ())
  ""
  (store-point))
(ad-activate 'beginning-of-buffer)

(defadvice fi:lisp-find-definition (before jump-return ())
  ""
  (store-point))
(ad-activate 'fi:lisp-find-definition)

(defadvice isearch-forward (before jump-return ())
  ""
  (store-point))
(ad-activate 'isearch-forward)

;; Run cl each time emacs is run:
;;(run-common-lisp)


(setq auto-mode-alist (cons '("\\.py$" . python-mode) auto-mode-alist))
(setq interpreter-mode-alist (cons '("python" . python-mode)
                                    interpreter-mode-alist))
(autoload 'python-mode "python-mode" "Python editing mode." t)

(global-font-lock-mode t)
(setq font-lock-maximum-decoration t)
(setq transient-mark-mode t)
(column-number-mode 1)
(custom-set-variables
  ;; custom-set-variables was added by Custom -- don't edit or cut/paste it!
  ;; Your init file should contain only one such instance.
 '(case-fold-search t)
 '(current-language-environment "Latin-1")
 '(default-input-method "latin-1-prefix")
 '(global-font-lock-mode t nil (font-lock))
 '(show-paren-mode t nil (paren))
 '(transient-mark-mode t))
(custom-set-faces
  ;; custom-set-faces was added by Custom -- don't edit or cut/paste it!
  ;; Your init file should contain only one such instance.
 )


;; Make text the default mode
(setq default-major-mode 'text-mode)
;; Use fill mode (fixed length lines
(add-hook 'text-mode-hook 'turn-on-auto-fill)
;; ========== Set the fill column ==========
;; 78 visible character + line ending (two bytes on windows)
(setq-default fill-column 78)


;; ===== Turn off tab character =====
;;
;; Emacs normally uses both tabs and spaces to indent lines. If you
;; prefer, all indentation can be made from spaces only. To request this,
;; set `indent-tabs-mode' to `nil'. This is a per-buffer variable;
;; altering the variable affects only the current buffer, but it can be
;; disabled for all buffers.
;;
;; Use (setq ...) to set value locally to a buffer
;; Use (setq-default ...) to set value globally 
;;
(setq-default indent-tabs-mode nil)


(put 'upcase-region 'disabled nil)


;;tskogan@common-lisp.net:/project/crypticl/public_html
(defun scp-buffer (dst)
  (interactive "s scp current file to remote destination: ")
  (let ((retval (call-process "pscp" nil nil nil (buffer-file-name) dst)))
    (if (eq 0 retval)
        (princ (format "Copied file %s to %s" (buffer-file-name) dst))
      (princ (format "scp failed with return value %s" retval)))))
